#!/bin/bash
############################################################################
#Setup the environment for ntuple-gizmo
#USAGE: source setup.sh [<LCG-version>] [<platform>]
#       <LCG-version> defaults to 100
#       <platform>    defaults to x86_64-centos7-gcc10-opt
#   OR: source setup.sh lb-conda [<environment>]
#       <environment> defaults to default
############################################################################

printf "\033[1;37m INFO   \033[0m: Setting up environment for ntuple-gizmo\n"

if [[ "$1" == "lb-conda" ]];then
  printf "\033[1;37m INFO   \033[0m: Using lb-conda\n"
  #parse platform
  if [[ -z "$2" ]];then
    printf "\033[1;37m INFO   \033[0m: No lb-conda environment specified. Using default\n"
    CONDA_ENV="default"
  else
    CONDA_ENV="$2"
  fi
  shift $# # Clear the positional arguments so they don't get passed to LbEnv and lb-conda
  CONDA_ENV_PATH=$(which lb-conda &> /dev/null || source /cvmfs/lhcb.cern.ch/lib/LbEnv &> /dev/null; lb-conda $CONDA_ENV which python | sed 's/bin\/python//')
  source /cvmfs/lhcbdev.cern.ch/conda/miniconda/linux-64/prod/bin/activate
  printf "\033[1;37m INFO   \033[0m: Activating conda environment in $CONDA_ENV_PATH\n"
  conda activate $CONDA_ENV_PATH
  printf "\033[1;32m SUCCESS\033[0m: Conda environment was activated\n"
else
  #parse lcg version
  if [[ -z "$1" ]];then
    printf "\033[1;37m INFO   \033[0m: No LCG version specified. Defaulting to 100\n"
    LCG_VER="100"
  else
    LCG_VER="$1"
  fi

  #parse platform
  if [[ -z "$2" ]];then
    printf "\033[1;37m INFO   \033[0m: No platform specified. Defaulting to x86_64-centos7-gcc10-opt\n"
    PLTF="x86_64-centos7-gcc10-opt"
  else
    PLTF="$2"
  fi

  SFT_DIR=/cvmfs/sft.cern.ch/lcg/views/LCG_$LCG_VER/$PLTF

  #for implicit multithreading
  export OPENBLAS_MAIN_FREE=1

  if [[ -d "$SFT_DIR" ]]; then
    printf "\033[1;37m INFO   \033[0m: Setting up environment from LCG $LCG_VER views in $SFT_DIR\n"
    source $SFT_DIR/setup.sh
    printf "\033[1;32m SUCCESS\033[0m: LCG environment was set up\n"
  else
    printf "\033[1;31m ERROR  \033[0m: Can not setup environment. Make sure cvmfs is mounted...\n"
  fi
fi


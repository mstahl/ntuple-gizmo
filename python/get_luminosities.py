import os
bhadron_run2 = { "15U" : "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco15a/Stripping24r2/90000000/BHADRON.MDST",
                 "15D" : "/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r2/90000000/BHADRON.MDST",
                 "16U" : "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r2/90000000/BHADRON.MDST",
                 "16D" : "/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r2/90000000/BHADRON.MDST",
                 "17U" : "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST",
                 "17D" : "/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/BHADRON.MDST",
                 "18U" : "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/BHADRON.MDST",
                 "18D" : "/LHCb/Collision18/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco18/Stripping34/90000000/BHADRON.MDST"}
bhadron_lumis = dict()

for yp_tag, bookkeping_path in bhadron_run2.items():
  os.system(f'dirac-bookkeeping-get-files -B "{bookkeping_path}" --DQFlags=OK --Output=BHAD{yp_tag}.files')
  os.system(f'dirac-bookkeeping-file-metadata --Full --File=BHAD{yp_tag}.files | egrep ^Luminosity > BHAD{yp_tag}.lumis')
  lumi = 0.
  with open(f'BHAD{yp_tag}.lumis') as f:
    for line in f:
      lumi += float(line.replace("Luminosity","").strip())
  bhadron_lumis[yp_tag] = lumi/1e9

print(bhadron_lumis)
# Numbers pasted from bookkeeping using the DST output of the indicated stripping version.
# Where multiple streams available, BHADRON.DST is used. By courtesy of Adam Morris.
lumi_dict = {
  "pp": {
    "11D": 593755257.925, # S21
    "11U": 475338507.524,
    "12D": 986642790.155, # S21
    "12U": 995793413.518,
    "13D": 3570605.81119, # S20r2
    "15D": 162388414.933, # S24r2
    "15U": 122232478.105,
    "16D": 842214447.728, # S28r2
    "16U": 777775096.036,
    "17D": 861512732.786, # S29r2
    "17U": 820361392.954,
    "17-5TeV": 111729136.984,
    "18D": 1023802178.76, # S34
    "18U": 1106839299.7
  },
  "Pbp": {
    "13D": 302.797996517, # S20r3p1
    "13U": 263.311362797,
    "16D": 17454.6540634, # S30r2
  },
  "pPb": {
    "13D": 768.622014621, # S20r3p1
    "13U": 298.051124477,
    "16D": 12479.8630784 # S30r3
  }
}
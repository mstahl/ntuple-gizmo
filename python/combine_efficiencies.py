#!/usr/bin/python
import uproot    # https://uproot.readthedocs.io/en/latest/index.html
import awkward   # https://awkward-array.readthedocs.io/en/latest/index.html
import json      # https://docs.python.org/3/library/json.html
import argparse  # https://docs.python.org/3/library/argparse.html
import math

# parse command line arguments
parser = argparse.ArgumentParser(description="Combine efficiencies from file containing numerator and denominator trees with weights split by year and polarity. "\
                                             "ATTENTION: The script assumes that the ENTIRE DATAset for a given year and polarity has been processed (no failed ganga/DIRAC jobs!)")
parser.add_argument("-i","--input", dest="input", type=str, required=True, nargs="+", help="e.g. /path/to/input.root. Should contain trees with weights to combine.")
parser.add_argument("-o","--output", dest="output", type=str, required=True, help="e.g. /path/to/output.json, will contain pairs of \"efficiency\" and \"uncertainty\".")
parser.add_argument("-r","--reco_tree_name_pattern", dest="reco_tree_name_pattern", type=str, default="reco", help="unique string to match numerator trees.")
parser.add_argument("-t","--truth_tree_name_pattern", dest="truth_tree_name_pattern", type=str, default="truth", help="unique string to match denominator trees.")
parser.add_argument("-w","--weight_name", dest="weight_name", type=str, nargs="+", default="gb_weight", help="Name of the weight-branch.")
parser.add_argument("--truncate_weights", dest="truncation_limits", type=float, nargs=2, help="Truncate normalized weights before combination. Re-normalize after truncation.")
parser.add_argument("-c","--collision_mode", dest="collision_mode", type=str, default="pp", choices=["pp", "Pbp", "pPb"], help="Mode of collision to read from lumi table.")
args = parser.parse_args()

import sys
sys.path.append(".")
from lumi_dict import lumi_dict

efficiency  = 0.
std_dev = 0.
lumi = 0.
unweighted_efficiency = 0.
assert len(args.input) == len(args.weight_name), "input file and weight name arrays must have the same size."

# The input files contain the same set of trees (friend-trees) with weights from different factorizing weighting steps.
# The input trees are assumed to be split by year and magnet polarity and will be combined taking luminosity into account.
# For every (reco) input tree that matches "reco_tree_name_pattern", the weights from the friend-trees need to be multiplied event-by-event
# and then summed up to calculate the efficiency numerator. The same is done for generated events ("truth") for the efficiency denominator.
# The so obtained per year-polarity efficiencies are summed with a luminosity weight. Uncertainties are simple asymptotic binomial ones.
reco_treenames = [tn for tn in uproot.open(args.input[0]).keys(cycle=False) if args.reco_tree_name_pattern in tn]
for tn in reco_treenames:
  list_of_reco_friend_arrays = []
  list_of_truth_friend_arrays = []
  for fn,weight in zip(args.input,args.weight_name):
    file = uproot.open(fn)
    list_of_reco_friend_arrays.append(file[tn][weight].array())
    truth_tree_name = tn.replace(args.reco_tree_name_pattern,args.truth_tree_name_pattern)
    if truth_tree_name in [k.split(";",1)[0] for k in file.keys()]:
      list_of_truth_friend_arrays.append(file[truth_tree_name][weight].array())
  # read all weights as if they were friend trees (axis=0, otherwise it would be a scalar)
  reco_weights  = awkward.prod(list_of_reco_friend_arrays,axis=0)
  truth_weights = awkward.prod(list_of_truth_friend_arrays,axis=0)
  if args.truncation_limits is not None and len(args.truncation_limits) == 2:
    reco_weights  = awkward.nan_to_num(reco_weights,posinf=0,neginf=0)[(args.truncation_limits[0]<reco_weights)&(reco_weights<args.truncation_limits[1])]
    truth_weights = awkward.nan_to_num(truth_weights,posinf=0,neginf=0)[(args.truncation_limits[0]<truth_weights)&(truth_weights<args.truncation_limits[1])]
  # calculate efficiency with normalization of truncated weights
  sumw_numerator = awkward.sum(reco_weights)
  sumw_denominator = awkward.sum(truth_weights)
  n_eff_denominator = pow(sumw_denominator,2) / awkward.sum(truth_weights**2)
  # print("\033[0;36m{0:20} \033[1;33m{1:8}\033[0m {2}".format("combine_efficiencies","DEBUG:",
  #  "sumw_numerator {0:.4f} sumw_denominator {1:.4f} n_eff_denominator {1:.4f}".format(sumw_numerator, sumw_denominator, n_eff_denominator)),flush=True)
  # ATTENTION: the keys in the lumi dict must match the (reco) tree name to associate year-polarity weights from weighting to the lumi-weight!
  lumi_factors = {k:v for k, v in lumi_dict[args.collision_mode].items() if k in tn}
  if len(lumi_factors) > 1 :
    print("\033[0;36m{0:20} \033[1;33m{1:8}\033[0m {2}".format("combine_efficiencies","WARNING:", "Found more than one lumi factor."),flush=True)
  elif len(lumi_factors) == 1 :
    tmp_eff = sumw_numerator/sumw_denominator
    print("\033[0;36m{0:20} \033[1;37m{1:8}\033[0m {2}".format("combine_efficiencies","INFO:", \
    "Efficiency of {0}: ({1:.4f} +- {2:.4f})%".format(list(lumi_factors.keys())[0],100*tmp_eff, 100*math.sqrt(tmp_eff*(1-tmp_eff)/n_eff_denominator))),flush=True)
    efficiency += list(lumi_factors.values())[0]*tmp_eff
    unweighted_efficiency += list(lumi_factors.values())[0]*len(reco_weights)/len(truth_weights)
    std_dev += list(lumi_factors.values())[0]**2*tmp_eff*(1-tmp_eff)/n_eff_denominator
    lumi += list(lumi_factors.values())[0]
  else : print("\033[0;36m{0:20} \033[1;33m{1:8}\033[0m {2}".format("combine_efficiencies","WARNING:", "Something wrong with parsing lumi factors."),flush=True)
efficiency /= lumi
unweighted_efficiency /= lumi
uncertainty = math.sqrt(std_dev)/lumi
print("\033[0;36m{0:20} \033[1;37m{1:8}\033[0m {2}".format("combine_efficiencies","INFO:", \
      "Combined efficiency: ({0:.4f} +- {1:.4f})%".format(100*efficiency, 100*uncertainty)),flush=True)
# write output to disk
with open(args.output, "w") as outfile:
  outfile.write(json.dumps({ "efficiency" : efficiency, "uncertainty" : uncertainty, "unweighted_efficiency" : unweighted_efficiency }, indent=2))
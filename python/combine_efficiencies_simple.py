#!/usr/bin/python
import ROOT      # https://root.cern/doc/master
import json      # https://docs.python.org/3/library/json.html
import argparse  # https://docs.python.org/3/library/argparse.html
import math

# TODO : this script assumes the year/polarity splitting is done in files; could also have single file with many trees

# parse command line arguments
parser = argparse.ArgumentParser(description="Combine efficiencies from files split by year and polarity containing numerator and denominator trees with weights. "\
                                             "ATTENTION: The script assumes that the ENTIRE DATAset for a given year and polarity has been processed (no failed ganga/DIRAC jobs!)")
parser.add_argument("-o","--output", dest="output", type=str, required=True, help="e.g. /path/to/output.json, will contain pairs of \"efficiency\" and \"uncertainty\".")
parser.add_argument("-r","--reco", dest="reco", type=str, required=True, nargs="+", help="numerator file(s).")
parser.add_argument("-t","--truth", dest="truth", type=str, required=True, nargs="+", help="denominator file(s).")
parser.add_argument("--treereco", dest="treereco", type=str, default="t", help="numerator tree.")
parser.add_argument("--treetruth", dest="treetruth", type=str, default="t", help="denominator tree.")
parser.add_argument("-c","--collision_mode", dest="collision_mode", type=str, default="pp", choices=["pp", "Pbp", "pPb"], help="Mode of collision to read from lumi table.")
args = parser.parse_args()

import sys
sys.path.append(".")
from lumi_dict import lumi_dict

efficiency  = 0.
std_dev = 0.
lumi = 0.
assert len(args.reco) == len(args.truth), "lists of reco and truth input files must have the same length."
for rf,tf in zip(args.reco,args.truth):
  reco_df = ROOT.RDataFrame(args.treereco, rf)
  truth_df = ROOT.RDataFrame(args.treetruth, tf)
  sum_numerator = reco_df.Count().GetValue()
  sum_denominator = truth_df.Count().GetValue()
  # ATTENTION: the keys in the lumi dict must match the (reco) tree name to associate year-polarity weights from weighting to the lumi-weight!
  lumi_factors = {k:v for k, v in lumi_dict[args.collision_mode].items() if k in rf}
  if len(lumi_factors) > 1 :
    print("\033[0;36m{0:20} \033[1;33m{1:8}\033[0m {2}".format("combine_efficiencies","WARNING:", "Found more than one lumi factor."),flush=True)
  elif len(lumi_factors) == 1 :
    tmp_eff = sum_numerator/sum_denominator
    print("\033[0;36m{0:20} \033[1;37m{1:8}\033[0m {2}".format("combine_efficiencies","INFO:", \
    "Efficiency of {0}: ({1:.4f} +- {2:.4f})%".format(list(lumi_factors.keys())[0],100*tmp_eff, 100*math.sqrt(tmp_eff*(1-tmp_eff)/sum_denominator))),flush=True)
    efficiency += list(lumi_factors.values())[0]*tmp_eff
    std_dev += list(lumi_factors.values())[0]**2*tmp_eff*(1-tmp_eff)/sum_denominator
    lumi += list(lumi_factors.values())[0]
  else : print("\033[0;36m{0:20} \033[1;33m{1:8}\033[0m {2}".format("combine_efficiencies","WARNING:", "Something wrong with parsing lumi factors."),flush=True)
efficiency /= lumi
uncertainty = math.sqrt(std_dev)/lumi
print("\033[0;36m{0:20} \033[1;37m{1:8}\033[0m {2}".format("combine_efficiencies","INFO:", \
      "Combined efficiency: ({0:.4f} +- {1:.4f})%".format(100*efficiency, 100*uncertainty)),flush=True)
# write output to disk
with open(args.output, "w") as outfile:
  outfile.write(json.dumps({ "efficiency" : efficiency, "uncertainty" : uncertainty }, indent=2))

#!/usr/bin/python
from hep_ml import reweight # https://arogozhnikov.github.io/hep_ml/index.html
import uproot               # https://uproot.readthedocs.io/en/latest/index.html
import pandas               # https://pandas.pydata.org/pandas-docs/stable/index.html
import numpy                # https://numpy.org/
import json                 # https://docs.python.org/3/library/json.html
import argparse             # https://docs.python.org/3/library/argparse.html
from argparse import RawTextHelpFormatter

# parse command line arguments
parser = argparse.ArgumentParser(description="Weight a sample \"r\" to a target-sample \"t\" with GBReweighter from hep_ml. \n\u26A0 \u2620 \033[1;31m ATTENTION \033[0m \u2620 \u26A0 Use negative weights such as \"sWeights\" at your own risk!", formatter_class=RawTextHelpFormatter)
parser.add_argument("-r","--sample_to_weight",   dest="sample_to_weight", type=str, required=True, nargs="+", help="e.g. /path/to/the/sample_to_weight.root:DecayTree")
parser.add_argument("-t","--target_sample",      dest="target_sample",    type=str, required=True, nargs="+", help="e.g. /path/to/the/target_sample.root:dir/DecayTree")
parser.add_argument("-o","--output_sample",      dest="output_sample",    type=str, required=True, help="e.g. /path/to/the/output_sample.root, it will contain weights only")
parser.add_argument("-c","--config",             dest="config",           type=str, required=True, help="e.g. /path/to/the/config_file.json")
parser.add_argument("-a","--additional_samples", dest="more_samples",     type=str, nargs="+",     help="e.g. /path/to/the/another_sample_to_apply_the_weighting_to.root:t")
parser.add_argument("-f","--filter",             dest="filter",           type=str,
                    help="Filter string for sample_to_weight. The filter is applied for training, the weights will be calculated for the whole sample")
args = parser.parse_args()

#WARNING: Sometimes the script seems to get NaN or inf values. Everytime this happened, re-producing the inputs solved the issue.
# If NaN/innf really should become an issue, check this: https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.isnull.html

# get and parse config file
with open(args.config, "r") as f:
  config = json.load(f)
# store stuff we need a couple of times
tw = config.get("target_weight",None)
ow = config.get("origin_weight",None)
if ow and args.filter:
  print("\033[0;36m{0:32} \033[1;33m{1:8}\033[0m {2}".format("gbreweighter","WARNING:", "Are you sure you want to filter AND apply weights at the same time?"),flush=True)
target_branches = config.get("branches",["*"])
all_target_branches = target_branches + ([] if tw is None else [tw])
origin_branches = config.get("origin_branches",target_branches)
all_origin_branches = origin_branches + ([] if ow is None else [ow]) + config.get("branches_for_filter",[])
chain_treename_target = args.target_sample[0].rsplit(":",1)[1]
chain_treename_weight = args.sample_to_weight[0].rsplit(":",1)[1]
# sanity checks
def check_if_chain_is_sane(chain, treename):
  for chain_elt in chain:
    assert chain_elt.rsplit(":",1)[1] == treename
check_if_chain_is_sane(args.target_sample, chain_treename_target)
check_if_chain_is_sane(args.sample_to_weight, chain_treename_weight)
# maybe there is a usecase for cuts , but we currently don't support it
print("\033[0;36m{0:32} \033[1;37m{1:8}\033[0m {2}".format("gbreweighter","INFO:", "Reweighting {} in file(s) {} to match {} in {}"\
        .format(target_branches, args.sample_to_weight, origin_branches, args.target_sample)),flush=True)
target_df   = uproot.concatenate(args.target_sample, expressions=all_target_branches, filter_name="|".join(all_target_branches), library="pd")
origin_df = uproot.concatenate(args.sample_to_weight, expressions=all_origin_branches, filter_name="|".join(all_origin_branches), library="pd")
# train the reweighter
print("\033[0;36m{0:32} \033[1;37m{1:8}\033[0m {2}".format("gbreweighter","INFO:", "Done reading inputs. Now training reweighter..."),flush=True)
# see http://arogozhnikov.github.io/2015/10/09/gradient-boosted-reweighter.html for an introduction
# the default values here reimplement the ones from v0.6.0
gb_reweighter = reweight.GBReweighter(n_estimators=config.get("n_estimators",40), learning_rate=config.get("learning_rate",0.2), max_depth=config.get("max_depth",3),
                                      min_samples_leaf=config.get("min_samples_leaf",200), loss_regularization=config.get("loss_regularization",0.5),
                                      gb_args=config.get("gb_args",None))
do_folding = config.get("folding",False)
if do_folding :
  gb_reweighter = reweight.FoldingReweighter(gb_reweighter, n_folds=config.get("n_folds",2), random_state=config.get("random_state",None), verbose=config.get("verbose",False))
wdf = origin_df[origin_branches] if args.filter is None else origin_df.query(args.filter)[origin_branches]
gb_reweighter.fit(wdf, target_df[target_branches], original_weight=None if ow is None else origin_df[ow], target_weight=None if tw is None else target_df[tw])

# apply reweighting to an input dataframe (df) given a set of branches (with weight branch (wbn) containing origin weights) and returns a dataframe with weights
def assign_weights(df,branches,wbn):
  if do_folding :
    gb_weights = gb_reweighter.predict_weights(df[branches], original_weight=None if wbn is None else df[wbn], vote_function = lambda x : numpy.median(x, axis=0))
  else : gb_weights = gb_reweighter.predict_weights(df[branches], original_weight=None if wbn is None else df[wbn])
  if wbn: gb_weights = numpy.divide(gb_weights,df[wbn].to_numpy())
  truncation_limits = config.get("truncate_weights",None)
  if truncation_limits is not None and len(truncation_limits) == 2:
    norm = numpy.sum(gb_weights**2)
    gb_weights = (gb_weights/norm)[(truncation_limits[0]<gb_weights)&(gb_weights<truncation_limits[1])]
  if config.get("normalize_weights",False) :
    if truncation_limits is None:
      print("\033[0;36m{0:32} \033[1;33m{1:8}\033[0m {2}".format("gbreweighter","WARNING:", "Normalizing weights without previous truncation. This can go wrong if there are too large or small weights."))
    gb_weights = numpy.nan_to_num(gb_weights,posinf=0,neginf=0)
    normalization = numpy.sum(gb_weights)/numpy.sum(gb_weights**2)
    gb_weights = numpy.multiply(gb_weights,normalization)
  # make a DataFrame with normalized weights for the output
  return pandas.DataFrame(gb_weights, columns=[config.get("output_branch_name","gb_weight")])

# persist weights in friend trees
print("\033[0;36m{0:32} \033[1;37m{1:8}\033[0m {2}".format("gbreweighter","INFO:", "Done with training. Writing output to "+args.output_sample),flush=True)
outputfile = uproot.recreate(args.output_sample)
outputfile[config.get("output_tree",chain_treename_weight+"weights")] = assign_weights(origin_df,origin_branches,ow)
# apply the weighting scheme to other inputs
if args.more_samples is not None :
  for s in args.more_samples :
    another_df_to_weight = uproot.concatenate(s, expressions=origin_branches, filter_name="|".join(origin_branches), library="pd")
    outputfile[s.split(":",1)[0].split("/")[-1].replace(".root","")+"weights"] = assign_weights(another_df_to_weight,origin_branches,ow)

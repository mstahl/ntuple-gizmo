#!/usr/bin/python
import ROOT      # https://root.cern/doc/master
import json      # https://docs.python.org/3/library/json.html
import argparse  # https://docs.python.org/3/library/argparse.html
import math

# TODO : this script assumes the year/polarity splitting is done in single file with many trees
#        (also that numerator and denominator can be calculated from the same tree); could also have many files with one tree each

# parse command line arguments
parser = argparse.ArgumentParser(description="Combine a ratio of filters (e.g. pass/total) from a file containing numerator and denominator trees with weights split by year and polarity. "\
                                             "ATTENTION: The script assumes that the ENTIRE DATAset for a given year and polarity has been processed (no failed ganga/DIRAC jobs!)")
parser.add_argument("-o","--output", dest="output", type=str, required=True, help="e.g. /path/to/output.json, will contain pairs of \"efficiency\" and \"uncertainty\".")
parser.add_argument("-i","--input", dest="input_files", type=str, required=True, nargs="+", help="input file(s).")
parser.add_argument("-f","--friends", dest="friend_files", type=str, nargs="+", help="optional friend file(s).")
parser.add_argument("-t","--tree", dest="tree_name", type=str, default="t", help="tree name. (multiple files, single tree)")
parser.add_argument("--treenamepattern", dest="tree_name_pattern", type=str, help="unique string to match trees in friend files.")
parser.add_argument("-n","--numerator", dest="numerator", type=str, required=True, help="cut string for numerator.")
parser.add_argument("-d","--denominator", dest="denominator", type=str, required=True, help="cut string for denominator.")
parser.add_argument("-w","--weight_name", dest="weight_name", type=str, nargs="+", default="gb_weight", help="Name of the weight-branch.")
parser.add_argument("-c","--collision_mode", dest="collision_mode", type=str, default="pp", choices=["pp", "Pbp", "pPb"], help="Mode of collision to read from lumi table.")
args = parser.parse_args()

import sys
sys.path.append(".")
from lumi_dict import lumi_dict

result  = 0.
lumi = 0.
assert len(args.friend_files) == len(args.weight_name), "input file and weight name arrays must have the same size."

# get trees that are named such that we can apply lumi weights on them (e.g. <tree_name_pattern>.*18U.*)
first_f = ROOT.TFile.Open(args.friend_files[0])
list_of_trees_in_friend_file = [tn.GetName() for tn in first_f.GetListOfKeys()]
for fn in args.input_files:
  # ATTENTION: the keys in the lumi dict must match the (reco) tree name to associate year-polarity weights from weighting to the lumi-weight!
  lumi_factors = {k:v for k, v in lumi_dict[args.collision_mode].items() if k in fn}
  assert len(lumi_factors) == 1, "Something wrong with parsing lumi factors."
  # if that's ok, go on with the slow part
  f = ROOT.TFile.Open(fn)
  t = f.Get(args.tree_name)
  list_of_matching_trees = []
  for ff in args.friend_files:
    for tn in list_of_trees_in_friend_file:
      if args.tree_name_pattern in tn and list(lumi_factors.keys())[0] in tn:
        list_of_matching_trees.append(tn)
        t.AddFriend(tn,ff)
  df = ROOT.RDataFrame(t)
  assert len(list_of_matching_trees) == len(args.weight_name)
  df = df.Define("prod","*".join([f"{tn}.{wn}" for tn, wn in zip(list_of_matching_trees,args.weight_name)]))
  sum_numerator = df.Filter(args.numerator).Sum("prod").GetValue()
  sum_denominator = df.Filter(args.denominator).Sum("prod").GetValue()
  tmp_ratio = sum_numerator/sum_denominator
  print("\033[0;36m{0:20} \033[1;37m{1:8}\033[0m {2}".format("weighted_ratio_of_filtered_events","INFO:", \
  "result of {0}: {1:.4f}".format(list(lumi_factors.keys())[0],tmp_ratio,)),flush=True)
  result += list(lumi_factors.values())[0]*tmp_ratio
  lumi += list(lumi_factors.values())[0]
result /= lumi
print("\033[0;36m{0:20} \033[1;37m{1:8}\033[0m {2}".format("weighted_ratio_of_filtered_events","INFO:", "Combined result: {0:.4f}".format(result)),flush=True)
# write output to disk
with open(args.output, "w") as outfile:
  outfile.write(json.dumps({ "result" : result }, indent=2))

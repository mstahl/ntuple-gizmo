"""
Snakemake wrapper for elwms.
For script documentation see
https://gitlab.cern.ch/mstahl/ntuple-gizmo/-/blob/ca3927e178e72333c9083022ba1e667478a03dd0/doc/elwms.md

The wrapper consumes inputs from the "input.sample" node or from "params.sample".
This is to have hooks for input files that are part of the workflow and those that aren't.
In addition there can be friend trees. If they come from within the workflow, but have a different tree name than "params.tree_name",
we advise to still put the friend tree in "input.<something that is not friends or sample>", but pass the "file:tree" combination to
"params.friend". This is where files from out of the workflow (genuine input files) belong as well.

The wrapper needs:
- input.sample or params.sample (list or string): an input root file.
- params.config (str): config file
Optional arguments:
- params.work_dir (str): working directory, default "$RF"
- params.tee (str): for logging, default ">"
- params.tree_name (str): tree name for input files and friends (see note above), default "t"
- params.workspace_name (str): name of output RooWorkspace, default "w"
- params.dataset_name (str): name of output RooDataSet, default "ds"
- params.verbosity (castable to str)
- params.weight_file (str or dict) : combination(s) of <weightfile (xml format):BDT response name>. If given as str, they need to be separated by a semicolon.
- params.transformation (str or dict) : whitespace separated combination(s) of <variable->transformation>; alternatively pass them as {variable : transformation, ...} dict
- params.friend or input.friend (str or list): friend trees
- params.<anything> (str or dict): used to assemble joker string to replace stuff in config file
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["NTGROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples, parse_friends, parse_joker)

# before we do anything, check if the executable is there
exe = get_exe("elwms")

samples = snakemake.input.get("sample", None)
friends = snakemake.input.get("friend", None)
more_samples = snakemake.params.get("sample", None)
more_friends = snakemake.params.get("friend", None)
cfg = snakemake.params.get("config", None)
wd  = snakemake.params.get("work_dir", "$RF")
tn  = snakemake.params.get("tree_name", "t")
wn  = snakemake.params.get("workspace_name", "w")
ds  = snakemake.params.get("dataset_name", "ds")
vb  = snakemake.params.get("verbosity", None)
wf  = snakemake.params.get("weight_file", None)
trf = snakemake.params.get("transformation", None)
tee = snakemake.params.get("tee", ">")
out = snakemake.output
log = snakemake.log

# make sure that all inputs are what we expect them to be
assert samples is not None or more_samples is not None, "\"input.sample\" or non-workflow input from \"params.sample\" is missing"
ifn = parse_samples(samples,more_samples)

friends = parse_friends(tn,friends,more_friends)

assert cfg is not None and isinstance(cfg,str), "\"params.config\" has to be provided and needs to be a string"
assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(tn,str), "\"params.tree_name\" is expected to be a string"
assert isinstance(wn,str), "\"params.workspace_name\" is expected to be a string"
assert isinstance(ds,str), "\"params.dataset_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

if vb is not None: vb = "-v"+str(vb)
else : vb = ""

# parse file for applying TMVA weights
if wf is not None:
  if isinstance(wf,dict): wf = ";".join(["{}:{}".format(k,v) for k,v in wf.items()])
  assert isinstance(wf,str), "\"params.weight_file\" is expected to be a string or dict"
  wf = "-m '"+wf+"'"
else : wf = ""

# parse transformations
if trf is not None:
  if isinstance(trf,dict): trf = " ".join(["'{}->{}'".format(k,v) for k,v in trf.items()])
  assert isinstance(trf,str), "\"params.transformation\" is expected to be a string or dict"
else : trf = ""

if log.__len__() > 0: logcmd = f"{tee} {log.__str__()} 2>&1"
else : logcmd = ""

# parse joker arguments
joker = parse_joker(snakemake.params,["config","work_dir","tree_name","tee","sample","friend","verbosity","workspace_name","dataset_name","weight_file","transformation"])

# output file
of = out[0].split("/")[-1]

shell("{exe} -c {cfg} -d {wd} -i '{ifn}' -o {of} -t {tn} -w {wn} -r {ds} {joker} {friends} {wf} {vb} {trf} {logcmd}")
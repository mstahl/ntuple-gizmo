"""
Snakemake wrapper for chain2histRDF.
For script documentation see
https://gitlab.cern.ch/mstahl/ntuple-gizmo/-/blob/ca3927e178e72333c9083022ba1e667478a03dd0/doc/chain2histRDF.md

The wrapper consumes inputs from the "input.sample" node or from "params.sample".
This is to have hooks for input files that are part of the workflow and those that aren't.
In addition there can be friend trees. If they come from within the workflow, but have a different tree name than "params.tree_name",
we advise to still put the friend tree in "input.<something that is not friends or sample>", but pass the "file:tree" combination to
"params.friend". This is where files from out of the workflow (genuine input files) belong as well.

The wrapper needs:
- input.sample or params.sample (list or string): an input file with histograms to draw
- params.config (str): config file for plotting
Optional arguments:
- params.work_dir (str): working directory, default "$RF"
- params.tee (str): for logging, default ">"
- params.tree_name (str): tree name for input files and friends (see note above), default "t"
- params.friend or input.friend (str or list): friend trees
- params.verbosity (castable to str)
- params.<anything> (str or dict): used to assemble joker string to replace stuff in config file
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["NTGROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples, parse_friends, parse_joker)

# before we do anything, check if the executable is there
exe = get_exe("chain2histRDF")

samples = snakemake.input.get("sample", None)
friends = snakemake.input.get("friend", None)
more_samples = snakemake.params.get("sample", None)
more_friends = snakemake.params.get("friend", None)
cfg = snakemake.params.get("config", None)
wd  = snakemake.params.get("work_dir", "$RF")
tn  = snakemake.params.get("tree_name", "t")
vb  = snakemake.params.get("verbosity", None)
tee = snakemake.params.get("tee", ">")
log = snakemake.log

# make sure that all inputs are what we expect them to be
assert samples is not None or more_samples is not None, "\"input.sample\" or non-workflow input from \"params.sample\" is missing"
ifn = parse_samples(samples,more_samples)

friends = parse_friends(tn,friends,more_friends)

assert cfg is not None and isinstance(cfg,str), "\"params.config\" has to be provided and needs to be a string"
assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(tn,str), "\"params.tree_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

if vb is not None: vb = "-v"+str(vb)
else : vb = ""

if log.__len__() > 0: logcmd = f"{tee} {log.__str__()} 2>&1"
else : logcmd = ""

# parse joker arguments
joker = parse_joker(snakemake.params,["config","work_dir","tree_name","tee","sample","friend","verbosity"])

shell("{exe} -c {cfg} -d {wd} -i '{ifn}' -o {snakemake.output[0]} -t {tn} {joker} {vb} {friends} {logcmd}")
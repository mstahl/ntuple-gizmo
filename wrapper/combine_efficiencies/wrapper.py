"""
Snakemake wrapper for combine_efficiencies.
For script documentation see
https://gitlab.cern.ch/mstahl/ntuple-gizmo/-/blob/ca3927e178e72333c9083022ba1e667478a03dd0/doc/combine_efficiencies.md
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell

# to make it obvious what the default parameters of the scripts are, they are repeated here:
rtnp = snakemake.params.get("reco_tree_name_pattern", "reco")
ttnp = snakemake.params.get("truth_tree_name_pattern", "truth")
wn   = snakemake.params.get("weight_name", "gb_weight")
cm   = snakemake.params.get("collision_mode", "pp")
tw   = snakemake.params.get("truncate_weights", [])
tee  = snakemake.params.get("tee", ">")

assert isinstance(tw,list), "\""+tw+"\" is expected to be a list. It should define the limits of weight truncation like [0.1,10]"
truncation = ""
if tw is not None and len(tw) == 2: truncation = "--truncate_weights "+" ".join(tw)

shell("python $NTGROOT/python/combine_efficiencies.py -i {snakemake.input} -o {snakemake.output[0]} -r {rtnp} -t {ttnp} -w {wn} -c {cm} {truncation} {tee} {snakemake.log} 2>&1")
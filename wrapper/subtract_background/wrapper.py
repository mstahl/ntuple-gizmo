"""
Snakemake wrapper for subtract_background.
For script documentation see https://gitlab.cern.ch/mstahl/ntuple-gizmo/-/blob/future/doc/subtract_background.md

The wrapper consumes inputs from the "input.sample" node or from "params.sample".
This is to have hooks for input files that are part of the workflow and those that aren't.

The wrapper needs:
- input.sample or params.sample (list or string): an input file with unbinned data from which the main histogram for background subtraction was made
- params.control_vars (list or string): only 2 variables are supported, like [B_PT,B_ETA] or "B_PT;B_ETA"
- params.control_var_hist (string): name of the histogram with the mixed distribution
- params.subtract_hists (list or string): name of the histogram(s) to subtract
- params.file_control_var_hist (string): file containing hist of control vars
- params.files_subtract_hists (list or string): file containing hists to subtract
- params.norm_control_var_hist (string): normalization of control var histogram (like the number of events from a fit to the discriminating variable)
- params.norms_subtract_hists (list or string): normalization of histograms to subtract
Optional arguments:
- params.work_dir (str): working directory, default "$RF"
- params.tee (str): for logging, default ">"
- params.tree_name (str): tree name for input files, default "t"
- params.verbosity (castable to str)
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["NTGROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples)

# before we do anything, check if the executable is there
exe = get_exe("subtract_background")

samples = snakemake.input.get("sample", None)
more_samples = snakemake.params.get("sample", None)
wd  = snakemake.params.get("work_dir", "$RF")
tn  = snakemake.params.get("tree_name", "t")
wn  = snakemake.params.get("weight_name", "w")
cvs = snakemake.params.get("control_vars", None)
meb = str(snakemake.params.get("min_events_per_bin", 42))
cvh = snakemake.params.get("control_var_hist", None)
shs = snakemake.params.get("subtract_hists", None)
fch = snakemake.params.get("file_control_var_hist", None)
fsh = snakemake.params.get("files_subtract_hists", None)
nch = snakemake.params.get("norm_control_var_hist", None)
nsh = snakemake.params.get("norms_subtract_hists", None)
vb  = snakemake.params.get("verbosity", None)
tee = snakemake.params.get("tee", ">")
log = snakemake.log

# make sure that all inputs are what we expect them to be
assert samples is not None or more_samples is not None, "\"input.sample\" or non-workflow input from \"params.sample\" is missing"
ifn = parse_samples(samples,more_samples)

assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(tn,str), "\"params.tree_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

vb = "-v"+str(vb) if vb is not None else ""

if log.__len__() > 0: logcmd = f"{tee} {log.__str__()} 2>&1"
else : logcmd = ""

# parse joker arguments (here the joker arguments are the 2 calibration variables)
assert cvs is not None, "\"params.control_vars\" is missing"

cali_vars = cvs if isinstance(shs,str) else parse_samples(cvs)

# assemble the histogram string
histos = f"'{fch}:{cvh};{nch}'"

if isinstance(shs,str):
  assert isinstance(fsh,str), "\"params.files_subtract_hists\" is expected to be a string"
  assert isinstance(nsh,str), "\"params.norms_subtract_hists\" is expected to be a string"
  histos += f" '{fsh}:{shs};{nsh}'"
elif isinstance(shs,list):
  assert isinstance(fsh,list) and len(shs) == len(fsh) or isinstance(fsh,str), \
         "\"params.files_subtract_hists\" is expected to be a string or a list with the same length as \"params.subtract_hists\""
  assert isinstance(nsh,list) and len(shs) == len(nsh), "\"params.norms_subtract_hists\" is expected to be a list and to have the same length as \"params.subtract_hists\""
  if isinstance(fsh,list):
    for file,hist,norm in zip(fsh,shs,nsh):
      histos += f" '{file}:{hist};{norm}'"
  elif isinstance(fsh,str): # we might have several histograms in one input file
    for hist,norm in zip(shs,nsh):
      histos += f" '{fsh}:{hist};{norm}'"

shell("{exe} -d {wd} -i '{ifn}' -j '{cali_vars}' -n {meb} -o {snakemake.output[0]} -t {tn} -w {wn} {histos} {vb} {logcmd}")
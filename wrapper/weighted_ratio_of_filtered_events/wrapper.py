"""
Snakemake wrapper for weighted_ratio_of_filtered_events.
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell

# to make it obvious what the default parameters of the scripts are, they are repeated here:
inf = snakemake.input.get("sample", None)
ffs = snakemake.input.get("friends", None)
tnp = snakemake.params.get("tree_name_pattern", "reco")
num = snakemake.params.get("numerator", None)
den = snakemake.params.get("denominator", None)
cm  = snakemake.params.get("collision_mode", "pp")
wn  = snakemake.params.get("weight_name", None)
tee = snakemake.params.get("tee", ">")

assert num is not None and den is not None

shell("python $NTGROOT/python/weighted_ratio_of_filtered_events.py -i {inf} -f {ffs} -o {snakemake.output[0]} -n '{num}' -d '{den}' -c {cm} --treenamepattern {tnp} -w {wn} {tee} {snakemake.log} 2>&1")
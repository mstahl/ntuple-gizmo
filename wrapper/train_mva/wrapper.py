"""
Snakemake wrapper for train_mva.
For script documentation see
https://gitlab.cern.ch/mstahl/ntuple-gizmo/-/blob/future/doc/train_mva.md

The wrapper consumes inputs from the "input.sig_sample" and "input.bkg_sample" nodes or
from "params.sig_sample" and "params.bkg_sample".
This is to have hooks for input files that are part of the workflow and those that aren't.

The wrapper needs:
- input.sig_sample or params.sig_sample (list or string): an input root file with signal data.
- input.bkg_sample or params.bkg_sample (list or string): an input root file with background data.
- params.config (str): config file
Optional arguments:
- params.work_dir (str): working directory, default "$RF"
- params.tee (str): for logging, default ">"
- params.sig_tree_name (str): tree name for signal input files (see note above), default "t"
- params.bkg_tree_name (str): tree name for background input files (see note above), default "t"
- params.verbosity (castable to str)
- params.<anything> (str or dict): used to assemble joker string to replace stuff in config file
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["NTGROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples, parse_friends, parse_joker)

# before we do anything, check if the executable is there
exe = get_exe("train_mva")

signal_sample = snakemake.input.get("sig_sample", None)
background_sample = snakemake.input.get("bkg_sample", None)
more_signal_samples = snakemake.params.get("sig_sample", None)
more_background_samples = snakemake.params.get("bkg_sample", None)

cfg = snakemake.params.get("config", None)
wd  = snakemake.params.get("work_dir", "$RF")
stn = snakemake.params.get("sig_tree_name", "t")
btn = snakemake.params.get("bkg_tree_name", "t")
vb  = snakemake.params.get("verbosity", None)
tee = snakemake.params.get("tee", ">")
out = snakemake.output
log = snakemake.log

# make sure that all inputs are what we expect them to be
assert signal_sample is not None or more_signal_samples is not None, "\"input.sig_sample\" or non-workflow input from \"params.sig_sample\" is missing"
assert background_sample is not None or more_background_samples is not None, "\"input.bkg_sample\" or non-workflow input from \"params.bkg_sample\" is missing"

assert cfg is not None and isinstance(cfg,str), "\"params.config\" has to be provided and needs to be a string"
assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(stn,str), "\"params.sig_tree_name\" is expected to be a string"
assert isinstance(btn,str), "\"params.bkg_tree_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

sfn = parse_samples(signal_sample,more_signal_samples)
bfn = parse_samples(background_sample,more_background_samples)
sfn += f":{stn}"
bfn += f":{btn}"

if vb is not None: vb = "-v"+str(vb)
else : vb = ""

if log.__len__() > 0: logcmd = f"{tee} {log.__str__()} 2>&1"
else : logcmd = ""

# parse joker arguments
joker = parse_joker(snakemake.params,["config","work_dir","sig_tree_name","bkg_tree_name","tee","sig_sample","bkg_sample","verbosity"])

shell("{exe} -c {cfg} -d {wd} -s '{sfn}' -b '{bfn}' -o {snakemake.output[0]} {joker} {vb} {logcmd}")
"""
Snakemake wrapper for generate_toy_histograms.
For script documentation see ../../doc/generate_toy_histograms.md

The wrapper needs:
- input.sample (string): input file with histogram
- params.histogram_name (string): name of the input histogram (to generate toys from)

Optional arguments:
- params.work_dir (str): working directory, default "$RF"
- params.tee (str): for logging, default ">"
- params.verbosity (castable to str)
- params.ntoys (int) number of toy histograms to generate, default 100
- params.nbins (int) number of bins of output histograms, default 2500
"""

__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["NTGROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import get_exe

# before we do anything, check if the executable is there
exe = get_exe("generate_toy_histograms")

sample = snakemake.input.get("sample", None)
wd      = snakemake.params.get("work_dir", "$RF")
hn      = snakemake.params.get("histogram_name", None)
ntoys   = str(snakemake.params.get("ntoys", 100))
nbins   = str(snakemake.params.get("nbins", 2500))
vb      = snakemake.params.get("verbosity", None)
tee     = snakemake.params.get("tee", ">")

log = snakemake.log

# make sure that all inputs are what we expect them to be
assert sample is not None and isinstance(sample,str), "\"input.sample\" is missing or not a string"

assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(hn,str), "\"params.tree_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

vb = "-v"+str(vb) if vb is not None else ""

if log.__len__() > 0: logcmd = f"{tee} {log.__str__()} 2>&1"
else : logcmd = ""

shell("{exe} -d {wd} -i {sample} -n {ntoys} -m {nbins} -o {snakemake.output[0]} -t {hn} {vb} {logcmd}")
"""
Snakemake wrapper for weight_1d.
For script documentation see ../../doc/weight_1d.md

The wrapper can take several inputs from the workflow itself or from outside.
These are "sample", "target_file", "origin_file", "additional_sample".
This is to have hooks for input files that are part of the workflow and those that aren't.

The wrapper needs:
- input.origin_file or params.origin_file (string): file with the origin tree to deduce the weighting scheme
- input.target_file or params.target_file (string): file with the target tree to deduce the weighting scheme
- params.config (string): config file, see [docs](../../doc/weight_1d.md) on how to populate it

Optional arguments:
- input.additional_sample or params.additional_sample (list or str): files with data to apply the weighting to
- params.target_tree (string): name of the target tree (to weight to for deducing the weighting scheme)
- params.origin_tree (string): name of the origin tree (the one that's weighted for deducing the weighting scheme)
- params.work_dir (str): working directory, default "$RF"
- params.tee (str): for logging, default ">"
- params.tree_name (str): tree name for input files, default "t"
- params.verbosity (castable to str)
- params.min_events_per_bin (int) to define the binning, default 42
- params.additional_tree_name (str) name of trees in "additional_sample" (they need to be the same in each file)

"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["NTGROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples, parse_friends, parse_joker)

# before we do anything, check if the executable is there
exe = get_exe("weight_1d")

tif = snakemake.input.get("target_file", None)
oif = snakemake.input.get("origin_file", None)
ais = snakemake.input.get("additional_sample",None)
cfg = snakemake.params.get("config", None)
wd  = snakemake.params.get("work_dir", "$RF")
meb = str(snakemake.params.get("min_events_per_bin", 42))
tt  = snakemake.params.get("target_tree", "t")
tpf = snakemake.params.get("target_file", None)
ot  = snakemake.params.get("origin_tree", "t")
opf = snakemake.params.get("origin_file", None)
vb  = snakemake.params.get("verbosity", None)
tee = snakemake.params.get("tee", ">")
aps = snakemake.params.get("additional_sample",None)
atn = snakemake.params.get("additional_tree_name", ot)

log = snakemake.log

# make sure that all inputs are what we expect them to be
origin = parse_samples(oif,opf)
target = parse_samples(tif,tpf)

assert cfg is not None and isinstance(cfg,str), "\"params.config\" has to be provided and needs to be a string"
assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

asa = parse_friends(atn,ais,aps)

vb = "-v"+str(vb) if vb is not None else ""

if log.__len__() > 0: logcmd = f"{tee} {log.__str__()} 2>&1"
else : logcmd = ""

# parse joker arguments
joker = parse_joker(snakemake.params,["config","work_dir","tree_name","tee","sample","verbosity","min_events_per_bin","target_tree","target_file","origin_tree","origin_file"])

shell("{exe} -c {cfg} -d {wd} -i '{origin}:{ot}' {joker} -n {meb} -o {snakemake.output[0]} -t '{target}:{tt}' {asa} {vb} {logcmd}")
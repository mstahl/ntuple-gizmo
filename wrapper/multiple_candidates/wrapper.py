"""
Snakemake wrapper for multiple_candidates.
For script documentation see
https://gitlab.cern.ch/mstahl/ntuple-gizmo/-/blob/ca3927e178e72333c9083022ba1e667478a03dd0/doc/multiple_candidates.md

The wrapper consumes inputs from the "input.sample" node or from "params.sample".
This is to have hooks for input files that are part of the workflow and those that aren't.

In addition there can be friend trees. If they come from within the workflow, but have a different tree name than "params.tree_name",
we advise to still put the friend tree in "input.<something that is not friends or sample>", but pass the "file:tree" combination to
"params.friend". This is where files from out of the workflow (genuine input files) belong as well.

The wrapper needs:
- input.sample or params.sample (list or string): an input file with multiple candidates
Optional arguments:
- params.work_dir (str): working directory, default "$RF"
- params.tee (str): for logging, default ">"
- params.tree_name (str): tree name for input files and friends (see note above), default "t"
- params.friend or input.friend (str or list): friend trees
- params.verbosity (castable to str)
- params.mult_rej (str): passed to the exe as "joker" string (a user defined criterion for rejecting multiple candidates given to as Define action to RDataFrame)
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["NTGROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples, parse_friends)

# before we do anything, check if the executable is there
exe = get_exe("multiple_candidates")

samples = snakemake.input.get("sample", None)
friends = snakemake.input.get("friend", None)
more_samples = snakemake.params.get("sample", None)
more_friends = snakemake.params.get("friend", None)
wd  = snakemake.params.get("work_dir", "$RF")
tn  = snakemake.params.get("tree_name", "t")
otn = snakemake.params.get("output_tree_name", "w")
mr  = snakemake.params.get("mult_rej", None)
vb  = snakemake.params.get("verbosity", None)
tee = snakemake.params.get("tee", ">")
log = snakemake.log

# make sure that all inputs are what we expect them to be
assert samples is not None or more_samples is not None, "\"input.sample\" or non-workflow input from \"params.sample\" is missing"
ifn = parse_samples(samples,more_samples)

friends = parse_friends(tn,friends,more_friends)

assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(tn,str), "\"params.tree_name\" is expected to be a string"
assert isinstance(otn,str), "\"params.output_tree_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

if vb is not None: vb = "-v"+str(vb)
else : vb = ""

if log.__len__() > 0: logcmd = f"{tee} {log.__str__()} 2>&1"
else : logcmd = ""

# parse joker arguments (here the joker is the criterion for rejecting multiple candidates)
joker = "-j '"+mr+"'" if mr else ""

# if the tex output is given as well
texoutstub = ""
if len(snakemake.output)>1 :
  texoutstub = "-r "+snakemake.output[1][:-6]

shell("{exe} -d {wd} -i '{ifn}' {joker} -o {snakemake.output[0]} -t {tn} -w {otn} {texoutstub} {friends} {vb} {logcmd}")
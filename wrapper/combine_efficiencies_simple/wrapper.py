"""
Snakemake wrapper for combine_efficiencies_simple.
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell

# to make it obvious what the default parameters of the scripts are, they are repeated here:
rf  = snakemake.input.get("reco", None)
tf  = snakemake.input.get("truth", None)
arf = snakemake.params.get("reco", None)
atf = snakemake.params.get("truth", None)
rt  = snakemake.params.get("treereco", "t")
tt  = snakemake.params.get("treetruth", "t")
cm  = snakemake.params.get("collision_mode", "pp")
tee = snakemake.params.get("tee", ">")

reco_files = ""
truth_files = ""
if rf is not None:
  reco_files += rf if isinstance(rf,str) else " ".join(rf)
elif arf is not None:
  reco_files += arf if isinstance(arf,str) else " ".join(arf)
else : raise Exception("Cannot parse reco files")

if tf is not None:
  truth_files += rf if isinstance(tf,str) else " ".join(tf)
elif arf is not None:
  truth_files += arf if isinstance(atf,str) else " ".join(atf)
else : raise Exception("Cannot parse truth files")

shell("python $NTGROOT/python/combine_efficiencies_simple.py -r {reco_files} -t {truth_files} -o {snakemake.output[0]} -c {cm} --treereco {rt} --treetruth {tt} {tee} {snakemake.log} 2>&1")
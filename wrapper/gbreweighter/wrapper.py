"""
Snakemake wrapper for combine_efficiencies.
For script documentation see
https://gitlab.cern.ch/mstahl/ntuple-gizmo/-/blob/ca3927e178e72333c9083022ba1e667478a03dd0/doc/gbreweighter.md

The wrapper can take slightly different inputs that the script itself.
Samples to weight, additional samples or target samples can be passed as list via e.g. input.sample_to_weight
or as string with the treename appended in e.g. params.sample_to_weight.
When passed as a list via input, their argument to the script is assmebled here and the tree name (params.tree_name) is taken the same for all samples.
If the tree names differ it is still possible to pass the chain as string to params.
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell

rws = snakemake.params.get("sample_to_weight",None)
rwc = snakemake.input.get("sample_to_weight",None)
ts  = snakemake.params.get("target_sample",None)
tc  = snakemake.input.get("target_sample",None)
asa = snakemake.params.get("additional_sample",None)
ac  = snakemake.input.get("additional_sample",None)
tn  = snakemake.params.get("tree_name", "t")
cfg = snakemake.params.get("config", "")
cut = snakemake.params.get("filter", "")
tee = snakemake.params.get("tee", ">")

# make sure that all inputs are what we expect them to be
def prepare_inputs(as_input,as_param,name_for_messaging):
  if as_input is not None:
    if isinstance(as_input,str): return as_input+":"+tn
    else : return " ".join(["{}:{}".format(f,tn) for f in as_input])
  elif as_param is not None:
    assert isinstance(as_param,str), "\""+name_for_messaging+"\" is expected to be a string"
    for s in as_param.split(" "):
      assert ":" in s, "\""+name_for_messaging+"\" needs to contain tree name like so \"sample.root:tree\""
    return as_param

rifn = prepare_inputs(rwc,rws,"sample_to_weight")
tifn = prepare_inputs(tc,ts,"target_sample")
aifn = prepare_inputs(ac,asa,"additional_sample")
if aifn : aifn = "-a "+aifn
else : aifn = ""

assert cfg is not None and isinstance(cfg,str), "\"params.config\" has to be provided and needs to be a string"
assert isinstance(tn,str), "\"params.tree_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"
assert isinstance(cut,str), "\"params.filter\" has to be be a string"
if cut : cut = f"-f '{cut}'"

shell("python $NTGROOT/python/gbreweighter.py -c {cfg} -t {tifn} -r {rifn} -o {snakemake.output[0]} {cut} {aifn} {tee} {snakemake.log} 2>&1")
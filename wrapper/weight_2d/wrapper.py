"""
Snakemake wrapper for weight_2d.
For script documentation see ../../doc/weight_2d.md

The wrapper can take several inputs from the workflow itself or from outside.
These are "sample", "target_file", "origin_file", "additional_sample".
This is to have hooks for input files that are part of the workflow and those that aren't.

The wrapper needs:
- input.sample or params.sample (list or string): an input file with unbinned data from which the weighting scheme was deduced
- input.origin_file or params.origin_file (string): file with the origin histogram to deduce the weighting scheme
- input.target_file or params.target_file (string): file with the target histogram to deduce the weighting scheme
- params.target_hist (string): name of the target histogram (to weight to for deducing the weighting scheme)
- params.origin_hist (string): name of the origin histogram (the one that's weighted for deducing the weighting scheme)
- params.config (string): config file, see [docs](../../doc/weight_2d.md) on how to populate it

Optional arguments:
- input.additional_sample or params.additional_sample (list or str): files with data to apply the weighting to
- params.work_dir (str): working directory, default "$RF"
- params.tee (str): for logging, default ">"
- params.tree_name (str): tree name for input files, default "t"
- params.verbosity (castable to str)
- params.min_events_per_bin (int) to define the binning, default 42
- params.additional_tree_name (str) name of trees in "additional_sample" (they need to be the same in each file)

"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["NTGROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples, parse_friends, parse_joker)

# before we do anything, check if the executable is there
exe = get_exe("weight_2d")

samples = snakemake.input.get("sample", None)
tif     = snakemake.input.get("target_file", None)
oif     = snakemake.input.get("origin_file", None)
ais      = snakemake.input.get("additional_sample",None)

more_samples = snakemake.params.get("sample", None)
cfg          = snakemake.params.get("config", None)
wd           = snakemake.params.get("work_dir", "$RF")
tn           = snakemake.params.get("tree_name", "t")
meb          = str(snakemake.params.get("min_events_per_bin", 42))
th           = snakemake.params.get("target_hist", None)
tpf          = snakemake.params.get("target_file", None)
oh           = snakemake.params.get("origin_hist", None)
opf          = snakemake.params.get("origin_file", None)
vb           = snakemake.params.get("verbosity", None)
tee          = snakemake.params.get("tee", ">")
aps          = snakemake.params.get("additional_sample",None)
atn          = snakemake.params.get("additional_tree_name", tn)

log = snakemake.log

# make sure that all inputs are what we expect them to be
assert samples is not None or more_samples is not None, "\"input.sample\" or non-workflow input from \"params.sample\" is missing"
ifn = parse_samples(samples,more_samples)

assert oif is not None or opf is not None, "\"input.origin_file\" or non-workflow input from \"params.origin_file\" is missing"
of = oif if oif is not None else opf
assert tif is not None or tpf is not None, "\"input.target_file\" or non-workflow input from \"params.target_file\" is missing"
tf = tif if tif is not None else tpf

assert oh is not None, "\"params.origin_hist\" is missing"
assert th is not None, "\"params.target_hist\" is missing"

assert cfg is not None and isinstance(cfg,str), "\"params.config\" has to be provided and needs to be a string"
assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(tn,str), "\"params.tree_name\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"
assert isinstance(of,str), "\"origin_file\" is expected to be a string"
assert isinstance(tf,str), "\"target_file\" is expected to be a string"

asa = parse_friends(atn,ais,aps)

vb = "-v"+str(vb) if vb is not None else ""

if log.__len__() > 0: logcmd = f"{tee} {log.__str__()} 2>&1"
else : logcmd = ""

# parse joker arguments
joker = parse_joker(snakemake.params,["config","work_dir","tree_name","tee","sample","verbosity","min_events_per_bin","target_hist","target_file","origin_hist","origin_file"])

shell("{exe} -c {cfg} -d {wd} -i '{ifn}:{tn}' {joker} -n {meb} -o {snakemake.output[0]} -r '{of}:{oh}' -t '{tf}:{th}' {asa} {vb} {logcmd}")
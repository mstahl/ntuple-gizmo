"""
Snakemake wrapper for draw_stuff.
For script documentation see
https://gitlab.cern.ch/mstahl/ntuple-gizmo/-/blob/ca3927e178e72333c9083022ba1e667478a03dd0/doc/draw_stuff.md

The wrapper needs:
- input (list or string): an input file with histograms to draw
- params.config (str): config file for plotting
Optional arguments:
- params.work_dir (str): working directory
- params.tee (str): for logging
- params.<anything> (str or dict): used to assemble joker string to replace stuff in config file
"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["NTGROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_samples, parse_joker)

# before we do anything, check if the executable is there
exe = get_exe("draw_stuff")

samples = snakemake.input.get("sample",None)
cfg = snakemake.params.get("config", None)
wd  = snakemake.params.get("work_dir", "$RF")
tee = snakemake.params.get("tee", ">")
vb  = snakemake.params.get("verbosity", None)
log = snakemake.log

# make sure that all inputs are what we expect them to be
assert samples is not None, "\"input.sample\" is missing"
ifn = parse_samples(samples)

assert cfg is not None and isinstance(cfg,str), "\"params.config\" has to be provided and needs to be a string"
assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"

if vb is not None: vb = "-v"+str(vb)
else : vb = ""

if log.__len__() > 0: logcmd = f"{tee} {log.__str__()} 2>&1"
else : logcmd = ""

# parse joker arguments
joker = parse_joker(snakemake.params,["config","work_dir","tee"])

shell("{exe} -c {cfg} -d {wd} -i '{ifn}' {joker} {vb} {logcmd}")
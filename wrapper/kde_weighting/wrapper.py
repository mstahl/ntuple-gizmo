"""
Snakemake wrapper for kde_weighting.
For script documentation see ../../doc/kde_weighting.md

The wrapper can take several inputs from the workflow itself or from outside.
These are "sample", "target_file", "origin_file", "additional_sample".
This is to have hooks for input files that are part of the workflow and those that aren't.

The wrapper needs:
- input.origin_file or params.origin_file (string): file with the origin tree to deduce the weighting scheme
- input.target_file or params.target_file (string): file with the target tree to deduce the weighting scheme
- params.target_tree (string): name of the target tree (to weight to for deducing the weighting scheme)
- params.origin_tree (string): name of the origin tree (the one that's weighted for deducing the weighting scheme)
- params.config (string): config file, see [docs](../../doc/kde_weighting.md) on how to populate it

Optional arguments:
- input.additional_sample or params.additional_sample (list or str): files with data to apply the weighting to
- params.work_dir (str): working directory, default "$RF"
- params.tee (str): for logging, default ">"
- params.verbosity (castable to str)
- params.additional_tree_name (str) name of trees in "additional_sample" (they need to be the same in each file)

"""
__author__ = "Marian Stahl"
__email__ = "marian.stahl@cern.ch"

from snakemake.shell import shell
import sys
import os
sys.path.append(os.environ["NTGROOT"]+"/IOjuggler")
from snakemake_wrapper_utils import (get_exe, parse_friends, parse_joker)

# before we do anything, check if the executable is there
exe = get_exe("kde_weighting")

tif  = snakemake.input.get("target_file", None)
oif  = snakemake.input.get("origin_file", None)
ais  = snakemake.input.get("additional_sample",None)

cfg  = snakemake.params.get("config", None)
wd   = snakemake.params.get("work_dir", "$RF")
tt   = snakemake.params.get("target_tree", "t")
tpf  = snakemake.params.get("target_file", None)
ot   = snakemake.params.get("origin_tree", "t")
opf  = snakemake.params.get("origin_file", None)
vb   = snakemake.params.get("verbosity", None)
tee  = snakemake.params.get("tee", ">")
aps  = snakemake.params.get("additional_sample",None)
atn  = snakemake.params.get("additional_tree_name", ot)

ofn = snakemake.output[0]

log = snakemake.log

# make sure that all inputs are what we expect them to be
assert oif is not None or opf is not None, "\"input.origin_file\" or non-workflow input from \"params.origin_file\" is missing"
of = oif if oif is not None else opf
assert tif is not None or tpf is not None, "\"input.target_file\" or non-workflow input from \"params.target_file\" is missing"
tf = tif if tif is not None else tpf

assert ot is not None, "\"params.origin_tree\" is missing"
assert tt is not None, "\"params.target_tree\" is missing"

assert ofn is not None and isinstance(ofn,str), "\"output[0]\" has to be provided and needs to be a string"
assert cfg is not None and isinstance(cfg,str), "\"params.config\" has to be provided and needs to be a string"
assert isinstance(wd,str), "\"params.work_dir\" is expected to be a string"
assert isinstance(tee,str), "\"params.tee\" is expected to be a string"
assert isinstance(of,str), "\"origin_file\" is expected to be a string"
assert isinstance(tf,str), "\"target_file\" is expected to be a string"

asa = parse_friends(atn,ais,aps)

vb = "-v"+str(vb) if vb is not None else ""

if log.__len__() > 0: logcmd = f"{tee} {log.__str__()} 2>&1"
else : logcmd = ""

# parse joker arguments
joker = parse_joker(snakemake.params,["config","work_dir","tee","verbosity","target_tree","target_file","origin_tree","origin_file"])

shell("{exe} -c {cfg} -d {wd} -i '{of}:{ot}' {joker} -o {ofn} -t '{tf}:{tt}' {asa} {vb} {logcmd}")
/**
  @date:   2019-09-27
  @author: mstahl
  @brief:  script for generic plotting using boost property trees as config files.
           documentation: https://gitlab.cern.ch/sneubert/ntuple-gizmo/blob/master/config/draw_stuff.md
*/

#include <iomanip>
//ROOT libraries
#include <TAxis.h>
#include <TCanvas.h>
#include <TEfficiency.h>
#include <TF1.h>
#include <TFrame.h>
#include <TGraph.h>
#include <TGraph2D.h>
#include <TH1D.h>
#include <TKey.h>
#include <TLegend.h>
#include <TMultiGraph.h>
#include <TPad.h>
#include <TPaletteAxis.h>

#include <IOjuggler.h>
#include <lhcbStyle.C>
#include <CustomPalettes.h>
#ifdef USE_TH2A
#include <TH2A.h>
TH2A* special_dummy=nullptr;
#endif

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

namespace pt = boost::property_tree;

//TODO: think about returning them as structured binding declarations https://en.cppreference.com/w/cpp/language/structured_binding
struct special_objects {
  std::vector< std::unique_ptr<TBox> > boxs;
  std::vector< std::unique_ptr<TLatex> > ltxs;
  std::vector< std::unique_ptr<TLegend> > legs;
  std::vector< std::unique_ptr<TLine> > lines;
  std::vector< std::unique_ptr<TPaveText> > pts;
};

special_objects draw_stuff_in_canvas(const pt::ptree& canvconf, const std::vector<TFile*>& input_files, const MessageService& msgsvc);
special_objects draw_stuff_in_pads  (const pt::ptree& padconf,  const std::vector<TFile*>& input_files, const MessageService& msgsvc);
void            draw_stuff          (const pt::ptree& plotconf, TObject* obj, const MessageService& msgsvc, const std::string& new_name, double overall_max=-std::numeric_limits<double>::infinity());
void            global_setup_canv   (const pt::ptree& canvconf, const MessageService& msgsvc);
special_objects special_objects_canv(const pt::ptree& canvconf, const MessageService& msgsvc);


int main(int argc, char** argv){
  lhcbStyle();
  auto options = IOjuggler::parse_options(argc, argv, "c:d:i:j:h:v:");
  MessageService msgsvc("draw_stuff",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  const auto wd = options.get<std::string>("workdir");
  const auto ifn = options.get<std::string>("infilename");

  // runtime config file manipulations
  IOjuggler::replace_from_cmdline(configtree,options.get<std::string>("joker"));
  IOjuggler::replace_stuff_in_ptree(configtree,"{prefix}",options.get<std::string>("prefix",""),"");
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);

  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);

  auto list_of_files = IOjuggler::parse_filenames(ifn,wd,msgsvc);
  std::vector<TFile*> input_files;
  for(const auto& filename : list_of_files)
    input_files.push_back(IOjuggler::get_file(filename,wd));

  if(const auto cc = configtree.get_child_optional("Canvas")){
    for(auto& c : *cc){
      msgsvc.debugmsg("at canvas " + c.first);

      //make canvas (do it here because of optional TPads)
      TCanvas canv(c.second.get("name","canvas").data(),c.second.get("title","myCanvas").data(),
                   c.second.get("CanvXSize",2048),c.second.get("CanvYSize",1280));
      gPad->SetLeftMargin  (c.second.get("lmargin",0.12));
      gPad->SetBottomMargin(c.second.get("bmargin",0.12));
      gPad->SetRightMargin (c.second.get("rmargin",0.01));
      gPad->SetTopMargin   (c.second.get("tmargin",0.01));

      //draw stuff into canvas
      special_objects stuff;
      #ifdef USE_TH2A
        special_dummy=nullptr;
      #endif
      if(configtree.get_child_optional("Pad"))
        stuff = draw_stuff_in_pads(c.second,input_files,msgsvc);
      else {
        stuff = draw_stuff_in_canvas(c.second,input_files,msgsvc);
        //check if something was plotted at all
        bool nothing_there = true;
        for(const auto& plot : c.second.get_child("Plots")){
          if(gPad->FindObject(plot.first.data()) != nullptr){
            nothing_there = false;
            break;
          }
          #ifdef USE_TH2A
            if(special_dummy != nullptr)
              nothing_there = false;
          #endif
        }
        if(nothing_there){
          msgsvc.warningmsg("No object that could be drawn. Nothing will be saved to disk");
          continue;
        }
      }

      //save canvas
      const auto outnamestub = wd+"/"+c.second.get("output_name",c.first);
      IOjuggler::dir_exists(outnamestub);
      if(!c.second.get_child_optional("outputformats"))
        canv.SaveAs((outnamestub+".pdf").data());
      else
        for(const auto& ex : c.second.get_child("outputformats"))
          canv.SaveAs((outnamestub+"."+ex.first).data());
#ifdef USE_TH2A
      delete special_dummy;
#endif
    }
    for(const auto& inFile:input_files) if(inFile != nullptr) inFile->Close();
  }
  else if(const auto pc = configtree.get_child_optional("Plots")){//TODO: implement TPads
    const auto plot_one = *(*pc).begin();
    //TODO: loop other childs. idea: they only contain a different file, but the object's names
    //are the same, so that they all get plotted in the same canvas.
    //example: some variable for data/MC

    const auto objectname = plot_one.second.get_optional<std::string>("name");
    if(!objectname)
      throw std::runtime_error("Mandatory name in Plots child not given");
    //loop files and search for objects to draw
    for(const auto& inFile:input_files){
      //loop wildcarded objects
      auto objname = static_cast<TString>(*objectname);
      const auto objArr = objname.Tokenize("*");
      for(auto obj : *inFile->GetListOfKeys()){
        bool skip = false;
        for(int i = 0; i < objArr->GetEntries(); i++)
          skip |= !static_cast<TString>(obj->GetName()).Contains(static_cast<TObjString*>(objArr->At(i))->String().Data());
        if(skip)continue;

        msgsvc.infomsg("Requested to add "+static_cast<std::string>(obj->GetName())+" from file "+static_cast<std::string>(inFile->GetName())+" to canvas");

        TCanvas canv(configtree.get("name","canvas").data(),configtree.get("title","myCanvas").data(),
                     configtree.get("CanvXSize",2048),configtree.get("CanvYSize",1280));
        gPad->SetLeftMargin  (configtree.get("lmargin",0.12));
        gPad->SetBottomMargin(configtree.get("bmargin",0.12));
        gPad->SetRightMargin (configtree.get("rmargin",0.01));
        gPad->SetTopMargin   (configtree.get("tmargin",0.01));

        //some more configuration of the canvas
        global_setup_canv(configtree,msgsvc);

        //draw stuff into canvas
        draw_stuff(plot_one.second,obj,msgsvc,plot_one.first);

        //add legends, texts etc, and return a vector of unique_ptrs containing them
        auto stuff = special_objects_canv(configtree,msgsvc);

        //save canvas
        auto outnamestub = wd+"/"+configtree.get<std::string>("output_name_prefix","") +
                           configtree.get<std::string>("output_name",obj->GetName()) +
                           configtree.get<std::string>("output_name_suffix","");
        IOjuggler::dir_exists(outnamestub);
        if(auto opfs = configtree.get_child_optional("outputformats"))
          for(const auto& ex : *opfs)
            canv.SaveAs((outnamestub + "." + ex.first).data());
        else
          canv.SaveAs((outnamestub + ".pdf").data());

#ifdef USE_TH2A
        delete special_dummy;
#endif
      }
    }
  }
  else throw std::runtime_error("Mandatory child Canvas or Plots not given");
}

special_objects draw_stuff_in_pads(const pt::ptree& padconf, const std::vector<TFile*>& input_files, const MessageService& msgsvc){
  special_objects stuff_going_out_of_scope;
  for(auto& pad : padconf.get_child("Pad")){
    msgsvc.debugmsg("at pad " + pad.first);
    TPad p(pad.second.get("name","pad").data(),pad.second.get("title","myPad").data(),
           pad.second.get("xlo",0.0),
           pad.second.get("ylo",0.0),
           pad.second.get("xhi",1.0),
           pad.second.get("yhi",1.0));
    p.cd();
    auto stuff_from_pad = draw_stuff_in_canvas(pad.second,input_files,msgsvc);
    for(auto& box: stuff_from_pad.boxs)
      stuff_going_out_of_scope.boxs.push_back(std::move(box));
    for(auto& ltx: stuff_from_pad.ltxs)
      stuff_going_out_of_scope.ltxs.push_back(std::move(ltx));
    for(auto& leg: stuff_from_pad.legs)
      stuff_going_out_of_scope.legs.push_back(std::move(leg));
    for(auto& line: stuff_from_pad.lines)
      stuff_going_out_of_scope.lines.push_back(std::move(line));
    for(auto& pt: stuff_from_pad.pts)
      stuff_going_out_of_scope.pts.push_back(std::move(pt));
  }
  return stuff_going_out_of_scope;
}

special_objects draw_stuff_in_canvas(const pt::ptree& canvconf, const std::vector<TFile*>& input_files, const MessageService& msgsvc){

  //setup global stuff for this canvas
  global_setup_canv(canvconf,msgsvc);
  //draw plots before legends etc.
  //individual plots. different types can be mixed
  auto opt_plotnode = canvconf.get_child_optional("Plots");
  if(!opt_plotnode) throw std::runtime_error("Mandatory child Plots not given");
  // set scale for all plots based on the "largest" one
  auto overall_max = -std::numeric_limits<double>::infinity();
  if((*opt_plotnode).size() > 1){
    for(const auto& plot : *opt_plotnode){
      TObject* htmp = nullptr;
      TClass* cl = nullptr;
      const auto tmpplotname = plot.second.get("name",plot.first);
      for(const auto& inFile:input_files){
        htmp = inFile->FindObjectAny(tmpplotname.data());
        // TODO: worry about minimum and non TH1 objects once they are needed
        if(htmp != nullptr){
          cl = gROOT->GetClass(htmp->ClassName());
          if(cl->InheritsFrom("TKey")){
            cl = gROOT->GetClass(static_cast<TKey*>(htmp)->GetClassName());
            htmp = static_cast<TKey*>(htmp)->ReadObj();//otherwise: seg-fault...
          }
          if(cl != nullptr && cl->InheritsFrom("TH1")) {
            const auto hist = static_cast<TH1*>(htmp);
            auto tmp_max = hist->GetMaximum();
            if(plot.second.get_optional<double>("Norm")) tmp_max /= hist->GetSumOfWeights();
            if(auto s = plot.second.get_optional<double>("Scale")) tmp_max *= *s;
            if(plot.second.get_optional<std::string>("Scale_to") || plot.second.get_optional<std::string>("Scale_to_max"))
              msgsvc.warningmsg("Sorry, I was to lazy to set scale based on all plots for the \"Scale_to[_max]\" options");
            msgsvc.debugmsg("Max of "+tmpplotname+" is "+std::to_string(tmp_max));
            if(tmp_max > overall_max) overall_max = tmp_max;
          }
        }
      }
    }
  }
  // and now plot them...
  for(const auto& plot : *opt_plotnode){
    msgsvc.debugmsg("at plot " + plot.first);
    const auto plotname = plot.second.get("name",plot.first);
    msgsvc.debugmsg("Requested to draw "+plotname);
    for(const auto& inFile:input_files){
      //find the plottable object in file. problem: it's of type TObject*
      //i tried a bit, but there is no way to declare types at runtime,
      //such that you would be able to cast back to TH1*, TGraph* or TMultiGraph*.
      //maybe boost::variant can do it, but i didn't see the way there
      //instead, deduce the type manually and call templated lambdas to define the plotting style
      auto obj = inFile->FindObjectAny(plotname.data());
      if(obj == nullptr){
        msgsvc.debugmsg(plotname+" is nullptr. Going to next plot/file");
        continue;
      }
      draw_stuff(plot.second,obj,msgsvc,plot.first,overall_max);
    }
  }

  //add legends, texts etc, and return a vector of unique_ptrs containing them
  return special_objects_canv(canvconf,msgsvc);
}

void global_setup_canv(const pt::ptree& canvconf, const MessageService& msgsvc){
  if(canvconf.get_optional<bool>("logx"))gPad->SetLogx();
  if(canvconf.get_optional<bool>("logy"))gPad->SetLogy();
  if(canvconf.get_optional<bool>("logz"))gPad->SetLogz();
  if(auto nconts = canvconf.get_optional<int>("NumberContours"))gStyle->SetNumberContours(*nconts);
  if(auto pltix = canvconf.get_optional<int>("Palette")){
    if(*pltix >= 113){
      try { set_CustomPalette(*pltix); }
      catch(std::exception& e){
        msgsvc.warningmsg("caught exception: " + std::string(e.what()));
        msgsvc.warningmsg("Setting Palette 56 to be able to continue");
        gStyle->SetPalette(56);
      }
    }
    else gStyle->SetPalette(*pltix);
  }
  if(auto mdigs = canvconf.get_optional<int>("MaxDigits"))TGaxis::SetMaxDigits(*mdigs);
  if(auto hlw = canvconf.get_optional<int>("HatchesLineWidth"))gStyle->SetHatchesLineWidth(*hlw);
  if(auto htsp = canvconf.get_optional<double>("HatchesSpacing"))gStyle->SetHatchesSpacing(*htsp);
  if(canvconf.get_child_optional("ExponentOffsetX")) TGaxis::SetExponentOffset(canvconf.get<double>("ExponentOffsetX.OffsetX",0),canvconf.get<double>("ExponentOffsetX.OffsetY",-0.06),"x");
  if(canvconf.get_child_optional("ExponentOffsetY")) TGaxis::SetExponentOffset(canvconf.get<double>("ExponentOffsetY.OffsetX",-0.01),canvconf.get<double>("ExponentOffsetY.OffsetY",0.01),"y");
}

special_objects special_objects_canv(const pt::ptree& canvconf, const MessageService& msgsvc){
  //push legends pavetexts etc. into a vector, so they are not out of scope when saving the canvas
  special_objects stuff_going_out_of_scope;

  if(const auto& bxs = canvconf.get_child_optional("boxes")){
    gPad->Update();
    for(const auto& bx : *bxs) {
      msgsvc.debugmsg("Adding TBox " + bx.first);
      std::unique_ptr<TBox> box(new TBox);
      box->SetX1(bx.second.get<double>("x1",gPad->GetFrame()->GetX1()));
      box->SetX2(bx.second.get<double>("x2",gPad->GetFrame()->GetX2()));
      box->SetY1(bx.second.get<double>("y1",gPad->GetFrame()->GetY1()));
      box->SetY2(bx.second.get<double>("y2",gPad->GetFrame()->GetY2()));
      box->SetFillColorAlpha(LHCb::color(bx.second.get("FillColor","white")),
                             bx.second.get("FillColorAlpha",1.f));
      box->SetFillStyle(bx.second.get("FillStyle",1001));
      box->SetLineColorAlpha(LHCb::color(bx.second.get("LineColor","black")),
                             bx.second.get("LineColorAlpha",1.f));
      box->SetLineStyle(bx.second.get("LineStyle",1));
      box->SetLineWidth(bx.second.get("LineWidth",1));
      stuff_going_out_of_scope.boxs.push_back(std::move(box));
    }
  }

  if(const auto& lxs = canvconf.get_child_optional("latexts")){
    for(const auto& lx : *lxs) {
      msgsvc.debugmsg("Adding TLatex " + lx.first);
      std::unique_ptr<TLatex> ltx(new TLatex);
      if(!(lx.second.get_optional<double>("X") && lx.second.get_optional<double>("Y") &&
           lx.second.get_optional<std::string>("Text"))){
        msgsvc.warningmsg("Mandatory TLatex parameters not given");
        continue;
      }
      ltx->SetNDC();
      ltx->SetText(lx.second.get<double>("X"),lx.second.get<double>("Y"),
                   lx.second.get<std::string>("Text").data());
      ltx->SetTextAlign(lx.second.get<int>("TextAlign",11));
      ltx->SetTextAngle(lx.second.get<float>("TextAngle",0.f));
      ltx->SetTextColor(LHCb::color(lx.second.get("TextColor","black")));
      if(lx.second.get_optional<int>("TextFont"))
        ltx->SetTextFont(lx.second.get<int>("TextFont"));
      if(lx.second.get_optional<float>("TextSize"))
        ltx->SetTextSize(lx.second.get<float>("TextSize"));
      stuff_going_out_of_scope.ltxs.push_back(std::move(ltx));
    }
  }

  if(const auto& legs = canvconf.get_child_optional("legends")){
    for(const auto& l : *legs) {
      msgsvc.debugmsg("Adding TLegend " + l.first);
      std::unique_ptr<TLegend> leg(new TLegend);
      if(!((l.second.get_optional<double>("xlo") && l.second.get_optional<double>("dtop") &&
            l.second.get_optional<double>("height") && l.second.get_optional<double>("dright"))
           ||(l.second.get_optional<double>("xlo") && l.second.get_optional<double>("xhi") &&
              l.second.get_optional<double>("ylo") && l.second.get_optional<double>("yhi")))){
        msgsvc.warningmsg("Mandatory TLegend parameters not given");
        continue;//go to the next 'l'
      }

      if(const auto& leghead = l.second.get_optional<std::string>("header"))
        leg->SetHeader((*leghead).data());
      leg->SetX1(l.second.get<double>("xlo"));
      if(l.second.get_optional<double>("xhi")){
        leg->SetX2(l.second.get<double>("xhi"));
        leg->SetY1(l.second.get<double>("ylo"));
        leg->SetY2(l.second.get<double>("yhi"));
      }
      else{
        leg->SetY1(1-gPad->GetTopMargin()-l.second.get<double>("dtop")-
                   (l.second.get<double>("height")*l.second.get<double>("TextSize",gStyle->GetTextSize())));
        leg->SetX2(1-gPad->GetRightMargin()-l.second.get<double>("dright"));
        leg->SetY2(1-gPad->GetTopMargin()-l.second.get<double>("dtop"));
      }
      leg->SetBorderSize(l.second.get<int>("BorderSize",0));
      leg->SetFillColorAlpha(LHCb::color(l.second.get("FillColor","white")),l.second.get("FillColorAlpha",1.f));
      leg->SetFillStyle(static_cast<Style_t>(l.second.get<int>("FillStyle",0)));
      leg->SetTextAlign(l.second.get<short>("TextAlign",12));
      leg->SetTextAngle(l.second.get<float>("TextAngle",0.f));
      leg->SetTextSize(l.second.get<double>("TextSize",gStyle->GetTextSize()));
      leg->SetMargin(l.second.get<double>("Margin",leg->GetMargin()));
      for(const auto& v : l.second){
        if(const auto leglab = v.second.get_optional<std::string>("label")){
          const auto obn =  v.second.get("name",v.first);
          if(gPad->FindObject(obn.data()) == nullptr){
            msgsvc.warningmsg("Could not find "+obn+" in current pad. "
                                                    "Did you draw following objects with \"same\"?");
            continue;//go to the next 'v'
          }
          leg->AddEntry(obn.data(),(*leglab).data(),v.second.get<std::string>("option","lpf").data());
        }
      }
      stuff_going_out_of_scope.legs.push_back(std::move(leg));
    }
  }

  if(const auto& lins = canvconf.get_child_optional("lines")){
    for(const auto& l : *lins) {
      msgsvc.debugmsg("Adding TLine " + l.first);
      gPad->Update();
      std::unique_ptr<TLine> line(new TLine(l.second.get<double>("xlo",gPad->GetFrame()->GetX1()),
                                            l.second.get<double>("ylo",gPad->GetFrame()->GetY1()),
                                            l.second.get<double>("xhi",gPad->GetFrame()->GetX2()),
                                            l.second.get<double>("yhi",gPad->GetFrame()->GetY2())));
      line->SetLineStyle(l.second.get("style",2));
      line->SetLineWidth(l.second.get("width",1));
      line->SetLineColor(LHCb::color(l.second.get("color","black")));
      line->SetDrawOption(l.second.get("option","l").data());
      stuff_going_out_of_scope.lines.push_back(std::move(line));
    }
  }

  if(const auto& pts = canvconf.get_child_optional("pavetexts")){
    for(const auto& p : *pts) {
      msgsvc.debugmsg("Adding TPaveText " + p.first);
      std::unique_ptr<TPaveText> pt(new TPaveText);
      if(!((p.second.get_optional<double>("xlo") && p.second.get_optional<double>("dtop") &&
            p.second.get_optional<double>("height") && p.second.get_optional<double>("dright"))
           ||(p.second.get_optional<double>("xlo") && p.second.get_optional<double>("xhi") &&
              p.second.get_optional<double>("ylo") && p.second.get_optional<double>("yhi")))){
        msgsvc.warningmsg("Mandatory TPaveText parameters not given");
        continue;//go to the next 'p'
      }
      pt->SetX1NDC(p.second.get<double>("xlo"));
      if(p.second.get_optional<double>("xhi")){
        pt->SetX2NDC(p.second.get<double>("xhi"));
        pt->SetY1NDC(p.second.get<double>("ylo"));
        pt->SetY2NDC(p.second.get<double>("yhi"));
      }
      else{
        pt->SetY1NDC(1-gPad->GetTopMargin()-p.second.get<double>("dtop")-
                     (p.second.get<double>("height")*p.second.get<double>("TextSize",gStyle->GetTextSize())));
        pt->SetX2NDC(1-gPad->GetRightMargin()-p.second.get<double>("dright"));
        pt->SetY2NDC(1-gPad->GetTopMargin()-p.second.get<double>("dtop"));
      }
      pt->SetBorderSize(p.second.get<int>("BorderSize",0));
      pt->SetFillColorAlpha(LHCb::color(p.second.get("FillColor","white")),p.second.get("FillColorAlpha",1.f));
      pt->SetFillStyle(static_cast<Style_t>(p.second.get<int>("FillStyle",0)));
      pt->SetTextAlign(p.second.get<short>("TextAlign",12));
      pt->SetTextAngle(p.second.get<float>("TextAngle",0.f));
      pt->SetTextSize(p.second.get<double>("TextSize",gStyle->GetTextSize()));
      if(const auto& txs = p.second.get_child_optional("AddText"))
        //lambda to convert stuff to a string and set precision
        for(const auto& v : *txs){
          auto to_string_with_precision = [] (const auto& value, const int n = 6){
            std::ostringstream out;
            out << std::fixed;
            out << std::setprecision(n) << value;
            return out.str();
          };
          if(v.first == "KSTest"){
            for(const auto& kskvp : p.second.get_child("AddText.KSTest")){
              if(!(kskvp.second.get_optional<std::string>("comparand") &&
                   kskvp.second.get_optional<std::string>("text"))){
                msgsvc.warningmsg("Mandatory parameters to do a Kolmogorov-Smirnov Test are not given");
                continue;
              }
              //get the histograms(!)
              auto h1 = static_cast<TH1*>(gPad->FindObject(kskvp.first.data()));
              auto h2 = static_cast<TH1*>(gPad->FindObject(kskvp.second.get<std::string>("comparand").data()));
              if(h1 == nullptr || h2 == nullptr){
                msgsvc.errormsg(kskvp.first+" or "+kskvp.second.get<std::string>("comparand")+
                                " is a nullptr");
                continue;//go to the next 'v'
              }
              //get Kolmogorov test score
              auto KS_test_score = h1->KolmogorovTest(h2,kskvp.second.get<std::string>("opt","").data());
              //convert to string
              auto stKS = to_string_with_precision(KS_test_score,kskvp.second.get<int>("precision",3));
              //boost manipulates the string and needs a l-value
              auto text = kskvp.second.get<std::string>("text");
              boost::algorithm::replace_all(text,"#",stKS);
              pt->AddText(text.data());
            }
          }
          else if (v.first == "Integral"){
            for(const auto& intkvp : p.second.get_child("AddText.Integral")){
              if(!(intkvp.second.get_optional<std::string>("text"))){
                msgsvc.warningmsg("Mandatory key \"text\" to print the result "
                                  "of the integral is not given");
                continue;
              }
              //get the histogram(!)
              auto h1 = static_cast<TH1*>(gPad->FindObject(intkvp.first.data()));
              if(h1 == nullptr){
                msgsvc.errormsg(intkvp.first + " is a nullptr");
                continue;
              }
              //get the integral
              auto h1_integral = h1->Integral(intkvp.second.get<int>("first_bin",1),
                                              intkvp.second.get<int>("last_bin",h1->GetNbinsX()*h1->GetNbinsY()*h1->GetNbinsZ()),
                                              intkvp.second.get<std::string>("opt","").data());
              //convert to string
              auto integral = to_string_with_precision(h1_integral,intkvp.second.get<int>("precision",3));
              //boost manipulates the string and needs a l-value
              auto text = intkvp.second.get<std::string>("text");
              boost::algorithm::replace_all(text,"#",integral);
              pt->AddText(text.data());
            }
          }
          else
            pt->AddText(v.first.data());
        }
      stuff_going_out_of_scope.pts.push_back(std::move(pt));
    }
  }

  //draw objects which have been protected from going out of scope
  auto draw_special_objects = [] (const auto& vsob) {
    for(const auto& sob : vsob)
      if(sob!=nullptr)
        sob->Draw();
  };
  draw_special_objects(stuff_going_out_of_scope.boxs);
  draw_special_objects(stuff_going_out_of_scope.ltxs);
  draw_special_objects(stuff_going_out_of_scope.legs);
  draw_special_objects(stuff_going_out_of_scope.lines);
  draw_special_objects(stuff_going_out_of_scope.pts);

  if(const auto& overlay_str = canvconf.get_optional<std::string>("overlay")){
    for(const auto& plot : canvconf.get_child("Plots")){
      auto htmp = gPad->FindObject(plot.first.data());
      if(htmp == nullptr){
        msgsvc.warningmsg("overlay - Could not find " + plot.first +
                          " in current pad. Did you draw folling objects with \"same\"?");
        if(canvconf.get<std::string>("overlay") != "all") continue;
        else break;
      }
      auto dopt = plot.second.get<std::string>("drawing_opt","");
      boost::algorithm::to_upper(dopt);
      if(dopt == "BOX1D") continue;
      if(*overlay_str != "axis"){
        if(boost::algorithm::ends_with(dopt,"SAME")) htmp->Draw(dopt.data());
        else htmp->Draw((dopt+"SAME").data());
      }
      htmp->Draw("AXISSAME");
      if(*overlay_str != "all")
        break;
    }
  }
  return stuff_going_out_of_scope;
}

void draw_stuff(const pt::ptree& plotconf, TObject* obj, const MessageService& msgsvc, const std::string& new_name, double overall_max){

  //lambdas to configure axes. TGraph was making problems with the Zaxis, so this had to be split in 1 and 2D...
  //TMuligraph can't do much, so this requires another split
  //the following lambda is common to both versions
  auto rescale_axis = [&msgsvc] (TAxis* axis, const pt::ptree& ax, auto plot_obj) {
    //parse minimum and maximum from plot
    if( (ax.get_optional<double>("min") && ax.get_optional<double>("max"))
        || ax.get_optional<bool>("parse_range") ){
      //rescale in user defined way or parse range,
      //in this way it can still be user defined, but also scaled with minsub and maxadd
      auto min = 0.f, max = 1.f;
      //rescale a logscale plot. this only works for y and z axis (1 or 2D) and assumes nothing too freaky,
      //i.e. 1 D plots with logz or 2D plots with logy and linear z
      if(gPad->GetLogy() || gPad->GetLogz()){
        max = std::pow(10,std::log10(plot_obj->GetMaximum())+
                       ax.get("maxadd",0.1)*(std::log10(plot_obj->GetMaximum())-
                                             std::log10(ax.get("min",plot_obj->GetMinimum()))));
        min = std::pow(10,std::log10(plot_obj->GetMinimum())-
                       ax.get("minsub",0.1)*(std::log10(ax.get("max",plot_obj->GetMaximum()))
                                             -std::log10(plot_obj->GetMinimum())));
      }
      else {//linear plot
        max = plot_obj->GetMaximum() + ax.get("maxadd",0.1)*(plot_obj->GetMaximum()-
                                                             ax.get("min",plot_obj->GetMinimum()));
        min = plot_obj->GetMinimum() - ax.get("minsub",0.0)*(ax.get("max",plot_obj->GetMaximum())-
                                                             plot_obj->GetMinimum());
      }
      msgsvc.debugmsg("Setting range of axis to ["+std::to_string(ax.get("min",min))+","+std::to_string(ax.get("max",max))+"]");
      axis->SetRangeUser(ax.get("min",min),ax.get("max",max));
    }
  };

  auto config_single_axis = [] (TAxis*& axis, const pt::ptree& ax) {
    axis->SetLabelOffset(ax.get("LabelOffset",axis->GetLabelOffset()));
    axis->SetLabelSize  (ax.get("LabelSize",axis->GetLabelSize()));
    axis->SetNdivisions (ax.get("Ndivisions",axis->GetNdivisions()));
    axis->SetTickLength (ax.get("TickLength",axis->GetTickLength()));
    axis->SetTickSize   (ax.get("TickSize",0.03));
    axis->SetTitle      (ax.get("Title",static_cast<std::string>(axis->GetTitle())).data());
    axis->SetTitleOffset(ax.get("TitleOffset",axis->GetTitleOffset()));
    axis->SetTitleSize  (ax.get("TitleSize",axis->GetTitleSize()));
  };

  const auto opt_ax_child = plotconf.get_child_optional("axes");

  auto conf_mgaxes = [&plotconf,&opt_ax_child,&config_single_axis,&msgsvc] (auto plot_obj) {
    if(opt_ax_child){
      msgsvc.debugmsg("Configuring axes");
      for(const auto& ax : *opt_ax_child) {
        TAxis* axis;
        if      (ax.first == "X")axis = plot_obj->GetXaxis();
        else if (ax.first == "Y")axis = plot_obj->GetYaxis();
        else msgsvc.warningmsg("There is no " + ax.first + " axis in " + std::string(plot_obj->GetName()));
        config_single_axis(axis,ax.second);
        axis->SetRangeUser(ax.second.get("min",0.f),ax.second.get("max",1.f));
      }
    }
  };

  auto conf_2Daxes = [&plotconf,&opt_ax_child,&config_single_axis,&rescale_axis,&msgsvc] (auto plot_obj) {
    if(opt_ax_child){
      msgsvc.debugmsg("Configuring axes");
      for(const auto& ax : *opt_ax_child) {
        TAxis* axis;
        if      (ax.first == "X")axis = plot_obj->GetXaxis();
        else if (ax.first == "Y")axis = plot_obj->GetYaxis();
        else msgsvc.warningmsg("There is no " + ax.first + " axis in " + std::string(plot_obj->GetName()));
        config_single_axis(axis,ax.second);
        rescale_axis(axis,ax.second,plot_obj);
      }
    }
  };

  auto conf_axes = [&plotconf,&opt_ax_child,&config_single_axis,&rescale_axis,&msgsvc] (auto plot_obj) {
    if(opt_ax_child){
      msgsvc.debugmsg("Configuring axes");
      for(const auto& ax : *opt_ax_child) {
        TAxis* axis;
        if      (ax.first == "X")axis = plot_obj->GetXaxis();
        else if (ax.first == "Y")axis = plot_obj->GetYaxis();
        else if (ax.first == "Z")axis = plot_obj->GetZaxis();
        else if (ax.first == "PaletteAxis") msgsvc.debugmsg("Will configure PaletteAxis after drawing");
        else msgsvc.warningmsg("There is no " + ax.first + " axis in " + std::string(plot_obj->GetName()));
        config_single_axis(axis,ax.second);
        rescale_axis(axis,ax.second,plot_obj);
      }
    }
  };

  auto conf_palette_axes = [&plotconf,&msgsvc] (auto plot_obj) {
    auto pal = static_cast<TPaletteAxis*>(plot_obj->FindObject("palette"));
    if(pal == nullptr){
      msgsvc.warningmsg("Could not find TPaletteAxis \"palette\" in " + std::string(plot_obj->GetName()));
      return;
    }
    //somehow all this labelstuff doesn't work...
    if(plotconf.get_optional<std::string>("axes.PaletteAxis.LabelColor"))
      pal->SetLabelColor(LHCb::color(plotconf.get<std::string>("axes.PaletteAxis.LabelColor")));
    if(plotconf.get_optional<int>("axes.PaletteAxis.LabelFont"))
      pal->SetLabelFont(plotconf.get<int>("axes.PaletteAxis.LabelFont"));
    if(plotconf.get_optional<float>("axes.PaletteAxis.LabelOffset"))
      pal->SetLabelOffset(plotconf.get<float>("axes.PaletteAxis.LabelOffset"));
    if(plotconf.get_optional<double>("axes.PaletteAxis.margin"))
      pal->SetX1NDC(1 - gPad->GetRightMargin() + plotconf.get<double>("axes.PaletteAxis.margin"));
    if(plotconf.get_optional<double>("axes.PaletteAxis.width"))
      pal->SetX2NDC(pal->GetX1NDC() + plotconf.get<double>("axes.PaletteAxis.width"));
    pal->SetY1NDC(plotconf.get("axes.PaletteAxis.Y1NDC",pal->GetY1NDC()));
    pal->SetY2NDC(plotconf.get("axes.PaletteAxis.Y2NDC",pal->GetY2NDC()));
  };

  //lambda to configure basic TAttLine, TAttMarker, TAttFill attributes
  auto conf_attributes = [&plotconf,&msgsvc] (auto plot_obj) {
    msgsvc.debugmsg("Configuring attributes");
    if(plotconf.get_optional<std::string>("FillColor"))
      plot_obj->SetFillColorAlpha(LHCb::color(plotconf.get<std::string>("FillColor")),
                                  plotconf.get("FillColorAlpha",1.f));
    else
      plot_obj->SetFillColorAlpha(plot_obj->GetFillColor(),plotconf.get("FillColorAlpha",1.f));
    plot_obj->SetFillStyle(plotconf.get("FillStyle",plot_obj->GetFillStyle()));
    if(plotconf.get_optional<std::string>("LineColor"))
      plot_obj->SetLineColorAlpha(LHCb::color(plotconf.get<std::string>("LineColor")),
                                  plotconf.get("LineColorAlpha",1.f));
    else
      plot_obj->SetLineColorAlpha(plot_obj->GetLineColor(),plotconf.get("LineColorAlpha",1.f));
    plot_obj->SetLineStyle(plotconf.get("LineStyle",plot_obj->GetLineStyle()));
    plot_obj->SetLineWidth(plotconf.get("LineWidth",plot_obj->GetLineWidth()));
    if(plotconf.get_optional<std::string>("MarkerColor"))
      plot_obj->SetMarkerColorAlpha(LHCb::color(plotconf.get<std::string>("MarkerColor")),
                                    plotconf.get("MarkerColorAlpha",1.f));
    else
      plot_obj->SetMarkerColorAlpha(plot_obj->GetMarkerColor(),plotconf.get("MarkerColorAlpha",1.f));
    plot_obj->SetMarkerStyle(plotconf.get("MarkerStyle",plot_obj->GetMarkerStyle()));
    plot_obj->SetMarkerSize(plotconf.get("MarkerSize",plot_obj->GetMarkerSize()));
    plot_obj->SetTitle(plotconf.get("Title",plot_obj->GetTitle()).data());
  };

  const auto objname = static_cast<std::string>(obj->GetName());
  //cast back to (plot-wise configurable base class of) original type and pass it to the lambdas above
  TClass* cl = gROOT->GetClass(obj->ClassName());
  if(cl->InheritsFrom("TKey")){
    cl = gROOT->GetClass(static_cast<TKey*>(obj)->GetClassName());
    obj = static_cast<TKey*>(obj)->ReadObj();//otherwise: seg-fault...
  }
  if(cl->InheritsFrom("TH1")){
    msgsvc.debugmsg("Deduced " + objname + " as TH1");
    auto hist = static_cast<TH1*>(obj);
    hist->SetName(new_name.data());

    if(plotconf.get_optional<double>("Norm")) hist->Scale(1.f/hist->GetSumOfWeights());
    if(auto const s = plotconf.get_optional<double>("Scale")) hist->Scale(*s);
    if(auto const scale_to_what = plotconf.get_optional<std::string>("Scale_to"))
      if(auto scale_hist = static_cast<TH1*>(gPad->FindObject((*scale_to_what).data())); scale_hist != nullptr){
        hist->Sumw2();
        hist->Scale(scale_hist->GetSumOfWeights()/hist->GetSumOfWeights());
      }
      else msgsvc.errormsg("Can't scale "+objname+" because "+*scale_to_what+" can't be found in the current pad");
    if(auto const scale_to_what = plotconf.get_optional<std::string>("Scale_to_max"))
      if(auto scale_hist = static_cast<TH1*>(gPad->FindObject((*scale_to_what).data())); scale_hist != nullptr)
        hist->Scale(scale_hist->GetBinContent(scale_hist->GetMaximumBin())/hist->GetBinContent(hist->GetMaximumBin()));
      else msgsvc.errormsg("Can't scale "+objname+" because "+*scale_to_what+" can't be found in the current pad");

    if(auto const s = plotconf.get_optional<std::string>("Smooth"))hist->Smooth(1,(*s).data());

    // scaling will have reset the value retrieved by the GetMaximum function. Set them to the global one...
    auto current_max_axis = 0.;
    if(overall_max>-std::numeric_limits<double>::infinity()) {
      current_max_axis = hist->GetMaximum();
      hist->SetMaximum(overall_max);
    }

    conf_axes(hist);
    conf_attributes(hist);

    //special drawing options for TH1s
    gStyle->SetEndErrorSize(plotconf.get("EndErrorSize",3*plotconf.get("MarkerSize",hist->GetMarkerSize())));
    auto drawing_opt = plotconf.get("drawing_opt","e1p");
    boost::algorithm::to_upper(drawing_opt);
    if(drawing_opt == "BOX1D" && hist->GetDimension() == 1){
      auto ncpdbins = hist->GetXaxis()->GetNbins();
      for (decltype(ncpdbins) xbin = 1; xbin <= ncpdbins; xbin++ ){
        if(hist->GetBinContent(xbin) == 0)continue;

        auto xlo = hist->GetXaxis()->GetBinLowEdge(xbin);
        auto xhi = hist->GetXaxis()->GetBinUpEdge(xbin);
        auto ylo = hist->GetBinContent(xbin)-hist->GetBinError(xbin);
        auto yhi = hist->GetBinContent(xbin)+hist->GetBinError(xbin);

        TBox binbox(xlo,ylo,xhi,yhi);
        binbox.SetFillColor(hist->GetFillColor());
        binbox.SetFillStyle(hist->GetFillStyle());
        binbox.SetLineColor(hist->GetLineColor());
        binbox.DrawClone("SAME");

        TLine lline(xlo,ylo,xlo,yhi);
        TLine bline(xlo,ylo,xhi,ylo);
        TLine rline(xhi,ylo,xhi,yhi);
        TLine tline(xlo,yhi,xhi,yhi);
        auto set_line_attributes = [hist] (TLine& tl){
          tl.SetLineColor(hist->GetLineColor());
          tl.SetLineWidth(hist->GetLineWidth());
          tl.DrawClone("SAME");
        };
        set_line_attributes(lline);
        set_line_attributes(bline);
        set_line_attributes(rline);
        set_line_attributes(tline);
      }
      hist->DrawClone("AXISSAME");//it needs to be added to the list of primitives, but i don't know how to plot it invisible
    }
    else
      hist->DrawClone(drawing_opt.data()); //draw clone as resetting the maximum below would overwrite the axis range
    //reconfigure PaletteAxis
    if(plotconf.get_child_optional("axes.PaletteAxis")){
      gPad->Update();
      conf_palette_axes(hist);
    }
    // reset for the next plot
    if(overall_max>-std::numeric_limits<double>::infinity()) hist->SetMaximum(current_max_axis);
  }
  else if(cl->InheritsFrom("TF1")){
    msgsvc.debugmsg("Deduced " + objname + " as TF1");
    auto func = static_cast<TF1*>(obj);
    func->SetName(new_name.data());
    conf_axes(func);
    conf_attributes(func);
    func->Draw(plotconf.get("drawing_opt","c").data());
  }
  else if(cl->InheritsFrom("TGraph2D")){
    msgsvc.debugmsg("Deduced " + objname + " as TGraph2D");
    auto tg2 = static_cast<TGraph2D*>(obj);
    tg2->SetName(new_name.data());
    conf_axes(tg2);
    conf_attributes(tg2);
    tg2->Draw(plotconf.get("drawing_opt","cont4z").data());
    if(plotconf.get_child_optional("axes.PaletteAxis")){
      gPad->Update();
      conf_palette_axes(tg2);
    }
  }
  else if(cl->InheritsFrom("TGraph")){
    msgsvc.debugmsg("Deduced " + objname + " as TGraph");
    auto gr = static_cast<TGraph*>(obj);
    gr->SetName(new_name.data());
    conf_2Daxes(gr);
    conf_attributes(gr);
    gr->Draw(plotconf.get("drawing_opt","ac").data());
  }
  else if(cl->InheritsFrom("TEfficiency")){
    msgsvc.debugmsg("Deduced " + objname + " as TEfficiency");
    msgsvc.infomsg("TEfficiency does not allow to manipulate axes. If this is desired, "
                   "please make a TH1 derived object from it");
    auto eff = static_cast<TEfficiency*>(obj);
    eff->SetName(new_name.data());
    conf_attributes(eff);
    eff->Draw(plotconf.get("drawing_opt","ap").data());
    if(plotconf.get_child_optional("axes.PaletteAxis")){
      gPad->Update();
      conf_palette_axes(eff);
    }
  }
  else if(cl->InheritsFrom("TMultiGraph")){
    msgsvc.debugmsg("Deduced " + objname + " as TMultiGraph");
    msgsvc.infomsg("You are using TMultiGraph. This only allows to configure the canvas, "
                   "part of the axes and adding labels");
    auto mg = static_cast<TMultiGraph*>(obj);
    mg->SetName(new_name.data());
    conf_mgaxes(mg);
    mg->SetTitle(plotconf.get("title",static_cast<std::string>(mg->GetTitle())).data());
    mg->Draw(plotconf.get("drawing_opt","ac").data());
  }
#ifdef USE_TH2A
  else if(cl->InheritsFrom("TH2A")){
    msgsvc.debugmsg("Deduced " + objname + " as TH2A");
    special_dummy = static_cast<TH2A*>(obj);
    special_dummy->SetName(new_name.data());
    if(plotconf.get_optional<double>("axes.X.min") || plotconf.get_optional<double>("axes.X.max"))
      special_dummy->SetXRange(plotconf.get("axes.X.min",special_dummy->GetXMin()),
                               plotconf.get("axes.X.max",special_dummy->GetXMax()));
    if(plotconf.get_optional<double>("axes.Y.min") || plotconf.get_optional<double>("axes.Y.max"))
      special_dummy->SetYRange(plotconf.get("axes.Y.min",special_dummy->GetYMin()),
                               plotconf.get("axes.Y.max",special_dummy->GetYMax()));
    if(plotconf.get_optional<double>("axes.Z.min") || plotconf.get_optional<double>("axes.Z.max"))
      special_dummy->SetZRange(plotconf.get("axes.Z.min",special_dummy->GetZMin()),
                               plotconf.get("axes.Z.max",special_dummy->GetZMax()));
    if(plotconf.get_optional<int>("LineColor"))TH2A::SetLineColor(plotconf.get<int>("LineColor"));
    if(plotconf.get_optional<short>("LineWidth"))TH2A::SetLineWidth(plotconf.get<int>("LineWidth"));
    special_dummy->Draw();
    if(plotconf.get_child_optional("DPBoundary"))
      special_dummy->DrawDPBoundary(plotconf.get("DPBoundary.mother",5619.58),
                                    plotconf.get("DPBoundary.d1",2286.46),
                                    plotconf.get("DPBoundary.d2",1864.83),
                                    plotconf.get("DPBoundary.d3",493.677),
                                    plotconf.get<int>("DPBoundary.nbins",1e+4),
                                    plotconf.get("DPBoundary.offsetX",0.0),
                                    plotconf.get("DPBoundary.offsetY",0.0));
    auto dummy_hist = static_cast<TH2D*>(gROOT->FindObjectAnyFile("dummy"));
    conf_axes(dummy_hist);
    if(plotconf.get_child_optional("axes.PaletteAxis"))
      conf_palette_axes(dummy_hist);
    dummy_hist->Draw("axissame");
  }
#endif
  else {
    msgsvc.warningmsg(TString::Format("Object %s does not inherit from TH1, TH2A, TF1, TGraph, "
                                      "TGraph2D, TMultiGraph or TEfficiency\n "
                                      "%29.29s Please check if the requested class can be plotted "
                                      "and - if yes - add it to source file",obj->GetName()," "));
    return;
  }
  return;
}
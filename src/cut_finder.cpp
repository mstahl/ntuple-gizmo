/**
  @date:   2021-03-07
  @author: mstahl
  @brief:  Get quantiles of a distribution
*/
//STL
#include <cassert>
//ROOT
#include "ROOT/RDataFrame.hxx"
#include "TStopwatch.h"
//local
#include <IOjuggler.h>
#include "ProgressBar.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif
#define assertm(exp, msg) assert(((void)msg, exp))
int main(int argc, char** argv) {
  TStopwatch clock;
  clock.Start();
  //parse command line variables and put some of them in local variables
  auto options = IOjuggler::parse_options(argc, argv, "d:hi:j:m:n:o:t:v:");
  MessageService msgsvc("cut_finder", static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));

  auto chain = IOjuggler::get_chain(options.get<std::string>("treename"),options.get<std::string>("infilename"),options.get<std::string>("workdir"),msgsvc);
  [[maybe_unused]] auto fes = IOjuggler::get_friends(options,chain,msgsvc);
  const auto nev = chain->GetEntries();

  ROOT::EnableImplicitMT();//need to call this first to be able to call ROOT::GetThreadPoolSize()
  unsigned int current_poolsize;
#if ROOT_VERSION_CODE > ROOT_VERSION(6,22,05)
  current_poolsize = ROOT::GetThreadPoolSize();
#else
  current_poolsize = ROOT::GetImplicitMTPoolSize();
#endif
  const auto nSlots = std::min(options.get<unsigned int>("n"),current_poolsize);
  msgsvc.debugmsg("Running with " + std::to_string(nSlots) + " parallel threads");
  ROOT::EnableImplicitMT(nSlots);
  ROOT::RDataFrame d(*chain.get());
  ROOT::RDF::RNode df = d;
  const auto distribution = options.get<std::string>("joker");
  const auto columns = d.GetColumnNames();
  const bool needs_transformation = std::find(columns.begin(),columns.end(),distribution) == columns.end();
  if(needs_transformation) df = df.Define("custom_distribution",distribution);
  if(const auto filter = options.get_optional<std::string>("weightfile")){
    msgsvc.infomsg("Applying Filter: " + *filter);
    df = df.Filter(*filter);
  }

  ProgressBar pg;
  pg.start(nev, msgsvc);
  msgsvc.debugmsg("Filling distribution into vector for sorting.");
  auto dist_as_vect = df.Take<float>(needs_transformation ? "custom_distribution" : distribution);
  msgsvc.debugmsg("Sorting distribution");
  std::sort(dist_as_vect->begin(), dist_as_vect->end());

  const auto quantilestring = options.get<std::string>("outfilename");
  std::vector<std::string> tmp_qv;
  boost::algorithm::split(tmp_qv, quantilestring, boost::algorithm::is_any_of(";"));
  std::vector<long long> quantile_evts;
  for(const auto& q : tmp_qv){
    msgsvc.debugmsg(q);
    auto alpha = std::stof(q);
    assertm(alpha<1, "quantiles have to be less than 1 !");
    quantile_evts.push_back(std::llround(alpha*dist_as_vect->size()));
    msgsvc.debugmsg(std::to_string(quantile_evts.back()));
  }

  msgsvc.SetMsgLvl(MSG_LVL::INFO);
  for(auto i=0u; i < quantile_evts.size(); i++)
    msgsvc.infomsg(TString::Format("%8s quantile : %11.6g",tmp_qv[i].data(),dist_as_vect->operator[](quantile_evts[i])));

  clock.Stop();
  clock.Print();
  return 0;
}

// script to dump a roofit data set into a standard root tree
#include <TStopwatch.h>
#include <RooDataSet.h>
#include <IOjuggler.h>

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv) {

  TStopwatch clock;
  clock.Start();
  //parse and print options
  auto options = IOjuggler::parse_options(argc, argv, "d:hi:o:r:t:v:w:");
  const auto wd  = options.get<std::string>("workdir");
  const auto ifn = options.get<std::string>("infilename");
  const auto ofn = options.get<std::string>("outfilename");
  const auto dsn = options.get<std::string>("dsname");
  const auto wsn = options.get<std::string>("wsname");
  const auto tn  = options.get<std::string>("treename");

  MessageService msgsvc("dataSet2NTuple",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));

  //get RooDataSet to convert
  const auto ds = IOjuggler::get_wsobj<RooDataSet>(IOjuggler::get_obj<RooWorkspace>(IOjuggler::get_file(ifn,wd,"read",msgsvc),wsn,msgsvc),dsn,msgsvc);
  msgsvc.infomsg("Copying RooDataSet...");
  //make copy
  RooDataSet dump(tn.data(),tn.data(),ds,*ds->get());
  dump.convertToTreeStore();

  //save it as TTree in a root file
  const auto ot = dump.tree();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG)
    ot->Print();

  msgsvc.infomsg("Saving it as TTree...");
  ot->SaveAs(TString::Format("%s/%s",wd.data(),ofn.data()).Data(),"recreate");

  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();

}

/**
  @date:   2018-10-24
  @author: mstahl
  @brief:  simple hadd script allowing to copy a single tree in the input file
*/
#include <sstream>
#include <iostream>
//local
#include <IOjuggler.h>
#include <ProgressBar.h>
//ROOT
#include "TStopwatch.h"
#include "TObjArray.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();

  auto options = IOjuggler::parse_options(argc, argv, "d:i:t:o:v:h","nonoptions: any number of <file(list):friendtree> combinations");
  MessageService msgsvc("hadd_ntg",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));

  const auto wd  = options.get<std::string>("workdir");
  const auto ofn = options.get<std::string>("outfilename");
  const auto ifn = options.get<std::string>("infilename");
  const auto itn = options.get<std::string>("treename");

  msgsvc.infomsg("Hadding files from tree " + itn + " in file(-list) " + ifn + " to " + wd + "/" + ofn);

  //get input as TChain
  auto chain = IOjuggler::get_chain(itn,ifn,wd,msgsvc);
  //add friend tree(s)
  [[maybe_unused]] auto fes = IOjuggler::get_friends(options,chain,msgsvc);

  const TObjArray* lof = chain->GetListOfFiles();
  msgsvc.infomsg("Got the chain with "+std::to_string(lof->GetEntries())+" trees");
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG)
    for(auto t = lof->begin(); t != lof->end(); t.Next())
      msgsvc.debugmsg((*t)->GetTitle());

  if(!(fes[0]==nullptr) && lof->GetEntries() > 1)
    throw std::runtime_error("You ran into this bug: https://sft.its.cern.ch/jira/browse/ROOT-10778 . Try using elwms instead.");

  msgsvc.infomsg("Merging files. This might take a while now... ");
  chain->Merge((wd + "/" + ofn).data(),"fast");

  clock.Stop();
  clock.Print();
  return 0;
}

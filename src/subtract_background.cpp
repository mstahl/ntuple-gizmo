/**
  * @file subtract_background.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2021-10-04
  * @brief Subtract background(s) of different sources using a simple histrogram subtraction method. Histrograms are binned on the fly with a simple adaptive binning approach.
  *        The ouput will be a friend tree with weights.
  */

#include "TStopwatch.h"
#include "ROOT/RDataFrame.hxx"
#include <TH2A.h>
#include <IOjuggler.h>

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  // inputfile with tree contains the unbinned data. the histogram form of that is the first non-opt. we subtract the histograms that follow as non-opts.
  // output is a friend tree with the weight in it. we don't need a config file.
  auto options = IOjuggler::parse_options(argc, argv, "d:hi:j:n:o:t:v:w:","nonoptions: file, hist name(s) and normalization <file:histname;normalization>",2);
  MessageService msgsvc("subtract_background",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  TH2A::SetVerbosity(options.get<int>("verbosity"));
  const auto wd  = options.get<std::string>("workdir");
  const auto itn = options.get<std::string>("treename");
  const auto wn  = options.get<std::string>("wsname");
  const auto eas = options.get_child("extraargs");

  const auto& [filename, hn] = IOjuggler::split_at_last_occurence(eas.front().second.data());
  const auto& [histname, norm] = IOjuggler::split_at_last_occurence(hn,";");
  TH2A hsum;
  hsum.MakeBinning(*IOjuggler::get_obj<TH2F>(IOjuggler::get_file(filename,wd),histname),options.get<unsigned int>("n"));
  auto htarget = static_cast<TH2A*>(hsum.Clone("htarget"));//inherited from TNamed
  msgsvc.infomsg("Using "+histname+" from file "+filename+" as main hist to subtract from.");
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) {hsum.Print(); htarget->Print();}

  for(auto eas_it = std::next(eas.begin()); eas_it != eas.end(); eas_it++){//iterate remaining non-options
    const auto& [fn, hn] = IOjuggler::split_at_last_occurence((*eas_it).second.data());
    const auto& [hist, histnorm] = IOjuggler::split_at_last_occurence(hn,";");
    TH2A htmp(hsum); // copy over binning from initial histo
    htmp.FillFromHist(*IOjuggler::get_obj<TH2F>(IOjuggler::get_file(fn,wd),hist));
    if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) htmp.Print();
    // do the normalization with a double ratio.
    // the source of htmp might have a different origin/norm and first needs to be normalized to the mixed distribution.
    // then we can do the normalization of components that we extracted from the discriminating variable
    const auto double_ratio_for_scaling = htmp.GetSumOfWeights() && histnorm > 0 ? hsum.GetSumOfWeights()/htmp.GetSumOfWeights()*std::stod(histnorm)/std::stod(norm) : 0.;
    msgsvc.debugmsg("Scaling hist "+hist+" from file(s) "+fn+" with factor "+std::to_string(double_ratio_for_scaling));
    htmp.Scale(double_ratio_for_scaling);
    *htarget -= htmp;
    msgsvc.infomsg("Subtracted hist "+hist+" from file(s) "+fn+" from target histogram.");
  }

  // we will get back htarget later by multiplying the unbinned data with hratio
  // note that this is only done to speed things up (we could also calculate htarget/hsum for every event)
  TH2A hratio;
  hratio.BinomialDivide(*htarget,hsum,true);
  msgsvc.infomsg("Scaling by "+std::to_string(hratio.GetNBins()/hratio.GetSumOfWeights()));
  hratio.Scale(hratio.GetNBins()/hratio.GetSumOfWeights());
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) hratio.Print();

  // now loop over events in input tree and calculate weight.
  // get the chain
  auto chain = IOjuggler::get_chain(itn, options.get<std::string>("infilename"), wd, msgsvc);
  ROOT::DisableImplicitMT();
  ROOT::RDataFrame d(*chain.get());
  ROOT::RDF::RNode df = d;
  msgsvc.infomsg("Filling weights from background subtraction.");
  const auto& [calib_var_a, calib_var_b] = IOjuggler::split_at_last_occurence(options.get<std::string>("joker"),";");
  df = df.Define(wn,[&hratio](const double& cv_a, const double& cv_b) -> double {return hratio.GetBinContent(hratio.FindBin(cv_a,cv_b));},{calib_var_a,calib_var_b});

  const auto oloc = wd+"/"+boost::algorithm::replace_all_copy(options.get<std::string>("outfilename"), wd, "");
  msgsvc.infomsg("Writing output tuple to "+oloc);
  IOjuggler::dir_exists(oloc,msgsvc);
  [[maybe_unused]] auto snapshot_rptr = df.Snapshot(itn+"_weights",oloc,{wn});

  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();
  return 0;
}

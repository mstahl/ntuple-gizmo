/**
  * @file kde_weighting.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2022-08-02
  * @brief Up to 10-dimensional weighting with Meerkat's adaptive kernel density estimators.
  * @details Take "target" and "origin" input files, where "target" is the data that defines how "origin" should look like after weighting.
  *          The script applies the weight (i.e. the (normalized) ratio of inputs) on an event-by-event basis.
  */

#include "TStopwatch.h"
#include "ROOT/RDataFrame.hxx"
#include <IOjuggler.h>
//MEERKAT
#include "OneDimPhaseSpace.hh"
#include "CombinedPhaseSpace.hh"
#include "AdaptiveKernelDensity.hh"
#include "BinnedKernelDensity.hh"
#include "FactorisedDensity.hh"
#include "Logger.hh"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  // Reweight data from -i to the target data -t.
  // Once the two KDEs have been created, we can loop through the input sample -i and additional samples in the nonoptions to apply the weights.
  // At some point these weights will have to be re-normalized, which can be done here or in a subsequent step.
  auto options = IOjuggler::parse_options(argc, argv, "c:d:hi:j:o:t:v:","nonoptions: more files and trees to be weighted <file:tree>",0);
  MessageService msgsvc("kde_weighting",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: "+static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  const auto wd = options.get<std::string>("workdir");

  // runtime configuration
  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  //append and replace stuff in ptree
  IOjuggler::replace_from_cmdline(configtree,options.get<std::string>("joker"));
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);
  else Logger::setLogLevel(2);

  // get the inputs (expect <file0>:<tree>)
  const auto& [ofn, otn] = IOjuggler::split_at_last_occurence(options.get<std::string>("infilename"));
  const auto& [tfn, ttn] = IOjuggler::split_at_last_occurence(options.get<std::string>("treename"));
  auto origin_file = IOjuggler::get_file(ofn,wd,"READ",msgsvc);// make this explicit in case we need to reset the bit for reshuffled entries
  auto origin_tree = IOjuggler::get_obj<TTree>(origin_file,otn);
  auto target_tree = IOjuggler::get_obj<TTree>(IOjuggler::get_file(tfn,wd),ttn);

  // read config and prepare output
  msgsvc.debugmsg("Creating phase space and preparing KDEs");
  std::vector<TString> varnames;
  std::vector<unsigned int> kde_bins, nbins_raw;
  std::vector<double> kde_widths;
  // Keep Meerkat-objects in scope, otherwise there'll be no density
  std::vector<AbsPhaseSpace*> phsp_dump;
  std::vector<BinnedKernelDensity*> origin_bkdes, target_bkdes;
  std::vector<AdaptiveKernelDensity*> origin_akdes, target_akdes;
  std::vector<FactorisedDensity*> origin_fkdes, target_fkdes;
  std::vector<CombinedPhaseSpace*> phsps;

  auto ntoys = configtree.get<unsigned int>("ntoys",2000000);
  // trick to use weights in Meerkat: add weightname to varnames, so varnames is one dimension larger than the phase space dimensionality
  // this leads to a bit of ugly copy-pasting below
  const auto ow = configtree.get_optional<std::string>("origin_weight");
  const auto tw = configtree.get_optional<std::string>("target_weight");

  msgsvc.debugmsg("Getting KDEs");
  for(const auto& v: configtree.get_child("variables")){
    const auto lo = v.second.get<double>("low"), hi = v.second.get<double>("high");
    const auto nbins = v.second.get<unsigned int>("nbins_kde",1000);
    phsp_dump.push_back(new OneDimPhaseSpace(v.first.data(),lo,hi));
    varnames.emplace_back(v.first);
    kde_bins.push_back(nbins);
    nbins_raw.push_back(nbins);
    kde_widths.push_back(v.second.get<double>("width_kde",(hi - lo)/static_cast<double>(kde_bins.back())));
    // adapt binning to dimensionality
    for(int ji = phsp_dump.size()-1; ji >= 0; ji--){
      kde_bins[ji] = static_cast<unsigned int>(std::round(std::pow(10,std::log10(nbins_raw[ji])-phsp_dump.size()+1)));
      kde_widths[ji] = (phsp_dump[ji]->upperLimit(0) - phsp_dump[ji]->lowerLimit(0))/static_cast<double>(kde_bins[ji]);
      msgsvc.debugmsg(TString::Format("Number of bins in dimension %u: %u. Bin width %.11g",ji+1,kde_bins[ji],kde_widths[ji]));
    }
    // use logic of https://gitlab.cern.ch/lhcb/Urania/-/blob/abec39e38fe01cee90605e81a5132af0d9135fb2/PIDCalib/PIDPerfScripts/python/PIDGenExpert/Run2/PIDPdf.py
    std::vector<unsigned int> tmp_1dkdebins{nbins_raw.back()};
    std::vector<double> tmp_1dkde_widths{v.second.get<double>("width_kde",(hi - lo)/static_cast<double>(nbins_raw.back()))};
    std::vector<TString> tmp_1dkde_vars{varnames.back()};
    if(ow) tmp_1dkde_vars.emplace_back(*ow);
    origin_bkdes.push_back(new BinnedKernelDensity(("OB1DKDE_"+v.first).data(),phsp_dump.back(),origin_tree,tmp_1dkde_vars,tmp_1dkdebins,tmp_1dkde_widths,0,ntoys));
    if(ow) tmp_1dkde_vars.pop_back();
    if(tw) tmp_1dkde_vars.emplace_back(*tw);
    target_bkdes.push_back(new BinnedKernelDensity(("TB1DKDE_"+v.first).data(),phsp_dump.back(),target_tree,tmp_1dkde_vars,tmp_1dkdebins,tmp_1dkde_widths,0,ntoys));
    if(tw) tmp_1dkde_vars.pop_back();
    if(phsp_dump.size() == 1){
      if(ow) varnames.emplace_back(*ow);
      origin_akdes.push_back(new AdaptiveKernelDensity(("OAKDE_"+v.first).data(),phsp_dump.back(),origin_tree,varnames,kde_bins,kde_widths,origin_bkdes.back(),0,ntoys));
      if(ow) varnames.pop_back();
      if(tw) varnames.emplace_back(*tw);
      target_akdes.push_back(new AdaptiveKernelDensity(("TAKDE_"+v.first).data(),phsp_dump.back(),target_tree,varnames,kde_bins,kde_widths,target_bkdes.back(),0,ntoys));
      if(tw) varnames.pop_back();
    }
    else {
      phsps.push_back(new CombinedPhaseSpace(("phsp_"+v.first).data(),phsp_dump));
      origin_fkdes.push_back(new FactorisedDensity(("OFKDE_"+v.first).data(),phsps.back(),origin_akdes.back(),origin_bkdes.back()));
      target_fkdes.push_back(new FactorisedDensity(("TFKDE_"+v.first).data(),phsps.back(),target_akdes.back(),target_bkdes.back()));
      if(ow) varnames.emplace_back(*ow);
      origin_bkdes.push_back(new BinnedKernelDensity(("OBKDE_"+v.first).data(),phsps.back(),origin_tree,varnames,kde_bins,kde_widths,origin_fkdes.back(),ntoys));
      origin_akdes.push_back(new AdaptiveKernelDensity(("OAKDE_"+v.first).data(),phsps.back(),origin_tree,varnames,kde_bins,kde_widths,origin_bkdes.back(),origin_fkdes.back(),ntoys));
      if(ow) varnames.pop_back();
      if(tw) varnames.emplace_back(*tw);
      target_bkdes.push_back(new BinnedKernelDensity(("TBKDE_"+v.first).data(),phsps.back(),target_tree,varnames,kde_bins,kde_widths,target_fkdes.back(),ntoys));
      target_akdes.push_back(new AdaptiveKernelDensity(("TAKDE_"+v.first).data(),phsps.back(),target_tree,varnames,kde_bins,kde_widths,target_bkdes.back(),target_fkdes.back(),ntoys));
      if(tw) varnames.pop_back();
    }
  }

  // need to "Define" phase-space point to get the densities later
  std::string transform_phase_space_variables_to_vector{"std::vector<double>{"};
  for(const auto vn : varnames)
    transform_phase_space_variables_to_vector += vn+",";
  transform_phase_space_variables_to_vector.pop_back();
  transform_phase_space_variables_to_vector += "};";
  msgsvc.debugmsg("Phase-space point definition: " + transform_phase_space_variables_to_vector);

  const auto wn = configtree.get("output_branch_name","kde_weight");
  const auto oloc = wd+"/"+boost::algorithm::replace_all_copy(options.get<std::string>("outfilename"), wd, "");
  msgsvc.infomsg("Writing output tuple to "+oloc);
  IOjuggler::dir_exists(oloc,msgsvc);

  // lambda to loop over events in input tree, fill and persist weights.
  const auto final_target_akde = target_akdes.back(), final_origin_akde = origin_akdes.back();
  auto apply_weights = [&varnames, &wn, &oloc, &configtree, &final_origin_akde, &final_target_akde, &transform_phase_space_variables_to_vector]
                       (std::unique_ptr<TChain> c, const std::string&& out_treename, const std::string&& fileopt) -> void {
    ROOT::RDataFrame d(*c.get());
    ROOT::RDF::RNode df = d;
    // don't filter NaN or inf, but set to 0, otherwise the weights might not be usable as friend trees
    df = df.Define("phsp_point",transform_phase_space_variables_to_vector);
    df = df.Define(wn,[&final_origin_akde, &final_target_akde](std::vector<double>& point) -> double {
        const auto w = final_target_akde->density(point) / final_origin_akde->density(point);
        return !(std::isnan(w) || std::isinf(w)) ? w : 0.; },{"phsp_point"});
    ROOT::RDF::RSnapshotOptions rso(fileopt, static_cast<ROOT::ECompressionAlgorithm>(configtree.get<unsigned int>("outcompalg",ROOT::RCompressionSetting::EAlgorithm::kInherit)),
                                    configtree.get<unsigned int>("outcomplvl",ROOT::RCompressionSetting::ELevel::kInherit),0,99,false);
    [[maybe_unused]] auto snapshot_rptr = df.Snapshot(out_treename,oloc,{wn},rso);
  };
  // do it - for the regular input
  msgsvc.infomsg("Applying weights from weighting to input sample.");
  apply_weights(IOjuggler::get_chain(otn, ofn, wd, msgsvc), configtree.get("output_tree",otn+"weights"), "RECREATE");

  // loop nonoptions to apply weighting for additional inputs
  if(auto eas = options.get_child_optional("extraargs"); (*eas).size() > 0){
    msgsvc.infomsg("Applying weights from weighting to additional samples.");
    for(const auto& ea : *eas){//iterate non-options
      const auto& [efn, etn] = IOjuggler::split_at_last_occurence(ea.second.data());
      apply_weights(IOjuggler::get_chain(etn, efn, wd, msgsvc), boost::algorithm::replace_all_copy(IOjuggler::split_at_last_occurence(efn,"/").back(), ".root", "")+"weights", "UPDATE");
    }
  }

  // make a nice plot
  auto f = TFile::Open(oloc.data(),"UPDATE");
  f->cd();
  const auto final_phsp = phsps.back();
  switch(unsigned int d = final_phsp->dimensionality()){
    case 1u : {
        TH1D h1_weight((wn+"_hist").data(),"",kde_bins[0],final_phsp->lowerLimit(0),final_phsp->upperLimit(0));
        for(unsigned int ibinx = 0; ibinx < h1_weight.GetXaxis()->GetNbins()+1; ibinx++){
          auto phsp_point = std::vector<double>{h1_weight.GetXaxis()->GetBinCenter(ibinx)};
          const auto w = final_target_akde->density(phsp_point) / final_origin_akde->density(phsp_point);
          h1_weight.SetBinContent(ibinx, !(std::isnan(w) || std::isinf(w)) ? w : 0.);
        }
        h1_weight.Write();
      }
      break;
    case 2u : {
        TH2D h2_weight((wn+"_hist").data(),"",kde_bins[0],final_phsp->lowerLimit(0),final_phsp->upperLimit(0),kde_bins[1],final_phsp->lowerLimit(1),final_phsp->upperLimit(1));
        for(unsigned int ibinx = 0u; ibinx < h2_weight.GetXaxis()->GetNbins()+1; ibinx++){
          for(unsigned int ibiny = 0u; ibiny < h2_weight.GetYaxis()->GetNbins()+1; ibiny++){
            auto phsp_point = std::vector<double>{h2_weight.GetXaxis()->GetBinCenter(ibinx), h2_weight.GetYaxis()->GetBinCenter(ibiny)};
            const auto w = final_target_akde->density(phsp_point) / final_origin_akde->density(phsp_point);
            h2_weight.SetBinContent(ibinx, ibiny, !(std::isnan(w) || std::isinf(w)) ? w : 0.);
          }
        }
        h2_weight.Write();
      }
      break;
    case 3u :{
        TH3D h3_weight((wn+"_hist").data(),"",kde_bins[0],final_phsp->lowerLimit(0),final_phsp->upperLimit(0),kde_bins[1],final_phsp->lowerLimit(1),final_phsp->upperLimit(1),kde_bins[2],final_phsp->lowerLimit(2),final_phsp->upperLimit(2));
        for(unsigned int ibinx = 0u; ibinx < h3_weight.GetXaxis()->GetNbins()+1; ibinx++){
          for(unsigned int ibiny = 0u; ibiny < h3_weight.GetYaxis()->GetNbins()+1; ibiny++){
            for(unsigned int ibinz = 0u; ibinz < h3_weight.GetZaxis()->GetNbins()+1; ibinz++){
              auto phsp_point = std::vector<double>{h3_weight.GetXaxis()->GetBinCenter(ibinx), h3_weight.GetYaxis()->GetBinCenter(ibiny), h3_weight.GetZaxis()->GetBinCenter(ibinz)};
              const auto w = final_target_akde->density(phsp_point) / final_origin_akde->density(phsp_point);
              h3_weight.SetBinContent(ibinx, ibiny, ibinz, !(std::isnan(w) || std::isinf(w)) ? w : 0.);
            }
          }
        }
        h3_weight.Write();
      }
      break;
    default :
      msgsvc.warningmsg(TString::Format("Cannot make plot of %u dimensional weights.",d));
  }

  // reset bit of input tree, since that tree and the new friend tree are index-aligned now.
  if(origin_tree->TestBit(TTree::EStatusBits::kEntriesReshuffled)){
    msgsvc.infomsg("Resetting kEntriesReshuffled bit in input");
    origin_file = IOjuggler::get_file(ofn,wd,"UPDATE",msgsvc);
    origin_tree = IOjuggler::get_obj<TTree>(origin_file,otn);
    origin_tree->ResetBit(TTree::EStatusBits::kEntriesReshuffled);
    origin_tree->Write();
    origin_file->Close();
  }

  //we kept all densities etc in scope. now we need to delete them
  auto delete_stuff = [ ] (auto container) -> void {
    for(auto thing : container) delete thing;
  };
  delete_stuff(phsp_dump);
  delete_stuff(origin_bkdes);
  delete_stuff(origin_akdes);
  delete_stuff(origin_fkdes);
  delete_stuff(target_bkdes);
  delete_stuff(target_akdes);
  delete_stuff(target_fkdes);
  delete_stuff(phsps);

  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO) {clock.Print();}
  return 0;
}

/**
  @date:   2019-05-13, updated in Feb. 2021
  @author: mstahl
  @brief:  Find multiple candidates and store their multiplicity count in a friend tree
*/
// STL
#include <tuple>
// ROOT
#include "TH1I.h"
#include "TLeaf.h"
#include "ROOT/RDataFrame.hxx"
#include "TStopwatch.h"
// local
#include <IOjuggler.h>
#include <CalcGizmo.h>
#include "ProgressBar.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

namespace pt = boost::property_tree;
typedef std::tuple<unsigned long long, unsigned long long, unsigned char, float, float> mult_tpl;

int main(int argc, char** argv) {
  // hardcoded stuff
  constexpr int nbins_mult_hist = 15;  //maximum multiplicity for some operations

  TStopwatch clock;
  clock.Start();

  // parse command line variables and put some of them in local variables
  auto options = IOjuggler::parse_options(argc, argv, "d:hi:j:o:r:t:v:w:");
  MessageService msgsvc("multiple_candidates", static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  const auto wd = options.get<std::string>("workdir");
  const auto of = options.get<std::string>("outfilename");
  const auto tn = options.get<std::string>("treename");
  const auto sc = options.get<std::string>("joker");

  // get the tree
  auto input_root_file = IOjuggler::get_file(options.get<std::string>("infilename"),wd,"READ",msgsvc);
  auto tree = IOjuggler::get_obj<TTree>(input_root_file,tn);
  [[maybe_unused]] auto fes = IOjuggler::get_friends(options, tree, msgsvc);

  // let's not run multithreaded when juggling with indices... (it's off unless we call ROOT::EnableImplicitMT())
  ROOT::RDataFrame d(*tree);
  ROOT::RDF::RNode df = d;
  df = df.Define("unique_event_number",[](const unsigned int rn, const unsigned long long en) -> unsigned long long {return 10e+9*rn+en;},{"runNumber","eventNumber"});
  if(!sc.empty()) {
    df = df.Define("min_crit","static_cast<float>("+sc+")");
    df = df.Define("max_crit","static_cast<float>("+sc+")");
  } else {
    df = df.Define("min_crit","0.f");
    df = df.Define("max_crit","0.f");
  }
  // initialize vector of tree-index, unique event number and multiplicity. fill it by looping the input tree
  std::vector<mult_tpl> uevs;
  const unsigned long long numevents = tree->GetEntries();
  ProgressBar pg;
  pg.start(numevents, msgsvc);
  msgsvc.infomsg("Filling unique event number vector from tree.");
  // fill the uevs tuple
  df.Foreach([&uevs, &pg](const unsigned long long ie, const unsigned long long uev, const float& crit){
    pg.update(ie);
    uevs.emplace_back(ie, uev, 0x01, crit, crit);
    }, {"rdfentry_", "unique_event_number", "min_crit"});
  pg.stop();

  //Sort the vector of tree-index and unique event number by the event number
  msgsvc.infomsg("Sorting by unique event number.");
  std::sort(uevs.begin(), uevs.end(), [](const mult_tpl& a, const mult_tpl& b) { return std::get<1>(a) < std::get<1>(b); });

  // now we need to find multiple candidates and safe their multiplicity in our small uevs tuple
  unsigned long long unique_event_number_previous_event = 0;
  unsigned int n_overall_multiple_candidates = 0u, ie = 0u;
  unsigned char multiplicity = 0x00;

  pg.start(uevs.size(), msgsvc);
  TH1I multiplicities("multiplicities", ";Multiplicity;Candidates", nbins_mult_hist, 1, nbins_mult_hist + 1);

  msgsvc.infomsg("Searching for multiple candidates.");
  for (auto const&[tree_index, unique_event, mult, mic, mac] : uevs) {
    if (unique_event == unique_event_number_previous_event) {  // this is a multiple candidate!
      n_overall_multiple_candidates++;
      multiplicity++;
    } else {  // this candidate has a different unique event number than the candidate before
      // i'm too stupid to use stl or boost to get min/max; so let's do it the old-fashioned way
      auto min_crit_in_range = FLT_MAX, max_crit_in_range = -FLT_MAX;
      for (int j = 1; j <= multiplicity; j++){
        if(std::get<3>(uevs[ie-j])<min_crit_in_range) min_crit_in_range = std::get<3>(uevs[ie-j]);
        if(std::get<4>(uevs[ie-j])>max_crit_in_range) max_crit_in_range = std::get<4>(uevs[ie-j]);
      }
      // go back and set current multiplicity and rejection criteria
      for (int j = 1; j <= multiplicity; j++){
        std::get<2>(uevs[ie-j]) = multiplicity;
        std::get<3>(uevs[ie-j]) = min_crit_in_range;
        std::get<4>(uevs[ie-j]) = max_crit_in_range;
      }
      // reset flags and bookkeeping
      multiplicities.Fill(multiplicity);
      multiplicity = 0x01;
    }
    unique_event_number_previous_event = unique_event;
    // some debug output
    if (ie < 20) msgsvc.debugmsg(TString::Format("Tree index %ull, Unique event number %u, multiplicity %d, min_crit %.3f, max_crit %.3f",
                                                 tree_index, unique_event, static_cast<int>(mult),mic,mac));
    pg.update(ie++);
  } // end loop over all events
  pg.stop();

  // now we're ready to fill the output tree. first, we need to sort back our uevs tuple so that the tree-indices align, and we can write the friend tree.
  msgsvc.infomsg("Re-sorting by tree entry index.");
  std::sort(uevs.begin(), uevs.end(), [](const mult_tpl& a, const mult_tpl& b) { return std::get<0>(a) < std::get<0>(b); });
  // init output file and it's tree structure
  const auto oloc = (wd.empty() || wd.compare(0,1,"/") == 0) ? of : wd+"/"+of;
  IOjuggler::dir_exists(oloc,msgsvc);
  msgsvc.infomsg("Writing friend tree to "+oloc);
  auto ff = TFile::Open(oloc.data(), "RECREATE");
  const auto otn = options.get<std::string>("wsname") == static_cast<std::string>("w") ? tn + "_mult" : options.get<std::string>("wsname");
  TTree otree(otn.data(),otn.data());
  otree.Branch("multiplicity", &multiplicity, "multiplicity/b");
  float min_crit, max_crit;
  otree.Branch("min_crit", &min_crit, "min_crit/F");
  otree.Branch("max_crit", &max_crit, "max_crit/F");

  for (auto const&[tree_index, unique_event, mult, mic, mac] : uevs) {
    if (ie < 20) msgsvc.debugmsg(TString::Format("Tree index %ull, Unique event number %u, multiplicity %d",tree_index, unique_event, static_cast<int>(mult)));
    multiplicity = mult;
    min_crit = mic;
    max_crit = mac;
    otree.Fill();
  }
  // Write tree and histo to the ouput file
  ff->cd();
  otree.ResetBit(TTree::EStatusBits::kEntriesReshuffled);
  otree.Write();
  multiplicities.Write();
  if (msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) otree.Print();

  // write result to tex
  const auto fraction_multiples_in_percent = 100 * static_cast<float>(n_overall_multiple_candidates) / static_cast<float>(numevents);
  const auto dsn = options.get<std::string>("dsname");
  if(const auto trn = dsn == "ds" ? "" : (wd.empty() || wd.compare(0,1,"/") == 0) ? dsn : wd+"/"+dsn; !trn.empty()){
    IOjuggler::dir_exists(trn);
    std::ofstream out_tex;
    msgsvc.infomsg("Writing statistics as TeX files to " + trn + "_n.tex and " + trn + "_f.tex");
    out_tex.open(trn+"_n.tex");
    out_tex << n_overall_multiple_candidates;
    out_tex.close();
    out_tex.open(trn+"_f.tex");
    auto f_round = fraction_multiples_in_percent;
    CalcGizmo::pdgrounding(f_round,0.1*f_round,0.f);
    out_tex << f_round;
    out_tex.close();
  }

  // Print statistics
  msgsvc.infomsg(std::string(static_cast<int>(8 * (nbins_mult_hist + 3)) + 1, '*'));
  msgsvc.infomsg(TString::Format(TString::Format("%%-%ds*", static_cast<int>(8 * (nbins_mult_hist + 3))),
                                 TString::Format("* Found %u multiple candidates in %u total events (%.2f %%)",
                                                 n_overall_multiple_candidates, numevents, fraction_multiples_in_percent)
                                     .Data()));
  msgsvc.infomsg(TString::Format(TString::Format("%%-%ds*", static_cast<int>(8 * (nbins_mult_hist + 3))), "* Multiplicities: "));
  TString line = "* Multiplicity  ";
  for (int j = 1; j < nbins_mult_hist + 1; j++)
    line += TString::Format("%-8d", j);
  line += "overflow*";
  msgsvc.infomsg(line);
  line = "* Candidates    ";
  for (int j = 1; j < nbins_mult_hist + 2; j++)
    line += TString::Format("%-8d", static_cast<int>(multiplicities.GetBinContent(j)));
  msgsvc.infomsg(line + "*");
  msgsvc.infomsg(std::string(static_cast<int>(8 * (nbins_mult_hist + 3)) + 1, '*'));

  // reset bit of input tree, since that tree and the new friend tree are index-aligned now.
  if(tree->TestBit(TTree::EStatusBits::kEntriesReshuffled)){
    msgsvc.infomsg("Resetting kEntriesReshuffled bit in input");
    input_root_file = IOjuggler::get_file(options.get<std::string>("infilename"),wd,"UPDATE",msgsvc);
    tree = IOjuggler::get_obj<TTree>(input_root_file,tn);
    tree->ResetBit(TTree::EStatusBits::kEntriesReshuffled);
    tree->Write();
    input_root_file->Close();
  }

  // timing and cleanup
  clock.Stop();
  if (msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();
  ff->Close();
  return 0;
}

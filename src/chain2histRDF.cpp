/**
  @date:   2019-09-30
  @author: mstahl
  @brief:  script for making histograms using boost property trees as config files.
           documentation: https://gitlab.cern.ch/sneubert/ntuple-gizmo/blob/master/doc/chain2histRDF.md
*/
//local
#include <ProgressBar.h>
#include <hists_RDF.h>

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  //parse command line options
  const auto options = IOjuggler::parse_options(argc, argv, "c:d:i:j:o:t:v:h","nonoptions: any number of <file(list):friendtree> combinations");
  const auto wd = options.get<std::string>("workdir");
  const auto ofn = options.get<std::string>("outfilename");
  const auto ifn = options.get<std::string>("infilename");
  const auto itn = options.get<std::string>("treename");
  MessageService msgsvc("chain2histRDF",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  // runtime config file manipulations
  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  IOjuggler::replace_from_cmdline(configtree,options.get<std::string>("joker"));
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);

  //first, we need to know how many events there are and how many threads we use:
  ROOT::EnableImplicitMT();//need to call this first to be able to call ROOT::GetThreadPoolSize()
  unsigned int current_poolsize;
  #if ROOT_VERSION_CODE > ROOT_VERSION(6,22,05)
  current_poolsize = ROOT::GetThreadPoolSize();
#else
  current_poolsize = ROOT::GetImplicitMTPoolSize();
#endif
  const auto nThreads = std::min(configtree.get("threads",current_poolsize),current_poolsize);
  if(nThreads == 0 || configtree.get_optional<unsigned int>("Range_end")){
    ROOT::DisableImplicitMT();
    current_poolsize = 1;
    msgsvc.infomsg("Implicit multithreading is switched off");
  }
  else{
    ROOT::EnableImplicitMT(nThreads);
    msgsvc.infomsg("Running with "+std::to_string(nThreads)+" threads on "+ std::to_string(current_poolsize)+" slots");
  }

  //get input as TChain
  auto chain = IOjuggler::get_chain(itn,ifn,wd,msgsvc);
  //attach friend-files, if any
  msgsvc.debugmsg("Trying to invite some friends");
  [[maybe_unused]] auto fes = IOjuggler::get_friends(options,chain,msgsvc);

  ROOT::RDataFrame d(*chain.get());
  //use ROOT::RDF::RNode to chain commands on the dataframe
  ROOT::RDF::RNode df = d;

  msgsvc.infomsg("Request to save histograms defined in " + options.get<std::string>("config") +
                 " from TTree " + itn + " in " + wd + "/" + ifn + " to " + wd + "/" + ofn);

  //call define action (do this here. the new column should be available for all hists in the following)
  if(const auto de = configtree.get_child_optional("Define")){
    for(const auto& def : *de){
      msgsvc.debugmsg("Calling define action. New column: " + def.first + ", defined by "+ def.second.get_value<std::string>());
      df = df.Define(def.first,def.second.get_value<std::string>());
    }
  }
  //global cut
  if(const auto filter = configtree.get_optional<std::string>("Filter")){
    msgsvc.infomsg("Applying global Filter " + *filter);
    df = df.Filter(*filter);
  }

  //make the histograms
  result_ptrs lazy_hists;
  auto const hists = configtree.get_child_optional("hists");
  if(hists) hists_RDF(*hists, lazy_hists, df);

  //keep us entertained with a progress bar
  ProgressBar pg;
  pg.start(chain->GetEntries(),msgsvc,nThreads);
  std::mutex barMutex;
  unsigned long long evt = 0ull;
  auto loop_lambda = [&pg,&evt,&barMutex](unsigned int, auto& ) {
    std::lock_guard<std::mutex> l(barMutex); // lock_guard locks the mutex at construction, releases it at destruction
    pg.update(evt);
    evt++;
  };
  if(!lazy_hists.empty())
    std::visit([&loop_lambda](auto& h){h.OnPartialResultSlot(1ull,loop_lambda);}, lazy_hists[0]);
  else msgsvc.errormsg("Nothing added. This should not happen");

  const auto oloc = (wd.empty() || wd.compare(0,1,"/") == 0) ? ofn : wd+"/"+ofn;
  IOjuggler::dir_exists(oloc,msgsvc);
  TFile of(oloc.data(),configtree.get("outfopt","recreate").data());
  // We need to trigger the event loop with this dummy-action before trying to write to file.
  // Only seen when running with friend-files.
  std::visit([](auto& h){h->GetDimension();}, lazy_hists[0]);
  of.cd();
  // Now the hists can be written to file
  for(auto lh : lazy_hists)
    std::visit([](auto& h){h->Write();}, lh);
  of.Write();
  of.Close();
  const int time = pg.stop();
  msgsvc.infomsg("Elapsed time for event loop: "+std::to_string(time)+" s");
  return 0;
}

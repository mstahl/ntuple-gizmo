/**
  @date:   2019-09-30
  @author: mstahl
  @brief:  script for making tuples, histograms, workspaces, applying TMVA weights - all multithreaded - using boost property trees as config files.
           documentation: https://gitlab.cern.ch/sneubert/ntuple-gizmo/blob/master/doc/elwms.md
*/
//ROOT
#include "TXMLEngine.h"
//ROOFIT
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooKeysPdf.h"
#include "RooGaussian.h"
#include "RooFFTConvPdf.h"
//TMVA
#include "TMVA/Reader.h"
//boost
#include <boost/property_tree/json_parser.hpp>
//local
#include <ProgressBar.h>
#include <misID_betaRDF.h>
#include <hists_RDF.h>

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

namespace pt = boost::property_tree;

int main(int argc, char** argv){

  /////////////////////////////////////////////////////
  ///  parse command-line options, start stopwatch  ///
  /////////////////////////////////////////////////////
  TStopwatch clock;
  clock.Start();
  auto options = IOjuggler::parse_options(argc, argv, "c:d:i:j:m:o:r:t:w:hv:","nonoptions: any number of <file(list):friendtree> combinations");
  MessageService msgsvc("Eierlegende Wollmilchsau",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: "+static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  const auto wd  = options.get<std::string>("workdir");
  const auto ofn = options.get<std::string>("outfilename");
  const auto ifn = options.get<std::string>("infilename");
  const auto itn = options.get<std::string>("treename");

  ///////////////////////////////
  ///  runtime configuration  ///
  ///////////////////////////////
  pt::ptree configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  //append and replace stuff in ptree
  IOjuggler::replace_from_cmdline(configtree,options.get<std::string>("joker"));
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);
  IOjuggler::SetRootVerbosity(static_cast<bool>(configtree.get_optional<int>("suppress_RootInfo")));

  ///////////////////////////////////////////////////////
  ///  get variables for storing and/or transforming  ///
  ///////////////////////////////////////////////////////
  const auto add_branch_mode = configtree.get<bool>("AddBranchMode",false);
  std::vector<std::string> vars_out = {};
  std::vector<std::pair<std::string,std::string>> trafos;
  // variables from config file
  if(const auto trim_vars = configtree.get_child_optional("variables")){
    if(add_branch_mode) msgsvc.infomsg("Adding "+std::to_string((*trim_vars).size())+" input variable(s)");
    else msgsvc.infomsg("Running with "+std::to_string((*trim_vars).size())+" input variable(s)");
    for(const auto& var : *trim_vars) vars_out.push_back(var.first);
  }
  else msgsvc.infomsg("Running with full set of input variables");

  // add command line variables
  if(auto eas = options.get_child_optional("extraargs"); (*eas).size() > 0){
    for(const auto& ea : *eas){//iterate non-options
      const auto split_pos = ea.second.data().find("->");
      if( split_pos != std::string::npos ){//was "->" found?
        const auto nn = ea.second.data().substr(0,split_pos);
        trafos.emplace_back(nn,ea.second.data().substr(split_pos+2));
        vars_out.push_back(nn);
      }
      else msgsvc.debugmsg("<variable->transformation> combination not found in "+ea.second.data());
    }
  }

  // Define actions from the config file
  if(const auto defa = configtree.get_child_optional("DefineInternal"))
    for(const auto& define_pair : *defa)
      trafos.emplace_back(define_pair.first,define_pair.second.data());

  if(const auto defa = configtree.get_child_optional("Define")){
    for(const auto& define_pair : *defa){
      const std::string new_variable = define_pair.second.data();
      trafos.emplace_back(define_pair.first,new_variable);
      if(new_variable.find("__internal__") == std::string::npos)
        vars_out.push_back(define_pair.first);
    }
  }

  //first, we need to know how many events there are and how many threads we use:
  ROOT::EnableImplicitMT();//need to call this first to be able to call ROOT::GetThreadPoolSize()
  unsigned int current_poolsize;
  #if ROOT_VERSION_CODE > ROOT_VERSION(6,22,05)
  current_poolsize = ROOT::GetThreadPoolSize();
#else
  current_poolsize = ROOT::GetImplicitMTPoolSize();
#endif
  const auto nThreads = std::min(configtree.get("threads",current_poolsize),current_poolsize);
  if(nThreads == 0 || configtree.get_optional<unsigned int>("Range_end")){
    ROOT::DisableImplicitMT();
    current_poolsize = 1;
    msgsvc.infomsg("Implicit multithreading is switched off");
  }
  else{
    ROOT::EnableImplicitMT(nThreads);
    msgsvc.infomsg("Running with "+std::to_string(nThreads)+" threads on "+ std::to_string(current_poolsize)+" slots");
  }

  //////////////////////////////////////////////
  ///  get input file(s) and make RDataFrame ///
  //////////////////////////////////////////////
  auto chain = IOjuggler::get_chain(itn,ifn,wd,msgsvc);
  //add friend tree(s).
  [[maybe_unused]] auto fes = IOjuggler::get_friends(options,chain,msgsvc);
  ROOT::RDataFrame d(*chain.get());
  if(add_branch_mode) for(auto const& coln : d.GetColumnNames()) if(configtree.get("add_friend_vars",1) || coln.find("Chain_") != 0) vars_out.push_back(coln);
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) d.Describe();
  //use ROOT::RDF::RNode to chain commands on the dataframe
  ROOT::RDF::RNode df = d;

  /////////////////////////////
  ///  Add Lumi info tuple  ///
  /////////////////////////////
  if(configtree.get("LumiTuple",0)){
    try {
      msgsvc.debugmsg("Adding lumi info tuple");
      //this can throw
      auto lc = IOjuggler::get_chain(configtree.get("LumiTupleTree","GetIntegratedLuminosity/LumiTuple"),ifn,wd,msgsvc);
      ROOT::RDataFrame dlumi(*lc.get());
      const auto oloclumi = wd+configtree.get("LumiOutputPrefix","/forWs/Lumi_")+ofn;
      IOjuggler::dir_exists(oloclumi);
      msgsvc.infomsg("Writing lumi tuple to "+oloclumi);

      const auto cols = dlumi.GetColumnNames();
      msgsvc.debugmsg("Column names: ");
      for(const auto& c : cols) msgsvc.debugmsg(c);
      //usually the following should work, but it doesn't:
      // if(dlumi.HasColumn("IntegratedLuminosity") && dlumi.HasColumn("IntegratedLuminosityErr"))
      if(std::find(cols.begin(), cols.end(), std::string("IntegratedLuminosity")) != cols.end() &&
         std::find(cols.begin(), cols.end(), std::string("IntegratedLuminosityErr")) != cols.end()){
        auto intlumi = dlumi.Sum("IntegratedLuminosity");
        //Lumi errors are simply added up,
        //see sl. 10 https://indico.cern.ch/event/100756/contributions/1296401/attachments/3262/4947/Lumi_Yas.pdf
        auto dintlumi = dlumi.Sum("IntegratedLuminosityErr");
        dlumi.Snapshot("LumiTuple", oloclumi, "");
        msgsvc.infomsg(TString::Format("Integrated luminosity: %.3f +- %.3f pb-1",intlumi.GetValue(),dintlumi.GetValue()));
      }
      else dlumi.Snapshot("LumiTuple", oloclumi, "");
    }
    catch(std::exception& e){
      msgsvc.warningmsg("Caught exception while trying to add LumiTuple: "+static_cast<std::string>(e.what()));
    }
  }

  ////////////////////////////////////////////////
  ///  Define actions (tansform/add variables) ///
  ////////////////////////////////////////////////
  if(const auto misID = configtree.get_child_optional("misIDMbeta")){
    std::vector<std::string> daughters;
    for(const auto& daughter : *misID)
      daughters.push_back(daughter.first);
    MbetaRDF(daughters,df,vars_out,msgsvc,configtree.get("add_lvs_to_output",false));
    msgsvc.debugmsg("Added masses and betas for misID studies");
  }
  //create index list
  if(configtree.get("create_indexlist",false)){
    df = df.Define("index","rdfentry_");
    vars_out.push_back("index");
  }
  if(!trafos.empty())
    for(const auto& tf : trafos){
      msgsvc.debugmsg("transforming "+tf.second+" to a new branch: "+tf.first);
      df = df.Define(tf.first,tf.second);
    }

  ///////////////////
  ///  apply MVA  ///
  ///////////////////
  if(const auto wf = options.get<std::string>("weightfile");wf!=""){
    //parse multiple weight files and BDT response names from the command line
    std::vector<std::string> weightfile_response_combinations;
    boost::algorithm::split( weightfile_response_combinations, wf, boost::algorithm::is_any_of(";"));
    for(const auto& wfrc : weightfile_response_combinations){
      //split at last occurence of ':', so that weight files from xrootd locations can be added
      const auto& [weightfilename, responsename] = IOjuggler::split_at_last_occurence(wfrc);
      auto wloc = wd+"/"+weightfilename;//try this location
      if(gSystem->AccessPathName(wloc.data())){
        wloc = weightfilename;//try without wd
        if(gSystem->AccessPathName(wloc.data()))
          throw std::runtime_error("Cannot locate weightfile "+weightfilename);
      }
      msgsvc.infomsg("Adding MVA response \""+responsename+"\" from "+wloc);
      //parsing of xml file stolen from https://gitlab.cern.ch/lhcb/Phys/-/blob/662446b27a7342b59c5af4881696aa11ea1d80f9/Phys/MVADictTools/src/lib/TMVATransform.cpp
      TXMLEngine xmlparser;
      std::vector<std::string> MVAvars, spectators;
      std::string list_of_MVA_vars;
      // Loop over all children (nodes)
      auto child = xmlparser.GetChild(xmlparser.DocGetRootElement(xmlparser.ParseFile(wloc.data())));
      // lambda for adding variables and spectators to the list of MVA variables
      auto parse_xml_children = [&xmlparser,&child,&msgsvc,&list_of_MVA_vars] (std::vector<std::string>& vars, std::string const&& type) -> void {
        auto gchild = xmlparser.GetChild(child);
        while(gchild != nullptr){
          vars.emplace_back(xmlparser.GetAttr(gchild, "Expression"));
          list_of_MVA_vars += "static_cast<float>("+vars.back()+"),";
          msgsvc.debugmsg(type+" variable: "+vars.back());
          gchild = xmlparser.GetNext(gchild);
        }
      };
      while(child != nullptr){
        if(auto nodename = static_cast<std::string>(xmlparser.GetNodeName(child)); nodename == "Variables")
          parse_xml_children(MVAvars,"Training");
        else if(nodename == "Spectators") parse_xml_children(spectators,"Spectator");
        child = xmlparser.GetNext(child);
      }// got variables from xml
      list_of_MVA_vars.pop_back();//remove last colon
      // check if they are known to our RDataFrame instance
      // auto const column_names = df.GetColumnNames();// this is needed to be able to use the == operator below (for some compilers only?)
      // for(const auto& v : MVAvars)
      //   if(std::find(column_names.begin(), column_names.end(), v) == column_names.end())
      //     throw std::runtime_error("The variable "+v+" that has been declared in the MVA weight file "+wloc+" is not known by the current DataFrame");
      // // same for the spectators, except that we don't care if they are really there and define a dummy if they are not
      // for(const auto& v : spectators)
      //   if(std::find(column_names.begin(), column_names.end(), v) == column_names.end()){
      //     msgsvc.infomsg("Spectator variable "+v+" does not exist. Making a dummy...");
      //     df = df.Define(v,[](){return 1.f;});//this should be a branch filled with floating ones
      //   }
      // now, we let the dataframe know that we need all these branches for the MVA
      df = df.Define("rdf_vars_for"+responsename,"std::vector<float>{"+list_of_MVA_vars+"};");
      // init Reader and add spectators
      std::vector<TMVA::Reader*> MVAreaders(current_poolsize,new TMVA::Reader(MVAvars,configtree.get("MVAReaderOpt","Silent")));
      for(auto& reader : MVAreaders)
        for(const auto& v : spectators)
          reader->DataInfo().AddSpectator(v,v,"",0.f,0.f);//AddSpectator is a bit different from AddVariable that is used in the Reader ctor above...
      // book a reader for every slot. the question here is: does it take longer to read in the (quite large) xml files, or the evaluation of the response...
      for(auto sl = 0u; sl < current_poolsize; sl++)
        MVAreaders[sl]->BookMVA(responsename+"_Reader_"+std::to_string(sl),wloc);
      // RDataFrames define magic... capture everything by value here, otherwise bad alloc!
      // TODO: implement rarity
      df = df.DefineSlot(responsename,[=](unsigned int sl, const std::vector<float>& cvar) -> double {
        return MVAreaders[sl]->EvaluateMVA(cvar,responsename+"_Reader_"+std::to_string(sl));}, {"rdf_vars_for"+responsename});
      vars_out.push_back(responsename);
    }//end loop MVAs
  }

  ////////////////////////////////////
  ///  make unfiltered histograms  ///
  ////////////////////////////////////
  [[maybe_unused]] result_ptrs lazy_hists;
  auto const unfiltered_hists = configtree.get_child_optional("unfiltered_hists");
  if(unfiltered_hists) hists_RDF(*unfiltered_hists, lazy_hists, df);

  ///////////////////////////////
  ///  Filter actions (cuts)  ///
  ///////////////////////////////
  if(const auto cut = configtree.get_optional<std::string>("Filter")){
    msgsvc.infomsg("Applying Filter action: "+*cut);
    df = df.Filter(*cut,"main filter   ");
  }
  const auto traceable_cut = configtree.get_child_optional("TraceableFilter");
  if(traceable_cut)
    for(const auto& cutconf : *traceable_cut){
      msgsvc.infomsg("Applying traceable Filter action "+cutconf.first+" : "+cutconf.second.data());
      df = df.Filter(cutconf.second.data(),cutconf.first);
    }
  if(configtree.get("remove_nan_inf",false)){
    msgsvc.infomsg("Rejecting event if the value of one of the chosen varibles is NaN or inf");
    if(add_branch_mode || vars_out.empty())//TODO: implement if needed
      msgsvc.warningmsg("NaN or inf filter only applicable if there is an explicit set of output variables.");
    else {
      std::string ridiculous_hack = "";
      for(const auto& var : vars_out)
        ridiculous_hack += "!(std::isnan("+var+") || std::isinf("+var+")) && ";
      if(ridiculous_hack.size() > 4)
        ridiculous_hack.erase(ridiculous_hack.size() - 4);
      df = df.Filter(ridiculous_hack,"nan inf filter");
    }
  }
  //now all output variables are known
  if(msgsvc.GetMsgLvl() > MSG_LVL::INFO){
    msgsvc.debugmsg("List of output variables:");
    for(const auto& var : vars_out)
      msgsvc.debugmsg(var);
  }
  if(auto const re = configtree.get_optional<unsigned int>("Range_end"))
    df = df.Range(configtree.get("Range_begin",0u),*re,configtree.get("Range_stride",1u));
  //add cut report for printing cut statistics after the snapshot
  auto cut_report = df.Report();

  //////////////////////
  ///  Progress bar  ///
  //////////////////////
  //lazy action that enables to draw the progress bar (the action doesn't take additional time)
  auto lac = df.Count();

  //keep us entertained with a progress bar
  ProgressBar pg;
  pg.start(chain->GetEntries(),msgsvc,nThreads);
  std::mutex barMutex;
  unsigned long long evt = 0ull;
  lac.OnPartialResultSlot(1ull, [&pg,&evt,&barMutex](unsigned int, auto& ) {
    std::lock_guard<std::mutex> l(barMutex); // lock_guard locks the mutex at construction, releases it at destruction
    pg.update(evt);
    evt++;
  });

  /////////////////////////
  ///  make histograms  ///
  /////////////////////////
  auto const hists = configtree.get_child_optional("hists");
  if(hists) hists_RDF(*hists, lazy_hists, df);

  ///////////////////////////////////////////
  ///  Snapshot action (saving the file)  ///
  ///////////////////////////////////////////
  //check here if we want to save a RooDataSet to make the Snapshot lazy
  const auto wsv = configtree.get_child_optional("ws_vars");
  const auto oloc = wd+configtree.get("OutputPrefix","/forWs/")+ofn;
  IOjuggler::dir_exists(oloc,msgsvc);
  // result pointer for lazy snapshots. see https://root-forum.cern.ch/t/rdataframe-iterative-snapshots/32732/8 . We get a warning, but things work as expected.
  ROOT::RDF::RResultPtr<ROOT::RDF::RInterface<ROOT::Detail::RDF::RLoopManager>> snapshot_rptr;
  if(!vars_out.empty()){
    //options for the output file the following options are currently (Dec 18) being updated by the root developers.
    //Check some time later here: https://root.cern.ch/doc/master/Compression_8h_source.html
    ROOT::RDF::RSnapshotOptions rso(configtree.get("outfopt","RECREATE"),
                                    static_cast<ROOT::ECompressionAlgorithm>(configtree.get<unsigned int>("outcompalg",ROOT::RCompressionSetting::EAlgorithm::kInherit)),
                                    configtree.get<unsigned int>("outcomplvl",ROOT::RCompressionSetting::ELevel::kInherit),0,99,static_cast<bool>(wsv));//wsv: make snapshot lazy if we save a ws
    msgsvc.infomsg("Writing output tuple to "+oloc);
    auto const otn = configtree.get("outtreename",itn);
    snapshot_rptr = df.Snapshot(otn,oloc,vars_out,rso);
    if(add_branch_mode) snapshot_rptr.GetValue().GetNSlots(); // just so that the snapshot is triggered in "AddBranchMode"
  }

  // save cut statistics of traceable cut(s) to json files
  if(traceable_cut){
    pt::ptree ptfr;
    for(const auto& cutconf : *traceable_cut){
      auto this_cut = cut_report->At(cutconf.first);
      ptfr.add(cutconf.first+".total", this_cut.GetAll());
      ptfr.add(cutconf.first+".pass",  this_cut.GetPass());
    }
    pt::json_parser::write_json(boost::algorithm::replace_all_copy(oloc, ".root", ".json"), ptfr);
    msgsvc.infomsg("Wrote cut report to "+boost::algorithm::replace_all_copy(oloc, ".root", ".json"));
  }

  //////////////////////////////////////////////
  ///  get variables to put in RooWorkspace  ///
  //////////////////////////////////////////////
  if(wsv){
    //we need this vector so that the RooRealVars don't go out of scope
    std::vector<std::vector<std::unique_ptr<RooRealVar>>> variables(current_poolsize);
    std::vector<RooArgList> alists(current_poolsize);
    std::string list_of_ws_vars;
    const auto dsname = options.get<std::string>("dsname");
    std::vector<RooDataSet> dsets;
    const auto vars_in = df.GetColumnNames();
    for(typename std::decay<decltype(current_poolsize)>::type sl = 0; sl < current_poolsize; sl++){
      for(const auto& v : *wsv){
        if(const auto name = v.first; std::find(vars_in.begin(), vars_in.end(), name) != vars_in.end()){
          list_of_ws_vars += "static_cast<double>("+name+"),";
          std::unique_ptr<RooRealVar> myvar(new RooRealVar(name.data(),name.data(),
                                                           v.second.get("low",-std::numeric_limits<double>::infinity()),
                                                           v.second.get("high",std::numeric_limits<double>::infinity())));
          if(const auto no_duplicate = alists[sl].add(*myvar.get()); no_duplicate)
            variables[sl].push_back(std::move(myvar));
        }
        else msgsvc.warningmsg("Could not add "+name+" to RooDataset, since it is not in output variables, defined by the \"variables\" node");
      }
      //we have to do this awkward exception here, because renaming the RooDataSet during import kills the links to the RooRealVars and nothing will happen when you try to fit something...
      if(sl == 0)
        dsets.emplace_back((dsname).data(),(dsname).data(),alists[sl]);
      else
        dsets.emplace_back((dsname+std::to_string(sl)).data(),(dsname+std::to_string(sl)).data(),alists[sl]);
    }
    list_of_ws_vars.pop_back();
    df = df.Define("rdf_vars_for_ws","std::vector<double>{"+list_of_ws_vars+"};");

    //////////////////////////////////
    ///  fill and save RooDataSet  ///
    //////////////////////////////////
    const auto n_ws_vars = alists[0].getSize();
    auto save_in_ds = [&alists,&dsets,&n_ws_vars] (unsigned int thread, const std::vector<double>& rdf_vars) -> void {
      for(auto i = 0u; i < n_ws_vars; i++)
        static_cast<RooRealVar*>(alists[thread].at(i))->setVal(rdf_vars[i]);
      dsets[thread].add(alists[thread]);
    };
    df.ForeachSlot(save_in_ds,{"rdf_vars_for_ws"});
    std::cout << std::endl;
    for(auto& tds : boost::make_iterator_range(dsets.begin()+1, dsets.end()))
      dsets[0].append(tds);
    const auto wsoloc = wd+configtree.get("WsOutputPrefix","/Ws/")+ofn;
    IOjuggler::dir_exists(wsoloc,msgsvc);
    msgsvc.infomsg("Writing RooWorkspace to "+wsoloc);
    auto wsf = TFile::Open(wsoloc.data(),configtree.get("outfopt","recreate").data());
    RooWorkspace w(options.get<std::string>("wsname").data(),true);
    w.import(dsets[0]);

    /////////////////////////
    ///  create KDE pdfs  ///
    /////////////////////////
    if(auto kde_confs = configtree.get_child_optional("kdepdfs")){
      for(const auto& v : *kde_confs){
        // make new node on which we can do additional cuts/manipulations and not mess with the output.
        ROOT::RDF::RNode adf = df;
        const auto obsname = static_cast<std::string>(v.second.get("observable",(*wsv).front().first));
        const auto obs_rdf = v.second.get("observable_rdf",obsname);
        const auto weight_rdf = v.second.get_optional<std::string>("weight_rdf");
        if(const auto cut = v.second.get_optional<std::string>("Filter"))
          adf = adf.Filter(*cut,"kde filter "+obs_rdf);
        msgsvc.infomsg("Creating KDE p.d.f. from RDataFrame variable "+obs_rdf+" in RooFit observable "+obsname);

        // could speed up things here if we know that the dataset created earlier is the one we want to use for making the KDE pdf.
        std::vector<RooDataSet> kdesets;
        std::vector<RooArgList> kdeobsarglists(current_poolsize);
        std::vector<RooRealVar*> kdeobservables, weights;
        for(typename std::decay<decltype(current_poolsize)>::type sl = 0; sl < current_poolsize; sl++){
          kdeobservables.push_back(static_cast<RooRealVar*>(alists[sl].find(obsname.data())));
          weights.emplace_back(new RooRealVar{(weight_rdf?(*weight_rdf).data():"dummy"), (weight_rdf?(*weight_rdf).data():"dummy"), -999. ,999.});
          kdeobsarglists[sl].add(*kdeobservables.back());
          kdeobsarglists[sl].add(*weights.back());
          //we have to do this awkward exception here, because renaming the RooDataSet during import kills the links to the RooRealVars and nothing will happen when you try to fit something...
          if(sl == 0)
            kdesets.emplace_back((obs_rdf+"_kdeds").data(),(obs_rdf+"_kdeds").data(),kdeobsarglists[sl],weight_rdf?(*weight_rdf).data():"dummy");
          else
            kdesets.emplace_back((obs_rdf+"_kdeds_"+std::to_string(sl)).data(),(obs_rdf+"_kdeds_"+std::to_string(sl)).data(),kdeobsarglists[sl],weight_rdf?(*weight_rdf).data():"dummy");
        }
        std::vector<float> meanQ(current_poolsize, 0.f);
        std::vector<float> sumw(current_poolsize, 0.f);
        const auto obs_lo_limit = kdeobservables[0]->getMin();
        const auto obs_hi_limit = kdeobservables[0]->getMax();
        auto save_in_ds = [&kdesets, obs_lo_limit, obs_hi_limit, &kdeobservables, &meanQ, &sumw] (unsigned int thread, const double rdf_var, const double rdf_wght) -> void {
          if(obs_lo_limit < rdf_var && rdf_var < obs_hi_limit){
            kdeobservables[thread]->setVal(rdf_var);
            kdesets[thread].add(*kdeobservables[thread], rdf_wght);
            meanQ[thread] += rdf_var;
            sumw[thread] += rdf_wght;
          }
        };
        if(weight_rdf) adf.ForeachSlot(save_in_ds,{obs_rdf.data(),(*weight_rdf).data()});
        else adf.Define("dummy","1.").ForeachSlot(save_in_ds,{obs_rdf.data(),"dummy"});
        for(typename std::decay<decltype(current_poolsize)>::type sl = 1; sl < current_poolsize; sl++){
          kdesets[0].append(kdesets[sl]);
          meanQ[0] += meanQ[sl];
          sumw[0] += sumw[sl];
        }

        //make KDE-pdf from MC data
        RooKeysPdf kdepdf(v.first.c_str(),v.first.c_str(),*kdeobservables[0],kdesets[0],
                          static_cast<RooKeysPdf::Mirror>(v.second.get("mirror",3)),
                          static_cast<double>(v.second.get("rho",1.f)));

        //fold it with the detector resolution (optional)
        //it's not possible to directly fold a resolution function with mass dependent width https://root.cern.ch/phpBB3/viewtopic.php?t=14482
        //we keep the resolution fixed at sqrt(<Q>), where <Q> is the energy release of the sample mean of our keys pdf
        if (configtree.get_child_optional("resolution") && static_cast<bool>(v.second.get("fold",1))){
          meanQ[0] /= sumw[0];
          meanQ[0] -= configtree.get("resolution.MassThreshold",0.f);
          auto resolution = sqrt(meanQ[0])*configtree.get("resolution.ResolutionScaling",1.f);
          msgsvc.infomsg(TString::Format("Folding %s with a %.2f MeV Gaussian resolution",kdepdf.GetName(),resolution));
          RooRealVar mg(TString::Format("%smg",kdepdf.GetName()).Data(),"mg",0);
          RooRealVar sg(TString::Format("%ssg",kdepdf.GetName()).Data(),"sg",resolution);
          RooGaussian gauss(TString::Format("%sres",kdepdf.GetName()).Data(),"Gaussian resolution",*kdeobservables[0],mg,sg);
          RooFFTConvPdf kxg(TString::Format("%sxg",kdepdf.GetName()).Data(),"kde (X) gauss",*kdeobservables[0],kdepdf,gauss);
          //w makes copy when importing. so we are safe when importing it in this scope and writing it later to file
          w.import(kxg);
        }
        else w.import(kdepdf);
      }
    }

    if(msgsvc.GetMsgLvl() > MSG_LVL::INFO)
      w.Print();
    wsf->cd();
    w.Write();
    wsf->Close();
    delete wsf;
  }// end putting stuff into RooWorkspace
  else std::cout << std::endl; //clear line after event loop

  //write lazy hists to file. The event loop should have run at this point
  if(hists || unfiltered_hists){
    const auto histoloc = wd+configtree.get("HistOutputPrefix","/hists/")+ofn;
    IOjuggler::dir_exists(histoloc,msgsvc);
    msgsvc.infomsg("Writing histogram output to "+histoloc);
    auto histf = TFile::Open(histoloc.data(),configtree.get("outfopt","recreate").data());
    for(auto lh : lazy_hists)
      std::visit([](auto& h){h->Write();}, lh);
    histf->Close();
    delete histf;
  }
  // reset kEntriesReshuffled bit if we're writing single threaded (to be able to use output as FriendTree)
  if(!vars_out.empty() && (nThreads == 0 || configtree.get_optional<unsigned int>("Range_end"))){
    // reset bit of output file
    IOjuggler::get_obj<TTree>(IOjuggler::get_file(oloc),configtree.get("outtreename",itn))->ResetBit(TTree::EStatusBits::kEntriesReshuffled);
    // for the input file, we can onl be sure to reset it if it's a single tree in a sinlge file.
    const auto input_files = IOjuggler::parse_filenames(ifn,wd,msgsvc);
    if(input_files.size()==1){
      auto input_root_file = IOjuggler::get_file(input_files[0],wd,"UPDATE",msgsvc);
      auto input_ttree = IOjuggler::get_obj<TTree>(input_root_file,itn);
      input_ttree->ResetBit(TTree::EStatusBits::kEntriesReshuffled);
      input_ttree->Write();
      input_root_file->Close();
      msgsvc.debugmsg("Successfully reset kEntriesReshuffled bit in input file.");
    }
    else msgsvc.debugmsg("Could not reset kEntriesReshuffled bit in input files. Merge them before trying to do something fancy with friend trees.");
  }

  msgsvc.infomsg("Cut statistics:");
  cut_report->Print();
  const int time = pg.stop();
  msgsvc.infomsg("Elapsed time for event loop: "+std::to_string(time)+" s");

  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO) clock.Print();

  return 0;
}

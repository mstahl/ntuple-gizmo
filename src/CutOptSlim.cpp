/**
  @date:   2019-05-08
  @author: nskidmor
  @brief:  Slimline version of CutOptScan
*/
//ROOT
#include <TStopwatch.h>
#include <TFile.h>
#include <TString.h>
#include <TError.h>
#include <TCanvas.h>
//RooFit
#include <RooDataSet.h>
#include <RooWorkspace.h>
#include <RooRealVar.h>
#include <RooAbsPdf.h>
#include <RooGlobalFunc.h>
#include <RooMsgService.h>
//C++
#include <cstdlib>
#include <string>
#include <iostream>
#include <vector>
#include <utility>
#include <type_traits>
#include <algorithm>
#include <limits>
#include <functional>
#include <iterator>
//BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
//local
#include <IOjuggler.h>

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

namespace pt = boost::property_tree;


std::vector<std::vector<std::string>> printcombos(std::vector<std::vector<std::string> >& arr) 
//function I found online https://www.geeksforgeeks.org/combinations-from-n-arrays-picking-one-element-from-each-array/
{   
  std::vector<std::vector<std::string>> combos;
  std::cout << "Cut combinations " << std::endl;
    // number of arrays 
    int n = arr.size();  
    // to keep track of next element in each of 
    // the n arrays 
    int* indices = new int[n];  
    // initialize with first element's index 
    for (int i = 0; i < n; i++) 
        indices[i] = 0;  
    while (1) {  
        // print current combination 
      std::vector<std::string> vec;
      
      for (int i = 0; i < n; i++) 
      {
        vec.push_back(arr[i][indices[i]]);
        std::cout << arr[i][indices[i]] << " ";
      }
      std::cout << std::endl;
        combos.push_back(vec);  
        // find the rightmost array that has more 
        // elements left after the current element  
        // in that array 
        int next = n - 1; 
        while (next >= 0 &&  
              (indices[next] + 1 >= arr[next].size())) 
            next--; 
  
        // no such array is found so no more  
        // combinations left 
        if (next < 0) 
            return combos; 
  
        // if found move to next element in that  
        // array 
        indices[next]++; 

 // for all arrays to the right of this  
        // array current index again points to  
        // first element 
        for (int i = next + 1; i < n; i++) 
            indices[i] = 0; 
    } 
    return combos;    
} 


int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();

  //get some stuff, IOjuggler takes care of this
  //parse options passed to executable
  const auto options = IOjuggler::parse_options(argc, argv, "c:d:i:o:r:v:w:h","nonoptions: fold number (optional)",0);
  const auto verbosity = options.get<int>("verbosity");
  MessageService msgsvc("CutOptScan",static_cast<MSG_LVL>(verbosity));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));

  const auto dir = options.get<std::string>("workdir");
  //get ptree
  const auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  //get dataset name (from -r option + fold number)
  const auto file_for_beef = options.get<std::string>("infilename");
  const auto ws_for_beef   = options.get<std::string>("wsname");
  auto ds_for_beef         = options.get<std::string>("dsname");
  auto extras              = options.get_child_optional("extraargs");
  if(extras)
   ds_for_beef += static_cast<std::string>((*extras).front().second.data());

  //some variables that are needed for the scan
  const auto nsigma         = configtree.get("nsigma",0.0);
  const auto width_var      = configtree.get<std::string>("width_var","");
  const auto mean_var       = configtree.get<std::string>("mean_var","");
  const auto sigpdfname     = configtree.get_optional<std::string>("sig_pdf");
  const auto bkgpdfname     = configtree.get_optional<std::string>("bkg_pdf");
  const auto Nsig_var       = configtree.get_optional<std::string>("Nsig_var");
  const auto Nbkg_var       = configtree.get_optional<std::string>("Nbkg_var");
  const auto beef_verbosity = configtree.get("beef_verbosity","2");
  if(!sigpdfname || !bkgpdfname || !Nsig_var || !Nbkg_var) throw std::runtime_error("\"sig_pdf\",\"bkg_pdf\",\"Nsig_var\" or \"Nbkg_var\" not found in config file");
  auto FoM_mode = configtree.get("FoM_mode",0);
  if(0 < FoM_mode || FoM_mode > 1){
    msgsvc.warningmsg("FoM mode " + std::to_string(FoM_mode) + " not known. Setting to 0: S/sqrt(S*B)");
    FoM_mode = 0;
  }

  //safety checks
  if(!configtree.get_child_optional("vars")) throw std::runtime_error("child \"vars\" not found in config file");
  if(!configtree.get_optional<std::string>("beef_config")) throw std::runtime_error("\"beef_config\" not found in config file");

  const auto nvars = configtree.get_child("vars").size();
  std::vector<std::string> varnames(nvars);
  std::vector<std::string> lowcuts(nvars);
  std::vector<std::string> highcuts(nvars);
  std::vector<std::string> numcuts(nvars);
  std::vector<std::string> cutdistances(nvars);

  std::vector<std::vector<std::string>> cuts(nvars);
  int ivar=0;  
  for(const auto& var : configtree.get_child("vars")){

    varnames[ivar]=var.first;   
    double lowcut=(var.second.get<float>("lowcut"));
    double highcut=(var.second.get<float>("highcut"));
    double cutdistance;
    if((var.second.get<float>("highcut")-var.second.get<float>("lowcut")<1e-5))
      cutdistance=999;
    else cutdistance=((var.second.get<float>("highcut")-var.second.get<float>("lowcut"))/var.second.get<float>("numcuts"));
    
    for(double i=lowcut; i<=highcut; i+=cutdistance)
      cuts[ivar].push_back(std::to_string(i));
    ivar++;     
    
  }

  std::vector<std::vector<std::string>> combos=printcombos(cuts);
  
  std::vector<std::string> cutstrings;
  std::vector<std::string> appendices;
  
  for(const auto& combo : combos)
  {    
    std::string cutstring="";
    for(int i=0; i<nvars; i++)
      cutstring+=varnames[i] + " > " + combo[i] + " && "; 
  
    cutstring.erase(cutstring.end()-4, cutstring.end());    
    cutstrings.push_back(cutstring);
    
    std::string appendix=cutstring;
    appendix.erase(remove_if(appendix.begin(), appendix.end(), isspace), appendix.end());   
    std::replace(appendix.begin(), appendix.end(), '>', '_');
    std::replace(appendix.begin(), appendix.end(), '&', '_');
    appendices.push_back(appendix);
    
  }

//lambda to call beef and calculate the FoM
  auto fit_and_get_FoM = [&] (auto&& cutstring, auto&& appendix) {
    //debug    
    msgsvc.debugmsg(std::string(128,'*'));
    msgsvc.debugmsg("At appendix: " + appendix + " . Current cutstring:");
    msgsvc.debugmsg(cutstring); 
    
    //replace the new cutstring with the dummy in the beef config file
    auto beef_config = IOjuggler::get_ptree(configtree.get<std::string>("beef_config"));
    IOjuggler::replace_stuff_in_ptree(beef_config,"INSERT_CUT_HERE",static_cast<std::string>(cutstring));
    //stuff for beef
    const std::string tmpdir = "tmp_forScan/";
    const auto tmp_config    = dir + "/" + tmpdir + "config" + appendix + ".info";
    const auto tmp_outfile   = tmpdir + "fitresults" + appendix + ".root";
    const auto plotname      = tmpdir + "plot" + appendix;
    IOjuggler::replace_stuff_in_ptree(beef_config,"INSERT_PLOTNAME_HERE",plotname);
    pt::write_info(tmp_config.data(),beef_config);
    //call beef from command line
    auto dumpit = std::system(("build/bin/beef"
                               " -c " + tmp_config +
                               " -i " + file_for_beef +
                               " -o " + tmp_outfile +
                               " -d " + dir +
                               " -w " + ws_for_beef +
                               " -r " + ds_for_beef +
                               " -v " + beef_verbosity).data());
//todo: use fit status
    if(dumpit != 0)msgsvc.debugmsg("beef finished with status " + std::to_string(dumpit));//to please the CI compiler
    //get FoM
    //read beef's output
    const auto tmpin = IOjuggler::get_file(tmp_outfile,dir);
    auto tmpws = IOjuggler::get_obj<RooWorkspace>(tmpin,"w");//"w" hardcoded in beef
    if(beef_config.get_child("observables").size() > 1)throw std::runtime_error("Sorry, can't do this scan for multidimensional fits");
    auto tmpobs = IOjuggler::get_wsobj<RooRealVar>(tmpws,beef_config.get_child("observables").front().first);
    //make signal-range
    std::string range_name;
    
    const auto tmpmean  = IOjuggler::get_wsobj<RooRealVar>(tmpws,mean_var )->getVal();
    const auto tmpwidth = IOjuggler::get_wsobj<RooRealVar>(tmpws,width_var)->getVal();
    //this range will change every time, so we need a unique name
    range_name = "SignalRange" + appendix;
    tmpobs->setRange(range_name.data(),tmpmean - nsigma*tmpwidth,tmpmean + nsigma*tmpwidth);

    //get signal and background yields in the signal range
    const auto sigyield = IOjuggler::get_wsobj<RooRealVar>(tmpws,*Nsig_var)->getVal() * IOjuggler::get_wsobj<RooAbsPdf>(tmpws,*sigpdfname)->createIntegral(*tmpobs,RooFit::NormSet(*tmpobs),RooFit::Range(range_name.data()))->getVal();
    const auto bkgyield = IOjuggler::get_wsobj<RooRealVar>(tmpws,*Nbkg_var)->getVal() * IOjuggler::get_wsobj<RooAbsPdf>(tmpws,*bkgpdfname)->createIntegral(*tmpobs,RooFit::NormSet(*tmpobs),RooFit::Range(range_name.data()))->getVal();
    const auto current_fom = FoM_mode == 0 ? sigyield/sqrt(sigyield+bkgyield) : sigyield/sqrt(sigyield+bkgyield)*sigyield/(sigyield+bkgyield);
    msgsvc.debugmsg(TString::Format("%-20.20s: %s","Nsig in range",  std::to_string(sigyield).data()));
    msgsvc.debugmsg(TString::Format("%-20.20s: %s","Nbkg in range",  std::to_string(bkgyield).data()));
    msgsvc.debugmsg(TString::Format("%-20.20s: %s","Figure of Merit",std::to_string(current_fom).data()));
    //very important to close the file. could result in a segfault for big scans (see: https://sft.its.cern.ch/jira/si/jira.issueviews:issue-html/ROOT-3785/ROOT-3785.html)
    tmpin->Close();
    delete tmpin;
    return current_fom;
  };
  
  
  std::ofstream TCutout; 
  TCutout.open((dir + "/" + options.get<std::string>("outfilename") + static_cast<std::string>((*extras).front().second.data()) + ".info").data());
  
  std::vector<std::pair<std::string,float> >  scan_results;
  for(int i=0; i<cutstrings.size(); i++){   
    scan_results.emplace_back(cutstrings[i], fit_and_get_FoM(cutstrings[i], appendices[i]));
    TCutout << scan_results[i].first + " : " +  scan_results[i].second << std::endl;
  }
  
  TCutout.close();
  
  //Need to save the plots as .root files for the following
  TCanvas *c= new TCanvas("c", "First canvas", 1600, 1200); 
  c->Divide(cutstrings.size()/2,cutstrings.size()/2);
  
  int counter=1; 
  for(const auto& app : appendices){
    std::string infile="tmp_forScan/plot" + app + ".root";
    const auto tmpin = IOjuggler::get_file(infile,dir);
    TCanvas *subc = (TCanvas *)tmpin->Get("c1");   
    c->cd(counter);
    subc->DrawClonePad();
    c->Update();    
    counter++;
  }
  c->SaveAs("fitresults.pdf");
  
  clock.Stop();
  clock.Print();
  return 0;
}



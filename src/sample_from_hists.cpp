/**
  @date:   2019-12-01
  @author: mstahl
  @brief:  simple script to do sampling from a list of TH1s and save the ouput in a TTree
*/
//local
#include <IOjuggler.h>
#include <ProgressBar.h>
//ROOT
#include "TH1F.h"
#include "TStopwatch.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();

  auto options = IOjuggler::parse_options(argc, argv, "d:i:n:o:v:h","nonoptions: a list of input histograms",1);
  MessageService msgsvc("sample_from_hists",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));

  auto const wd  = options.get<std::string>("workdir");
  auto const ifn = options.get<std::string>("infilename");
  auto const n   = options.get<unsigned int>("n");
  //input file
  auto const input_file = IOjuggler::get_file(ifn,wd);
  //output file and tree
  auto const oloc = wd+"/"+options.get<std::string>("outfilename");
  IOjuggler::dir_exists(oloc,msgsvc);
  auto output_file = TFile::Open(oloc.data(),"recreate");
  TTree t("t","t");
  //histograms to sample from
  std::vector<std::pair<TH1F*,double>> input_hists;
  auto const hist_list = options.get_child("extraargs");
  for(auto const& hist_name : hist_list)
    input_hists.emplace_back(IOjuggler::get_obj<TH1F>(input_file,hist_name.second.data()),0.);
  //can't declare the branch in the same loop using input_hists.back(), since this screws up the pointer addresses. this is very very odd...
  for(auto& hist : input_hists)
    t.Branch(hist.first->GetName(),&hist.second);

  msgsvc.infomsg("Ready to sample from " + std::to_string(hist_list.size()) + " histograms living in file " + ifn);
  //do the sampling
  ProgressBar pg;
  pg.start(n,msgsvc,1);
  for(unsigned int evt = 0; evt < n; evt++){
    pg.update(evt);
    for(auto& hist : input_hists)
      hist.second = hist.first->GetRandom();
    t.Fill();
  }
  const int time = pg.stop();
  msgsvc.infomsg("Elapsed time for sampling loop: "+std::to_string(time)+" s");

  t.Write();
  output_file->Write();output_file->Close();

  msgsvc.infomsg("Done with sampling. Branches ...");
  for(auto const& hist : input_hists)
    msgsvc.infomsg(hist.first->GetName());
  msgsvc.infomsg("... have been written to tree \"t\" in " + oloc);

  clock.Stop();
  clock.Print();
  return 0;
}

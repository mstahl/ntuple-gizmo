/**
  * @file weight_2d.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2021-11-02
  * @brief Weighting in 2 dimensions with an adaptive binning method.
  */

#include "TStopwatch.h"
#include "ROOT/RDataFrame.hxx"
#include <TH2A.h>
#include <IOjuggler.h>

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  // Weight histo from -r option to the target hist from -t. Use a binning that suits both distributions.
  // Once the weighting scheme has been defined, we can loop through the input sample -i and additional samples in the nonoptions to apply the weights.
  // At some point these weights will have to be re-normalized, which can be done here or in a subsequent step.
  auto options = IOjuggler::parse_options(argc, argv, "c:d:hi:j:n:o:r:t:v:","nonoptions: more files and trees to be weighted <file:tree>",0);
  MessageService msgsvc("weight_2d",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  //TH2A::SetVerbosity(options.get<int>("verbosity"));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: "+static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  const auto wd  = options.get<std::string>("workdir");

  // runtime configuration
  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  //append and replace stuff in ptree
  IOjuggler::replace_from_cmdline(configtree,options.get<std::string>("joker"));
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);

  // read config and prepare output
  const auto cvx = configtree.get<std::string>("calib_var_x");
  const auto cvy = configtree.get<std::string>("calib_var_y");
  const auto ow = configtree.get_optional<std::string>("origin_weight");
  const auto wn = configtree.get("output_branch_name","re_weight");
  const auto normalize_weights = static_cast<bool>(configtree.get("normalize_weights",0u));
  const auto trl = configtree.get("truncation_limit",-1.);
  const auto oloc = wd+"/"+boost::algorithm::replace_all_copy(options.get<std::string>("outfilename"), wd, "");
  msgsvc.infomsg("Writing output tuple to "+oloc);
  IOjuggler::dir_exists(oloc,msgsvc);

  // parse hist- and filenames
  const auto& [rfn, rhn] = IOjuggler::split_at_last_occurence(options.get<std::string>("dsname"));
  const auto& [tfn, thn] = IOjuggler::split_at_last_occurence(options.get<std::string>("treename"));
  TH2A wght_scheme; // create TH2A object
  msgsvc.infomsg("Weighting "+rhn+" from file "+rfn+" to match "+thn+" from "+tfn+".");
  // do weighting
  wght_scheme.Weight(*IOjuggler::get_obj<TH2F>(IOjuggler::get_file(rfn,wd),rhn),*IOjuggler::get_obj<TH2F>(IOjuggler::get_file(tfn,wd),thn),options.get<unsigned int>("n"),configtree.get("truncation_factor",25.));
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) {wght_scheme.Print();}

  // lambda to loop over events in input tree, fill and persist weights.
  auto apply_weights = [&cvx, &cvy, &ow, &wn, normalize_weights, &oloc, &wght_scheme, &trl, &configtree, &msgsvc] (std::unique_ptr<TChain> c, const std::string&& out_treename, const std::string&& fileopt) -> void {
    ROOT::RDataFrame d(*c.get());
    ROOT::RDF::RNode df = d;
    // don't filter NaN or inf, but set to 0, otherwise the weights might not be usable as friend trees
    if(ow) df = df.Define(normalize_weights ? "tmpw" : wn,[&wght_scheme,trl, &msgsvc](const double cvxl, const double cvyl, const double owl) -> double {
        const auto w = owl*wght_scheme.GetBinContent(wght_scheme.FindBin(cvxl,cvyl));
        if(cvxl < wght_scheme.GetXMin() || wght_scheme.GetXMax() < cvxl || cvyl < wght_scheme.GetYMin() || wght_scheme.GetYMax() < cvyl){
          if(w>0) msgsvc.debugmsg(TString::Format("w %.6g cvxl %.6g cvyl %.6g", w, cvxl, cvyl));
          return 0.;
        }
        if(std::isnan(w) || std::isinf(w)) return 0.;
        else return w > trl && trl > 0. ? trl : w;
      },{cvx,cvy,*ow});
    else df = df = df.Define(normalize_weights ? "tmpw" : wn,[&wght_scheme,trl, &msgsvc](const double cvxl, const double cvyl) -> double {
        const auto w = wght_scheme.GetBinContent(wght_scheme.FindBin(cvxl,cvyl));
        if(cvxl < wght_scheme.GetXMin() || wght_scheme.GetXMax() < cvxl || cvyl < wght_scheme.GetYMin() || wght_scheme.GetYMax() < cvyl){
          if(w>0) msgsvc.debugmsg(TString::Format("w %.6g cvxl %.6g cvyl %.6g", w, cvxl, cvyl));
          return 0.;
        }
        if(std::isnan(w) || std::isinf(w)) return 0.;
        else return w > trl && trl > 0. ? trl : w;
      },{cvx,cvy});
    if(normalize_weights){
      // df = df.Define("tmpwsq",[&trl](const double& w) -> double {return std::pow(w,2);},{"tmpw"});
      // auto norm = df.Sum<double>("tmpw").GetValue()/df.Sum<double>("tmpwsq").GetValue();// triggers event loop
      // msgsvc.debugmsg(TString::Format("Norm %s %.6g", out_treename.data(), norm));
      // df = df.Define(wn,[&norm](const double& w) -> double {return w*norm;},{"tmpw"});
      const auto norm = (ow ? df.Sum<double>(*ow).GetValue() : 1.)/df.Sum<double>("tmpw").GetValue();
      df = df.Define(wn,[norm](const double w) -> double {return w*norm;},{"tmpw"});
    }
    ROOT::RDF::RSnapshotOptions rso(fileopt, static_cast<ROOT::ECompressionAlgorithm>(configtree.get<unsigned int>("outcompalg",ROOT::RCompressionSetting::EAlgorithm::kInherit)),
                                    configtree.get<unsigned int>("outcomplvl",ROOT::RCompressionSetting::ELevel::kInherit),0,99,false);
    [[maybe_unused]] auto snapshot_rptr = df.Snapshot(out_treename,oloc,{wn},rso);
  };
  // do it - for the regular input
  msgsvc.infomsg("Applying weights from weighting to input sample.");
  const auto& [ifn, itn] = IOjuggler::split_at_last_occurence(options.get<std::string>("infilename"));
  apply_weights(IOjuggler::get_chain(itn, ifn, wd, msgsvc), configtree.get("output_tree",itn+"weights"), "RECREATE");

  // loop nonoptions to apply weighting scheme for additional inputs
  if(auto eas = options.get_child_optional("extraargs"); (*eas).size() > 0){
    msgsvc.infomsg("Applying weights from weighting to additional samples.");
    for(const auto& ea : *eas){//iterate non-options
      const auto& [efn, etn] = IOjuggler::split_at_last_occurence(ea.second.data());
      apply_weights(IOjuggler::get_chain(etn, efn, wd, msgsvc), boost::algorithm::replace_all_copy(IOjuggler::split_at_last_occurence(efn,"/").back(), ".root", "")+"weights", "UPDATE");
    }
  }
  if(const auto th2name = configtree.get_optional<std::string>("persist_th2a")){
    auto ofile = IOjuggler::get_file(oloc,wd,"UPDATE",msgsvc);
    wght_scheme.Write((*th2name).data());
    ofile->Close();
  }
  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO) {clock.Print();}
  return 0;
}

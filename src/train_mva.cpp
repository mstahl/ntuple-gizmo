/**
  @date:   2017-05-12
  @author: mstahl
  @brief:  TMVA traning wrapper using boost property trees.
           documentation: https://gitlab.cern.ch/sneubert/ntuple-gizmo/blob/master/doc/trainMVA.md
*/
//TMVA
#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/Config.h"
#include "TMVA/DataLoader.h"
#include "TMVA/CrossValidation.h"
//local
#include <IOjuggler.h>

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  //get some stuff, IOjuggler takes care of this
  //parse options passed to executable
  auto options = IOjuggler::parse_options(argc, argv, "b:c:d:j:o:s:hv:");
  MessageService msgsvc("trainMVA",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: "+static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  auto wd = options.get<std::string>("workdir");
  //get config-file
  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  // runtime config file manipulations
  IOjuggler::replace_from_cmdline(configtree,options.get<std::string>("joker"));
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);

  // input data
  const auto bkg_inputs = IOjuggler::split_at_last_occurence(options.get<std::string>("bkg_inputs"));
  const auto sig_inputs = IOjuggler::split_at_last_occurence(options.get<std::string>("sig_inputs"));
  auto bkg_chain = IOjuggler::get_chain(bkg_inputs[1],bkg_inputs[0],wd,msgsvc);
  auto sig_chain = IOjuggler::get_chain(sig_inputs[1],sig_inputs[0],wd,msgsvc);
  //TODO: add friend tree(s). Need to enable extraopts
  // [[maybe_unused]] auto bkg_fes = IOjuggler::get_friends(options,bkg_chain,msgsvc);

  //Create a ROOT output file where TMVA will store ntuples, histograms, etc.
  const auto of = options.get<std::string>("outfilename");
  const auto oloc = (wd.empty() || of.compare(0,1,"/") == 0) ? of : wd+"/"+of;
  IOjuggler::dir_exists(oloc,msgsvc);
  auto output_file = TFile::Open(oloc.data(),"RECREATE");

  ROOT::EnableImplicitMT();//need to call this first to be able to call ROOT::GetThreadPoolSize()
  unsigned int current_poolsize;
  #if ROOT_VERSION_CODE > ROOT_VERSION(6,22,05)
  current_poolsize = ROOT::GetThreadPoolSize();
#else
  current_poolsize = ROOT::GetImplicitMTPoolSize();
#endif
  const auto nThreads = std::min(configtree.get("threads",0u),current_poolsize);
  if(nThreads == 0){
    ROOT::DisableImplicitMT();
    current_poolsize = 1;
    msgsvc.infomsg("Implicit multithreading is switched off");
  }
  else{
    ROOT::EnableImplicitMT(nThreads);
    msgsvc.infomsg("Trying to run implicit multithreading with "+std::to_string(nThreads)+" threads on "+ std::to_string(current_poolsize)+" slots."
                   "This might only work for gradient BDTS or DNNs");
  }

  //set output directory of xml and class file
  TMVA::Tools::Instance();
  TMVA::gConfig().GetIONames().fWeightFileDir = configtree.get<std::string>("WeightFileDir",(wd.empty()?wd:wd+"/")+configtree.get<std::string>("WeightFileDirSuffix",""));

  //lambda for testing and training individual tasks
  auto train_and_test = [&msgsvc,&wd,&output_file,&bkg_chain,&sig_chain] (pt::ptree& taskconfig, const std::string& factory_name) {

    TStopwatch task_clock;
    task_clock.Start();

    //check if input variables exist. skip them, if not
    auto vars = taskconfig.get_child_optional("variables");
    if(!vars)
      throw std::runtime_error("\"variables\" node is missing. Aborting");
    auto specs = taskconfig.get_child_optional("spectators");

    if(taskconfig.get("ignore_missing_variables",0))
      for(const auto& var : *vars){
        if(sig_chain->GetLeaf(var.first.data()) == nullptr || bkg_chain->GetLeaf(var.first.data()) == nullptr){
          msgsvc.warningmsg("Variable \""+var.first+"\" not found in signal or background tree. Removing it from training");
          (*vars).erase(var.first);
        }
      if(specs)
        for(const auto& spec : *specs)
          if(sig_chain->GetLeaf(spec.first.data()) == nullptr || bkg_chain->GetLeaf(spec.first.data()) == nullptr){
            msgsvc.warningmsg("Spectator variable \""+spec.first+"\" not found in signal or background tree. Removing it from training");
            (*specs).erase(spec.first);
          }
      }

    for(const auto& var : *vars){
      sig_chain->SetBranchStatus(var.first.data(),true);
      bkg_chain->SetBranchStatus(var.first.data(),true);
    }
    if(specs)
     for(const auto& spec : *specs){
       sig_chain->SetBranchStatus(spec.first.data(),true);
       bkg_chain->SetBranchStatus(spec.first.data(),true);
     }

    auto load_data = [&] (auto& data_loader) -> void {
      //add input variables to train MVA
      for(const auto& var : *vars)
        data_loader.AddVariable(var.first, var.second.get("label",var.first), var.second.get("unit"," "),
                                      static_cast<char>(var.second.get("type",'F')),var.second.get("low",0.),var.second.get("high",0.));

      //add spectator variables to train MVA
      if(specs)
        for(const auto& spec : *specs)
          data_loader.AddSpectator(spec.first,spec.second.get("label",spec.first),spec.second.get("unit"," "),
                                         spec.second.get("low",0.),spec.second.get("high",0.));

      //get signal and background chains, add them to the factory
      data_loader.AddSignalTree    (sig_chain.get());
      data_loader.AddBackgroundTree(bkg_chain.get());

      //use weights
      if(auto sig_weight = taskconfig.get_optional<std::string>("sig_weightname"))
        data_loader.SetSignalWeightExpression(*sig_weight);
      if(auto bkg_weight = taskconfig.get_optional<std::string>("bkg_weightname"))
        data_loader.SetBackgroundWeightExpression(*bkg_weight);

      //Prepare Testing and Training. Apply additional cuts on the signal and background samples (can be different)
      data_loader.PrepareTrainingAndTestTree(TCut(taskconfig.get("sig_cut","").data()), TCut(taskconfig.get("bkg_cut","").data()), taskconfig.get<std::string>("traintestopt"));
    };

    if(taskconfig.get("CrossValidation",0)){
      msgsvc.infomsg("Running CrossValidation");
      TMVA::DataLoader dataloader("");
      load_data(dataloader);
      TMVA::CrossValidation cv(factory_name, &dataloader, output_file, taskconfig.get<std::string>("factory_option"));
      //book TMVA methods
      for(const auto& meth : taskconfig.get_child("method"))
        cv.BookMethod(static_cast<TMVA::Types::EMVA>(meth.second.get<int>("type")), meth.first, meth.second.get<std::string>("option"));
      cv.Evaluate();
    }
    else{
      //Create TMVA factory which handles the training and testing
      TMVA::Factory factory(factory_name, output_file, taskconfig.get<std::string>("factory_option"));
      TMVA::DataLoader dataloader("");
      load_data(dataloader);
      //book TMVA methods
      for(const auto& meth : taskconfig.get_child("method"))
        factory.BookMethod(&dataloader, static_cast<TMVA::Types::EMVA>(meth.second.get<int>("type")), meth.first, meth.second.get<std::string>("option"));

      // Train MVAs using the set of training events
      factory.TrainAllMethods();
      // ---- Evaluate all MVAs using the set of test events
      factory.TestAllMethods();
      // ----- Evaluate and compare performance of all configured MVAs
      factory.EvaluateAllMethods();
    }

    task_clock.Stop();
    if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)task_clock.Print();

  };

  //loop all tasks individually for training and testing
  for(auto& task : configtree.get_child("tasks"))
    train_and_test(task.second,task.first);

  msgsvc.infomsg(TString::Format("==> View results with:\nTMVA::TMVAGui(\"%s\")",output_file->GetName()));
  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)clock.Print();

}

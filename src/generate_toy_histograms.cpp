/**
  @date:   2022-07-30
  @author: mstahl
  @brief:  simple script generate a number of output histrograms from a single input histogram
  @todo:
    - template histogram type
    - add spline (e.g. Manuel's cubic spline, reused in Laura++) or KDE methods (Meerkat, root) for smearing the input
    - make things like binning more generic (can use config file if more options are needed)
*/
//local
#include <IOjuggler.h>
#include <ProgressBar.h>
//ROOT
#include "TH2D.h"
#include "TStopwatch.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){
  TStopwatch clock;
  clock.Start();
  // parse command line arguments
  auto options = IOjuggler::parse_options(argc, argv, "d:i:m:n:o:t:v:h");
  MessageService msgsvc("generate_toy_histograms",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  const auto wd      = options.get<std::string>("workdir");
  const auto n       = options.get<unsigned int>("n");
  const auto nbinsxy = options.get<unsigned int>("weightfile");
  // input
  TH2D* input_hist{IOjuggler::get_obj<TH2D>(IOjuggler::get_file(options.get<std::string>("infilename"),wd),options.get<std::string>("treename"))};
  // output
  const auto oloc = wd+"/"+boost::algorithm::replace_all_copy(options.get<std::string>("outfilename"), wd, "");
  IOjuggler::dir_exists(oloc,msgsvc);
  auto output_file = TFile::Open(oloc.data(),"recreate");
  std::vector<TH2D> output_hists(n);
  // do the work
  // TODO: implement different smooting methods
  input_hist->Smooth(/*1,"k5a"*/);
  ProgressBar pg;
  pg.start(n,msgsvc,1);
  for(auto i=0u; i<n; i++){
    pg.update(i);
    output_hists[i] = TH2D((static_cast<std::string>(input_hist->GetName())+"_toy_"+std::to_string(i)).data(),"",
                            nbinsxy, input_hist->GetXaxis()->GetXmin(), input_hist->GetXaxis()->GetXmax(),
                            nbinsxy, input_hist->GetYaxis()->GetXmin(), input_hist->GetYaxis()->GetXmax());
    output_hists[i].FillRandom(input_hist, input_hist->GetEntries());
    output_hists[i].Write();
  }
  pg.stop();
  output_file->Write();
  output_file->Close();
  msgsvc.infomsg("");
  clock.Stop();
  clock.Print();
  return 0;
}
/**
  * @file weight_1d.cpp
  * @version 0.1
  * @author Marian Stahl
  * @date 2022-10-23
  * @brief Weighting in 1 dimension.
  */

#include "TStopwatch.h"
#include "ROOT/RDataFrame.hxx"
#include "TH1D.h"
#include <IOjuggler.h>

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

int main(int argc, char** argv){

  TStopwatch clock;
  clock.Start();
  // Weight histo from -r option to the target hist from -t. Use a binning that suits both distributions.
  // Once the weighting scheme has been defined, we can loop through the input sample -i and additional samples in the nonoptions to apply the weights.
  // At some point these weights will have to be re-normalized, which can be done here or in a subsequent step.
  auto options = IOjuggler::parse_options(argc, argv, "c:d:hi:j:n:o:t:v:","nonoptions: more files and trees to be weighted <file:tree>",0);
  MessageService msgsvc("weight_1d",static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: "+static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  const auto wd = options.get<std::string>("workdir");

  // runtime configuration
  auto configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  //append and replace stuff in ptree
  IOjuggler::replace_from_cmdline(configtree,options.get<std::string>("joker"));
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG)
    IOjuggler::print_ptree(configtree);

  // read config and prepare output
  const auto cv = configtree.get<std::string>("calib_var");
  const auto ow = configtree.get<std::string>("origin_weight","one");
  const auto tw = configtree.get<std::string>("target_weight","one");
  const auto wn = configtree.get("output_branch_name","weight_1d");
  const auto normalize_weights = static_cast<bool>(configtree.get("normalize_weights",0u));
  const auto oloc = wd+"/"+boost::algorithm::replace_all_copy(options.get<std::string>("outfilename"), wd, "");
  msgsvc.infomsg("Writing output tuple to "+oloc);
  IOjuggler::dir_exists(oloc,msgsvc);

  // parse tree- and filenames
  const auto& [rfn, rtn] = IOjuggler::split_at_last_occurence(options.get<std::string>("infilename"));
  const auto& [tfn, ttn] = IOjuggler::split_at_last_occurence(options.get<std::string>("treename"));
  msgsvc.infomsg("Weighting "+rtn+" from file "+rfn+" to match "+ttn+" from "+tfn+".");
  std::vector<std::pair<double,double>> cvs_origin, cvs_target;
  const auto oc = IOjuggler::get_chain(rtn, rfn, wd, msgsvc);
  ROOT::RDataFrame origin{*oc.get()};
  ROOT::RDF::RNode origin_df = origin;
  if(ow=="one") origin_df = origin_df.Define(ow,"1.");
  origin_df.Foreach([&cvs_origin](const double tmp_cv, const double tmp_wght){cvs_origin.emplace_back(tmp_cv,tmp_wght);}, {cv,ow});
  const auto tc = IOjuggler::get_chain(ttn, tfn, wd, msgsvc);
  ROOT::RDataFrame target{*tc.get()};
  ROOT::RDF::RNode target_df = target;
  if(tw=="one") target_df = target_df.Define(tw,"1.");
  target_df.Foreach([&cvs_target](const double tmp_cv, const double tmp_wght){cvs_target.emplace_back(tmp_cv,tmp_wght);}, {cv,tw});
  std::sort(cvs_origin.begin(),cvs_origin.end(),[](auto& cv_wght_a, auto& cv_wght_b){return cv_wght_a.first < cv_wght_b.first;});
  std::sort(cvs_target.begin(),cvs_target.end(),[](auto& cv_wght_a, auto& cv_wght_b){return cv_wght_a.first < cv_wght_b.first;});

  const auto min_sumw_per_bin = options.get<double>("n");
  std::vector<double> binedges, bincontents;
  binedges.push_back(configtree.get<double>("calib_var_lo"));
  double origin_sumw = 0., target_sumw = 0.;
  auto truncation = configtree.get("truncation_factor",0.);
  if(truncation > 0.) truncation *= target_df.Sum<double>(tw).GetValue()/origin_df.Sum<double>(ow).GetValue();// triggers event loop
  // for filling the last bin
  auto last_bin_origin_sumw = 0., last_bin_target_sumw = 0.;
  auto origin_idx = 0u, target_idx = 0u;
  msgsvc.debugmsg(TString::Format("cvs_origin.size() %u cvs_target.size() %u",cvs_origin.size(),cvs_target.size()));
  for(; origin_idx < cvs_origin.size() && target_idx < cvs_target.size(); origin_idx++, target_idx++){
    msgsvc.debugmsg(TString::Format("Within bin -- origin_idx %u target_idx %u origin_cv %.6g target_cv %.6g origin_sumw %.6g target_sumw %.6g",
                     origin_idx, target_idx, cvs_origin[origin_idx].first, cvs_target[target_idx].first, origin_sumw, target_sumw));
    origin_sumw += cvs_origin[origin_idx].second;
    target_sumw += cvs_target[target_idx].second;
    // make new binedge if we're at or above the desired number of entries/sum of weights
    if(origin_sumw >= min_sumw_per_bin && target_sumw >= min_sumw_per_bin){
      const auto tmp_binedge = std::max(cvs_origin[origin_idx].first, cvs_target[target_idx].first);
      binedges.push_back(tmp_binedge);
      // advance both vectors to the next control variable value that is greater than the current binedge (then go one step back as this will be the first entry of the next bin);
      // and accumulate the weights that belong in this bin but haven't been counted yet
      const auto old_origin_index = origin_idx;
      origin_idx = std::distance(cvs_origin.begin(),std::find_if(std::next(cvs_origin.begin(),origin_idx),cvs_origin.end(),[tmp_binedge](const auto a){return a.first > tmp_binedge;}));
      if(origin_idx > old_origin_index)
        origin_sumw = std::accumulate(std::next(cvs_origin.begin(),old_origin_index),std::next(cvs_origin.begin(),origin_idx),origin_sumw,[](double tmp_sum, const auto a){return tmp_sum + a.second;});
      const auto old_target_index = target_idx;
      target_idx = std::distance(cvs_target.begin(),std::find_if(std::next(cvs_target.begin(),target_idx),cvs_target.end(),[tmp_binedge](const auto a){return a.first > tmp_binedge;}));
      if(target_idx > old_target_index)
        target_sumw = std::accumulate(std::next(cvs_target.begin(),old_target_index),std::next(cvs_target.begin(),target_idx),target_sumw,[](double tmp_sum, const auto a){return tmp_sum + a.second;});

      // set the bin content and reset values for the next bin
      const auto tmp_bincontent = target_sumw/origin_sumw;
      bincontents.push_back(tmp_bincontent > truncation && truncation > 0. ? truncation : tmp_bincontent);
      last_bin_origin_sumw = origin_sumw; last_bin_target_sumw = target_sumw;
      msgsvc.debugmsg(TString::Format("origin_idx %u target_idx %u origin_sumw %.6g target_sumw %.6g bincontents.back() %.6g binedges.back() %.6g",origin_idx, target_idx, origin_sumw, target_sumw, bincontents.back(), binedges.back()));
      target_sumw = 0.; origin_sumw=0.;
    }
  }
  // often the loop finishes with a remainder; let's add it to the last bin
  if(origin_sumw != 0 || target_sumw != 0){
    if(origin_idx != cvs_origin.size()) origin_sumw += std::accumulate(std::next(cvs_origin.begin(),origin_idx),cvs_origin.end(),origin_sumw,[](double tmp_sum, const auto a){return tmp_sum + a.second;});
    if(target_idx != cvs_target.size()) target_sumw = std::accumulate(std::next(cvs_target.begin(),target_idx),cvs_target.end(),target_sumw,[](double tmp_sum, const auto a){return tmp_sum + a.second;});
    origin_sumw += last_bin_origin_sumw;
    target_sumw += last_bin_target_sumw;
    binedges.back() = configtree.get<double>("calib_var_hi");
    const auto tmp_bincontent = target_sumw/origin_sumw;
    bincontents.back() = tmp_bincontent > truncation && truncation > 0. ? truncation : tmp_bincontent;
    msgsvc.debugmsg(TString::Format("origin_idx %u target_idx %u origin_sumw %.6g target_sumw %.6g bincontents.back() %.6g binedges.back() %.6g",origin_idx, target_idx, origin_sumw, target_sumw, bincontents.back(), binedges.back()));
  }

  TH1D wght_scheme(configtree.get<std::string>("histname","binning_"+cv).data(),"",binedges.size()-1,binedges.data());
  for(auto ibin = 0u; ibin<binedges.size(); ibin++) wght_scheme.SetBinContent(ibin+1,bincontents[ibin]);
  if(msgsvc.GetMsgLvl() >= MSG_LVL::DEBUG) {wght_scheme.Print();}

  // lambda to loop over events in input tree, fill and persist weights.
  auto apply_weights = [&cv, &ow, &wn, normalize_weights, &oloc, &wght_scheme, truncation, &configtree] (std::unique_ptr<TChain> c, const std::string&& out_treename, const std::string&& fileopt) -> void {
    ROOT::RDataFrame d(*c.get());
    ROOT::RDF::RNode df = d;
    if(ow=="one") df = df.Define(ow,"1.");
    // don't filter NaN or inf, but set to 0, otherwise the weights might not be usable as friend trees
    const auto column_names = df.GetColumnNames();
    if(std::find(column_names.begin(), column_names.end(), cv) == column_names.end()){ // cv not in df
      df = df.Define(normalize_weights ? "tmpw" : wn,[truncation](const double owl) -> double {
        if(std::isnan(owl) || std::isinf(owl)) return 0.;
        else return owl > truncation && truncation > 0. ? truncation : owl;
      },{ow});
    }
    else {
      df = df.Define(normalize_weights ? "tmpw" : wn,[&wght_scheme,truncation](const double cvl, const double owl) -> double {
          const auto w = owl*wght_scheme.GetBinContent(wght_scheme.FindBin(cvl));
          if(std::isnan(w) || std::isinf(w)) return 0.;
          else return w > truncation && truncation > 0. ? truncation : w;
        },{cv,ow});
    }
    if(normalize_weights){
      const auto norm = df.Sum<double>(ow).GetValue()/df.Sum<double>("tmpw").GetValue();
      df = df.Define(wn,[norm](const double w) -> double {return w*norm;},{"tmpw"});
      //df = df.Define("tmpwsq",[](const double& w) -> double {return std::pow(w,2);},{"tmpw"});
      //auto norm = df.Sum<double>("tmpw").GetValue()/df.Sum<double>("tmpwsq").GetValue();// triggers event loop
      //df = df.Define(wn,[&norm](const double& w) -> double {return w*norm;},{"tmpw"});
    }
    ROOT::RDF::RSnapshotOptions rso(fileopt, static_cast<ROOT::ECompressionAlgorithm>(configtree.get<unsigned int>("outcompalg",ROOT::RCompressionSetting::EAlgorithm::kInherit)),
                                    configtree.get<unsigned int>("outcomplvl",ROOT::RCompressionSetting::ELevel::kInherit),0,99,false);
    [[maybe_unused]] auto snapshot_rptr = df.Snapshot(out_treename,oloc,{wn},rso);
  };
  // do it - for the regular input
  msgsvc.infomsg("Applying weights from weighting to input sample.");
  apply_weights(IOjuggler::get_chain(rtn, rfn, wd, msgsvc), configtree.get("output_tree",rtn+"weights"), "RECREATE");

  // loop nonoptions to apply weighting scheme for additional inputs
  if(auto eas = options.get_child_optional("extraargs"); (*eas).size() > 0){
    msgsvc.infomsg("Applying weights from weighting to additional samples.");
    for(const auto& ea : *eas){//iterate non-options
      const auto& [efn, etn] = IOjuggler::split_at_last_occurence(ea.second.data());
      apply_weights(IOjuggler::get_chain(etn, efn, wd, msgsvc), boost::algorithm::replace_all_copy(IOjuggler::split_at_last_occurence(efn,"/").back(), ".root", "")+"weights", "UPDATE");
    }
  }
  auto ofile = IOjuggler::get_file(oloc,wd,"UPDATE",msgsvc);
  wght_scheme.Write();
  ofile->Close();
  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO) {clock.Print();}
  return 0;
}

/**
  @date:   2021-01-04
  @author: mstahl
  @brief:  Truth match MC with kinematics/geometry
*/
//STL
#include <cassert>
//boost
#include <boost/format.hpp>
//from ROOT
#include "TLeaf.h"
#include "Math/Vector4D.h"
#include "ROOT/RDataFrame.hxx"
#include "TStopwatch.h"
//local
#include <IOjuggler.h>
#include "ProgressBar.h"

#ifndef NTUPLE_GIZMO_GIT_HASH
#define NTUPLE_GIZMO_GIT_HASH " "
#endif

namespace pt = boost::property_tree;
// Use (void) to silent unused warnings.
#define assertm(exp, msg) assert(((void)msg, exp))
constexpr int max_final_state_particles = 6;
typedef std::array<int, max_final_state_particles> sveci;
typedef std::array<ROOT::Math::XYZTVectorF, max_final_state_particles> sveclv;
std::string sveci_str = "std::array<int, "+std::to_string(max_final_state_particles)+">";
std::string sveclv_str = "std::array<ROOT::Math::XYZTVectorF, "+std::to_string(max_final_state_particles)+">";
// "leightweight" tuple for truth matching.
// will contain EventInSequence (ul), MC_MOTHER_KEY of each particle to match (vect(int)), Lorentz vectors of each particle to match (vect(XYZTVectorF))
typedef std::tuple<unsigned long long, sveci, sveclv> reco_match_tpl;
typedef std::tuple<unsigned long long, sveci, sveclv, sveci, sveci, sveci> truth_match_tpl; // this one also has ID, motherID and grandmotherID

int main(int argc, char** argv) {
  //hardcoded stuff
  //enum class TRUTHMATCH_TYPE{QVAL = 0, DR, FOURVEC};

  TStopwatch clock;
  clock.Start();

  //parse command line variables and put some of them in local variables
  auto options = IOjuggler::parse_options(argc, argv, "c:d:hi:o:t:v:", "nonoptions: track candidate names or colon separated pairs thereof", 0);
  MessageService msgsvc("truth_match", static_cast<MSG_LVL>(options.get<int>("verbosity")));
  msgsvc.debugmsg("Current ntuple-gizmo git hash: " + static_cast<std::string>(NTUPLE_GIZMO_GIT_HASH));
  const auto workdir = options.get<std::string>("workdir");
  const auto output_file_name = options.get<std::string>("outfilename");
  const auto tree_name = options.get<std::string>("treename");

  // first we need a bunch of string containers
  std::vector<std::string> input_trees, input_files;
  // it's more natural to group the objects that get matched in pairs. the suffixes container defines how the Lorentz vectors are called in the input file. this is hardcoded!
  std::vector<std::pair<std::string,std::string>> prefixes;
  // then, we get the inputs
  boost::algorithm::split(input_trees, tree_name, boost::algorithm::is_any_of(";"));
  boost::algorithm::split(input_files, options.get<std::string>("infilename"), boost::algorithm::is_any_of(";"));
  assertm((input_trees.size() == 2u), "\"treename\" has to be of the form 'reco_tree;truth_tree'");
  assertm((input_trees.size() <= 2u), "\"infilename\" has to be of the form 'file1' or 'file1;file2'");
  auto reco_chain = IOjuggler::get_chain(input_trees[0], input_files[0], workdir, msgsvc);
  auto truth_chain = IOjuggler::get_chain(input_trees.back(), input_files.back(), workdir, msgsvc);

  // runtime configuration
  pt::ptree configtree = IOjuggler::get_ptree(options.get<std::string>("config"));
  // append and replace stuff in ptree
  IOjuggler::auto_append_in_ptree(configtree);
  IOjuggler::auto_replace_in_ptree(configtree);
  if(msgsvc.GetMsgLvl() == MSG_LVL::DEBUG) IOjuggler::print_ptree(configtree);

  const auto tolerance = configtree.get<float>("matching_tolerance",1.f);

  // choose between different types of truth matching
  //TRUTHMATCH_TYPE truthmatch_type;
  //std::string truthmatch_var;
  // switch(boost::algorithm::to_lower_copy(configtree.get<std::string>("match_type","q_val"))){
  //   case "delta_r":
  //     truthmatch_type = TRUTHMATCH_TYPE::DR;
  //     break;
  //   case "fourvector":
  //     truthmatch_type = TRUTHMATCH_TYPE::FOURVEC;
  //     break;
  //   default:
  //     truthmatch_type = TRUTHMATCH_TYPE::QVAL;
  //     break;
  // }
  //use KEY
  const bool use_key_var = static_cast<bool>(configtree.get<int>("use_mother_key",0));

  for(const auto& nonopt : *options.get_child_optional("extraargs")) {
    std::vector<std::string> particle_names_to_match;
    boost::algorithm::split(particle_names_to_match, nonopt.second.data(), boost::algorithm::is_any_of(":"));
    if(particle_names_to_match.size() == 2) prefixes.emplace_back(particle_names_to_match[0], particle_names_to_match[1]);
    else if(particle_names_to_match.size() == 1) prefixes.emplace_back(particle_names_to_match[0], particle_names_to_match[0]);
    else msgsvc.errormsg("Problem parsing non-options. Got "+nonopt.second.data());
  }

  // in the "match" node, we expect (key-value) pairs of "trackname_reco_tree" "trackname_truth_tree"
  if(const auto candidate_track_names = configtree.get_child_optional("match"); candidate_track_names && prefixes.size()<1)
    for(const auto& ctn : *candidate_track_names)
      prefixes.emplace_back(ctn.first, ctn.second.data());
  const auto n_particles_to_match = prefixes.size();

  ROOT::RDataFrame d_reco(*reco_chain.get()), d_truth(*truth_chain.get());
  ROOT::RDF::RNode df_reco = d_reco, df_truth = d_truth;
  std::string reco_4vectors, truth_4vectors, reco_mks, truth_mks;
  for(auto const& p : prefixes){
    //add the lorentz vectors to the current dataframe
    df_reco = df_reco.Define(p.first+"_lv",str(boost::format("ROOT::Math::XYZTVectorF(%1%_PX,%1%_PY,%1%_PZ,%1%_PE)") % p.first));
    df_truth = df_truth.Define(p.second+"_lv",str(boost::format("ROOT::Math::XYZTVectorF(%1%%2%X,%1%%2%Y,%1%%2%Z,%1%%2%E)") % p.second % "_TRUEP_"));
    reco_4vectors += p.first+"_lv,";
    truth_4vectors += p.second+"_lv,";
    reco_mks += p.first+"_MC_MOTHER_KEY,";
    truth_mks += p.second+"_MC_MOTHER_KEY,";
  }
  // make a vector of 4-vectors which will later be used to fill our truth matching data structure
  reco_4vectors.pop_back();
  truth_4vectors.pop_back();
  reco_mks.pop_back();
  truth_mks.pop_back();
  msgsvc.debugmsg("This list of 4-vectors is being pushed into the data frame: " + reco_4vectors + "  " + truth_4vectors);
  //we now have branches called <daughtername>_lv in our output tree, which contain the corresponding TLorentzVector
  df_reco = df_reco.Define("reco_vlv",sveclv_str+"{"+reco_4vectors+"};");
  df_truth = df_truth.Define("truth_vlv",sveclv_str+"{"+truth_4vectors+"};");
  df_reco = df_reco.Define("reco_vmk",sveci_str+"{"+reco_mks+"};");
  df_truth = df_truth.Define("truth_vmk",sveci_str+"{"+truth_mks+"};");
  df_truth = df_truth.Define("truth_vids",sveci_str+"{"+boost::algorithm::replace_all_copy(truth_mks, "_MC_MOTHER_KEY", "_ID")+"};");
  df_truth = df_truth.Define("truth_vmids",sveci_str+"{"+boost::algorithm::replace_all_copy(truth_mks, "_MC_MOTHER_KEY", "_MC_MOTHER_ID")+"};");
  df_truth = df_truth.Define("truth_vgmids",sveci_str+"{"+boost::algorithm::replace_all_copy(truth_mks, "_MC_MOTHER_KEY", "_MC_GD_MOTHER_ID")+"};");

  // init truth matching data structure
  const auto nev_reco = reco_chain->GetEntries(), nev_truth = truth_chain->GetEntries();
  std::vector<reco_match_tpl> reco_tpl;
  std::vector<truth_match_tpl> truth_tpl;
  reco_tpl.reserve(nev_reco);
  truth_tpl.reserve(nev_truth);

  // fill truth matching data structure
  ProgressBar pg;
  pg.start(nev_reco, msgsvc);
  msgsvc.infomsg("Filling tuple containing variables for truth matching (reco_tpl) from reconstructed tree.");
  df_reco.Foreach([&reco_tpl, &pg](const unsigned long long& eis, const sveclv& vlv, const sveci& mks, const unsigned long long& ie){
    pg.update(ie);
    reco_tpl.emplace_back(eis, mks, vlv);
    }, {"EventInSequence", "reco_vlv", "reco_vmk", "rdfentry_"});
  pg.stop();
  pg.start(nev_truth, msgsvc);
  msgsvc.infomsg("Filling tuple containing variables for truth matching (truth_tpl) from MC truth tree.");
  df_truth.Foreach([&truth_tpl, &pg](const unsigned long long& eis, const sveclv& vlv, const sveci& mks,
                                     const sveci& ids, const sveci& mids, const sveci& gmids, const unsigned long long& ie){
    pg.update(ie);
    truth_tpl.emplace_back(eis, mks, vlv, ids, mids, gmids);
    }, {"EventInSequence", "truth_vlv", "truth_vmk", "truth_vids", "truth_vmids", "truth_vgmids", "rdfentry_"});
  pg.stop();

  //prepare output
  const auto oloc = workdir.empty() ? output_file_name : workdir+"/"+output_file_name;
  IOjuggler::dir_exists(oloc,msgsvc);
  msgsvc.infomsg("Writing friend tree to "+oloc);
  IOjuggler::dir_exists(oloc);
  TFile ff(oloc.data(),"RECREATE");
  const auto out_tree_name = configtree.get<std::string>("ouput_tree_name","t");
  TTree matched_tree(out_tree_name.data(),out_tree_name.data());
  std::vector<int> is_matched(n_particles_to_match,0), id(n_particles_to_match,0), mother_id(n_particles_to_match,0), grandmother_id(n_particles_to_match,0);
  std::vector<float> output_data(n_particles_to_match,0.0);
  for(unsigned int i = 0u; i < n_particles_to_match; i++){
    matched_tree.Branch((prefixes[i].first+"_matched").data(),&is_matched[i],(prefixes[i].first+"_matched/O").data());
    matched_tree.Branch((prefixes[i].first+"_truthID").data(),&id[i],(prefixes[i].first+"_truthID/I").data());
    matched_tree.Branch((prefixes[i].first+"_truthMID").data(),&mother_id[i],(prefixes[i].first+"_truthMID/I").data());
    matched_tree.Branch((prefixes[i].first+"_truthGMID").data(),&grandmother_id[i],(prefixes[i].first+"_truthGMID/I").data());
  }

  if(static_cast<bool>(configtree.get<int>("store_matching_var",0)))
    /// here we probably need a different data structure... let's code up everything for qvalue matching and take it from there
    for(unsigned int i = 0u; i < n_particles_to_match; i++)
      matched_tree.Branch((prefixes[i].first+"_q_value").data(),&output_data[i],(prefixes[i].first+"_q_value/F").data());

  // sort by EventsInSequence
  std::sort(truth_tpl.begin(), truth_tpl.end(), [](const truth_match_tpl& a, const truth_match_tpl& b) { return std::get<0>(a) < std::get<0>(b); });

  msgsvc.infomsg("Starting truth matching procedure with "+std::to_string(nev_reco)+" reconstructed and "+std::to_string(nev_truth)+" MC truth events");
  pg.start(nev_reco,msgsvc);
  for(unsigned long rev = 0ul; rev < nev_reco; rev++){
    pg.update(rev);
    const auto reco_event_in_sequence = std::get<0>(reco_tpl[rev]);
    if(rev<20) msgsvc.debugmsg("Event " + std::to_string(rev) + " reco_event_in_sequence " + std::to_string(reco_event_in_sequence));
    auto truth_iter = std::find_if(truth_tpl.begin(), truth_tpl.end(), [&reco_event_in_sequence](const auto& a){return std::get<0>(a) == reco_event_in_sequence;});
    bool already_found = false;
    while(std::get<0>(*truth_iter) == reco_event_in_sequence && truth_iter != truth_tpl.end()){
      if(use_key_var){
        bool all_mothers_matched = true;
        for(unsigned int i = 0u; i < n_particles_to_match; i++)
          all_mothers_matched &= std::get<1>(reco_tpl[rev])[i] == std::get<1>(*truth_iter)[i];
        if(!all_mothers_matched){++truth_iter; continue;} // if the mother keys could not be matched: continue!
      }
      // the actual truth-matching condition
      bool is_fully_matched = true, is_partly_matched = false;
      for(unsigned i = 0u; i < n_particles_to_match; i++){
        output_data[i] = (std::get<2>(reco_tpl[rev])[i]+std::get<2>(*truth_iter)[i]).M() - std::get<2>(reco_tpl[rev])[i].M() - std::get<2>(*truth_iter)[i].M();
        abs(output_data[i]) < tolerance ? is_matched[i] = 1 : is_matched[i] = 0;
        is_fully_matched &= is_matched[i];
        is_partly_matched |= is_matched[i];
      }
      if(already_found && is_partly_matched)
        msgsvc.infomsg(TString::Format("This is some sort of multiple candidate. Please check EventInSequence Reco %u Truth %u is_fully_matched %d",
                                       reco_event_in_sequence,std::get<0>(*truth_iter),is_fully_matched));
      if(is_partly_matched){
        already_found = true;
        for(unsigned i = 0u; i < n_particles_to_match; i++){
          id[i] = std::get<3>(*truth_iter)[i];
          mother_id[i] = std::get<4>(*truth_iter)[i];
          grandmother_id[i] = std::get<5>(*truth_iter)[i];
        }
      }
      ++truth_iter;
    }// end loop truth tree
    matched_tree.Fill();
  }// end loop reco tree

  // that's it. write, close, print time, go home and drink a beer.
  pg.stop();
  matched_tree.Write();
  ff.Close();
  clock.Stop();
  if(msgsvc.GetMsgLvl() >= MSG_LVL::INFO)
    clock.Print();
  return 0;
}

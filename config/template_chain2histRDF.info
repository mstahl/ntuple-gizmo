;#################################################################
;# Template config file for chain2hist                           #
;# Date: 2018-11-28                                              #
;# Author: Marian Stahl                                          #
;# Description: The template should give an idea of how to write #
;#              config files for chain2hist and make use of its  #
;#              full functionality                               #
;# Execution: draw_stuff -c <config_file> -i <input-file>        #
;#            -t <input-tree> -d <workdir> -o <output-file>      #
;#            -p <prefix> -v <verbosity-level>                   #
;#################################################################

APPEND { ;;the append node is called first
  Filter  " && year == {prefix}" ;;parse -p option from the command line ({prefix} gets replaced by that) and append it to the filter action
}

REPLACE { ;;this node is ignored when actions in APPEND are executed
  _stdcuts_  "my_cut_var > 0." ;; now replace _stdcuts_ by the actual cut
  _Kmass_    493.677 ;MeV
}

Filter _stdcuts_ ;; this now reads "my_cut_var > 0. && year == <whatever was passed to -p flag>"

threads 24

Define { ;; make some funny new variables
  K_dProbNN     "K_ProbNNk - K_ProbNNp - K_ProbNNpi"
  p_dProbNN     "p_ProbNNp - p_ProbNNpi - p_ProbNNk"
  Lc_love       "TLorentzVector(Lc_PX,Lc_PY,Lc_PZ,Lc_PE)"
  pi_as_K_love  "TLorentzVector(pi_PX,pi_PY,pi_PZ,sqrt(pi_PX*pi_PX+pi_PY*pi_PY+pi_PZ*pi_PZ+_Kmass_*_Kmass_))"
  LcK_M         "(Lc_love+pi_as_K_love).M()" ;; be creative!
}

hists { ;this is mandatory!
  LcK { ;this will be the name of the output histo
    Draw "LcK_M";standard histogram options
  }
  dummy {; the name is parsed from Draw
    Draw "LcK_M>>LcK_weighted(100,5200,5600)"
    ;cut  "something_else < 1."
    weight K_dProbNN
  }
  dummy {
    var    p_dProbNN
    yvar   p_dProbNN
    nbins  100
    nbinsy 100
    min    -0.01
    max    0.01
    ymin   -0.01
    ymax   0.01
    histname my_funny_2D_hist
  }
}

# Documentation for elwms

Script for making trimmed samples, workspaces or histograms and applying TMVA weights using `RDataFrame` with property trees.

***Requirements*** A C++17 compiler, ROOT > 6.17/01. Make sure to `export OPENBLAS_MAIN_FREE=1` to use implicit multithreading.

## Command line options
The options which can be given to the executable are
- `-c` : location of the config file (automatic replacing and appending enabled)
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with `@<filename>` (similar to hadd functionality))
- `-j` : search-replace expressions for the config file (<search_for0:replace_by0;search_for1:replace_by1;...>)
- `-m` : combination(s) of `<weightfile (xml format):BDT response name>`. More than one of these combinations can be given, separated by a semicolon.
- `-o` : output file (since there can be more than 2 output files, the trimmed tree is put into `<working directory>/forWs/<outputfilename>`, a file containing a RooWorkspace is put in `<working directory>/forWs/<outputfilename>` and histograms are saved in `<working directory>/hists/<outputfilename>`)
- `-r` : name of output `RooDataSet` (only if ws_vars are given in config file)
- `-t` : input `TTree` name
- `-v` : verbosity (1-3, optional)
- `-w` : name of output `RooWorkspace` (only if ws_vars are given in config file)
- `-h` : help
- nonoptions are optional `friendfile(s):friendtree` combinations and/or `<variable->transformation>` combinations. Note that you can also add friend-chains, where the syntax for `friendfile(s)` is the same as for the `-i` option

### Hints for running with multiple input files
- Don't forget the ticks (`''`) when using the semicolon separated list.
- Don't mix input files that should be prepended with `workdir` with files at absolute paths. Give all of them with absolute paths instead.

## The config file
Check out the [template](config/template_elwms.info)!<br>
The following can be given in a config file to elwms:

- threads : ( `unsigned int`, default: all available cores ) Number of threads used by ROOT's implicit multithreading. If this is 0, multithreading is disabled.
- variables: ( `property_tree node`, optional ) A simple list of variables which are persisted in the output tree (none by default).
- AddBranchMode: (`bool`,default false) Persist/Keep all variables in the input TTree/TChain/RDataFrame.
- Define and DefineInternal : ( `property_tree node`, optional ) [`Define`](https://root.cern.ch/doc/master/classROOT_1_1RDF_1_1RInterface.html#aefd0d480704c0c39c599c28666c713d2)
                                                                  action for transforming variables. Define runs after DefineInternal.
```
; RDataFrame Define actions, i.e. variable transformations
; Variables in DefineInternal are not persisted with the output snapshot.
DefineInternal {
  ; add a TVector3 to the data-frame
  awesome_new_variable "TVector3(variable3,my_cool_new_variable,variable1)"
}
Define {
  ; simply rename variable2
  my_cool_new_variable variable2
  ; use the internal variable defined above
  awesome_new_variable_Mag "awesome_new_variable.Mag()"
  ; internal variables can also be created in this node using `__internal__` in the name
  __internal__one_over_pi "0.31830989"
  ; Here, we use the Lorentz-vectors that are automatically created by the misIDMbeta node below
  mprime_xic "__internal__one_over_pi*acos(2.*((Xc_pi_lv+Xc_K_lv).M()-633.24739)/896.35261-1.)" ; 633.24739 = mpi+mK, 896.35261 = mXic-mp-mK-mpi
  ; More complicated transformations over a few lines are possible as well:
  thetaprime_xic "auto lvRes=Xc_K_lv+Xc_pi_lv; auto tmp_1 = Xc_pi_lv; tmp_1.Boost(-lvRes.BoostVector()); auto tmp_2 = p_lv; tmp_2.Boost(-lvRes.BoostVector()); return 0.31830989*tmp_2.Angle(tmp_1.Vect());"
}
```
- Filter : ( `string`, optional ) Filter action called after all transformations (so that cuts on transformed variables can be used)
- TraceableFilter : ( `property_tree node`, optional ) Filter actions that will trigger a dedicated cut report which will be written to disk as `json`.

                 TraceableFilter {
                   name_of_this_cut "PT>1000 && ETA<5"
                   name_of_anoter cut "..."
                   ...
                 }

- remove_nan_inf: (`bool`,default false) rejects events where at least one value of the variables defined in the `variables` node (which is going to be saved in the output tree) is NaN or inf.
- create_indexlist: (`bool`,default false) Use `_rdfentry` for giving unique indices to entries.
- Range_end, Range_begin, Range_stride : ( `unsigned int`, optional ) filters each `Range_stride` (default 1) entry in range: `[Range_begin` (default 0), `Range_end)`. Applied on top of Filter action. Details in [ROOT doc](https://root.cern.ch/doc/master/classROOT_1_1RDF_1_1RInterface.html#a1b36b7868831de2375e061bb06cfc225).
- misIDMbeta: Triggers calculation of subcombinations of invariant masses and single particle momentum asymmetries. It is a node containing the names of final state particles. It will append their names by `_PX,_PY,_PZ,_PE` to build the LorentzVectors, so make sure they are on the input tuple.
- add_lvs_to_output: (`bool`,default false) persists TLorentzVectors that were used to calculate beta and M (only works if misIDMbeta is given)
- outtreename: Name of the tree in the output file (it's the inputname by default)
- outfopt: pass option string to ctor of TFile for the output-file (https://root.cern.ch/doc/master/classTFile.html#aadd8e58e4d010c80b728bc909ac86760). default is "recreate"
- outcompalg: [compression algorithm](https://root.cern.ch/doc/master/namespaceROOT.html#a60945f6fe634f9f3be9872e57bf87f2e) for the output file (default is inherited)
- outcomplvl: [compression level](https://root.cern.ch/doc/master/namespaceROOT.html#a0b09a8c4888c9bb4388950ce7d5041b6) for the output file (default is inherited)
- OutputPrefix (`string`, optional) Change output file. Sandwiched between workdir and outfilename (default: "/forWs/")
- HistOutputPrefix (`string`, optional) Change output file for histogram drawing. Sandwiched between workdir and outfilename (default: "/hists/")
- LumiTuple: takes an integer bool value dictating whether to save another root file (by default in `<workdir>/forWs/Lumi_<outfilename>`) containing a tree with the lumi information
- LumiTupleTree: (`string`, optional) Location of Lumi tree in input file. Default is `GetIntegratedLuminosity/LumiTuple`. Only active if LumiTuple is True
- LumiOutputPrefix (`string`, optional) Change output file name of lumi tuple. Sandwiched between workdir and outfilename (default: "/forWs/Lumi_")
- ws_vars:  (`property_tree node`) Similar to `variables` node described above. This node triggers creation of a `RooDataSet` in a `RooWorkspace`. <br>
                                   Their name is given as object identifier, optional parameters are `low`, `high` to specify the range. The default is `-inf` to `inf` <br>
                                   No transformations are possible here, but previously internal variables can still be added. <br>
                                   If this node is added, more options become available:
    - WsOutputPrefix (`string`, optional) Change output file name of the workspace. Sandwiched between workdir and outfilename (default: "/Ws/")
    - kdepdfs  : Pdfs added to workspace that use the `RooKeysPdf` class. <br>
      The name of the iterable object (lets call it _itername_) will be the name of the `RooKeysPdf`. <br>
      If the resolution node is given, the name will be appended with an additional `xg`. <br>
      When setting up the model with factory code use `RooKeysPdf::itername` or `RooFFTConvPdf::iternamexg`

        - filename   : ( `string`            , no default                ) Location of filename with shape for `RooKeysPdf` (absolute or relative to path given with `-d`)
        - treepath   : ( `string`            , no default                ) Location of tree within given file
        - obsinfile  : ( `string`            , default `observable.name` ) Name of branch/leaf where the shape will be extracted from. Defaults to the name of the observable specified above
        - observable : ( `string`            , optional                  ) Name of the observable that the `RooKeysPdf` will be created in (By default this is the `RooRealVar` created by `observable.name`) <br>
          **ATTENTION** It is highly recommended to set a range for the observable in the `variables` or `variables_override` node
        - mirror     : ( `RooKeysPdf::Mirror`, default `3 (MirrorBoth)`  ) Mirror-option in `RooKeysPdf` ctor. Chose an integer number between 0 and 8 for the following options <br>
          `enum Mirror { NoMirror = 0, MirrorLeft, MirrorRight, MirrorBoth, MirrorAsymLeft, MirrorAsymLeftRight, MirrorAsymRight, MirrorLeftAsymRight, MirrorAsymBoth };`
        - rho        : ( `double`            , default `1.0`             ) Should only be adjusted in extreme situations (very strong local peaks, see [arXiv:hep-ex/0011057](https://arxiv.org/abs/hep-ex/0011057 "arXiv:hep-ex/0011057"))
        - fold       : ( `bool`              , default `1 (true)`        ) Set folding for every pdf individually

                 kdepdfs {
                   LcDstgK { ;1st item in kdepdfs-list
                     filename   "LcD0gK_mc.root"
                     treepath   "smalltree"
                     obsinfile  "Lb_Cons_M"
                     fold       0
                   }
                   LcDstpi0K { ;2nd item in kdepdfs-list
                     filename   "root://eoslhcb.cern.ch//eos/lhcb/user/m/mstahl/Lb2LcD0K/XFeedMC/15196400.root"
                     treepath   "MCDecayTreeTuple/MCDecayTree"
                     obsinfile  "Lb_LcD0K_M"
                   }
                   Lc_from_Lb2LcDstgK { ;KDE in different observable (KDE defined in `mLb` and read from `Lc_M`)
                     filename   "LcD0gK_mc.root"
                     treepath   "smalltree"
                     obsinfile  "Lc_M"
                     observable "mLc"
                     fold       0
                   }
                 }

- hists:  (`property_tree node`) See note on histograms below.

### Saving histograms
The elwms script includes the functionality of chain2histRDF. For writing config files, please have a look at the [chain2histRDF doc](doc/chain2histRDF.md), the [stuff2hist doc](doc/stuff2hist.md) and the [template](config/template_chain2histRDF.info) for chain2hist.

### Hints for the -m option
Input variables to the TMVA::Reader are parsed from the xml file, and are expected to have the same name as during training.
The variables can be added locally to the RDataFrame object, and don't need to be persisted. They can thus be added to the `DefineInternal` node.
# Documentation for multiple_candidates.cpp

For a given input tree or chain, the script writes a friend tree with branches for the candidates multiplicity,
as well as maxima and minima withtin a set of multiple candidates of an optional rejection criterion.
The output branches will be called `multiplicity`, `min_crit` and `max_crit`.
A histogram of their multiplicities is added as well.<br>
The logic behind the rejection criterion is taylored to flat n-tuples. It can be any transformation of variables that live in the input tree/chain.
During the search for multiple candidates, the minimum and maximum of that new rejection variable are calculated among the multiple candidates.
Consider the following example: When selecting Lb -> Lc Ds decays, we want to a keep multiple candidate if it's combined BDT response is clearly superior
in comparison with to that of the other candidate. We then call `multiple_candidates ... -j 'Lc_BDT+Ds_BDT'` to fill `min_crit` and `max_crit`.
Later, we can use the following filter to reject bad multiple candidates: `multiplicity>1 && !(abs(Lc_BDT+Ds_BDT-max_crit)<1.e-4 && 2*(min_crit+1)<max_crit)`.<br>
The following (LHCb-specific) branches need to exist in the input tuple: `runNumber` and `eventNumber` as unsigned int and unsigned long long respectively.
As event numbers are not unique in LHCb, both information is needed to make a "unique event number" (which is `10e+9*runNumber+eventNumber`).
Multiple candidates are thus defined as entries in the input tree/chain which have the same unique event number<br>
It's runtime options are parsed entirely from the command line. I.e. no config file needed

## Command line options
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with @<filename> (similar to hadd functionality))
- `-j` : rejection criterion (optional)
- `-o` : output file
- `-t` : input `TTree` name
- `-w` : output `TTree` name (optional, default: input tree name + `Mult`)
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions are either branchnames which should be written to the output file (separated by `;`) or `friendfile(s):friendtree` combinations. More than 1 file:tree combination can be used (see syntax from `-i` option).
If no branchnames are specified, all are persisted.
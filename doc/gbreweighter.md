# Documentation for gbreweighter

Python script for weighting a sample `r` to a target-sample `t` with [GBReweighter](http://arogozhnikov.github.io/2015/10/09/gradient-boosted-reweighter.html) from [hep_ml](https://arogozhnikov.github.io/hep_ml/index.html). The script takes input samples and trees from the command line and outputs a root tree with a single weight branch (i.e. if the weight is applied to `r`, the variable-distributions should resemble those in `t`).

The gbreweighter trained from weighting `r` to `t` can be applied to further samples passed to the `-a` flag.
The ouput weights of applying the weighter to those samples are stored in separate trees in the output file (`-o`).
Those treenames will be parsed from the filenames passed to `-a` (stripping path and suffix).

## Command line options
Run `python gbreweighter.py -h` to see:
```
usage: gbreweighter.py [-h] -r SAMPLE_TO_WEIGHT [SAMPLE_TO_WEIGHT ...] -t TARGET_SAMPLE [TARGET_SAMPLE ...] -o OUTPUT_SAMPLE -c CONFIG [-a MORE_SAMPLES [MORE_SAMPLES ...]] [-f FILTER]

Weight a sample "r" to a target-sample "t" with GBReweighter from hep_ml.
⚠ ☠  ATTENTION  ☠ ⚠ Use negative weights such as "sWeights" at your own risk!

optional arguments:
  -h, --help            show this help message and exit
  -r SAMPLE_TO_WEIGHT [SAMPLE_TO_WEIGHT ...], --sample_to_weight SAMPLE_TO_WEIGHT [SAMPLE_TO_WEIGHT ...]
                        e.g. /path/to/the/sample_to_weight.root:DecayTree
  -t TARGET_SAMPLE [TARGET_SAMPLE ...], --target_sample TARGET_SAMPLE [TARGET_SAMPLE ...]
                        e.g. /path/to/the/target_sample.root:dir/DecayTree
  -o OUTPUT_SAMPLE, --output_sample OUTPUT_SAMPLE
                        e.g. /path/to/the/output_sample.root, it will contain weights only
  -c CONFIG, --config CONFIG
                        e.g. /path/to/the/config_file.json
  -a MORE_SAMPLES [MORE_SAMPLES ...], --additional_samples MORE_SAMPLES [MORE_SAMPLES ...]
                        e.g. /path/to/the/another_sample_to_apply_the_weighting_to.root:t
  -f FILTER, --filter FILTER
                        Filter string for sample_to_weight. The filter is applied for training, the weights will be calculated for the whole sample
```

## The config file
The script reads in a `json` config file. The configurables are not nested.

| name                  | type  | description                                                    | default                   |
-------------           |------ |------------                                                    |----------
| `branches`            | list  | variables used in the weighting                                | `['*']` (not recommended) |
| `n_estimators`        | int   | number of trees                                                | 40                        |
| `learning_rate`       | float | training parameter                                             | 0.2                       |
| `max_depth`           | int   | number of tree splitting levels                                | 3                         |
| `min_samples_leaf`    | int   | number of entries in terminal nodes                            | 200                       |
| `loss_regularization` | float | regularization parameter used in loss calculation              | 5.                        |
| `gb_args`             | dict  | see [here](https://arogozhnikov.github.io/hep_ml/gb.html#hep_ml.gradientboosting.UGradientBoostingClassifier) for parameters | None |
| `folding`             | bool  | use k-folding                                                  | False                     |
| `n_folds`             | int   | number of folds                                                | 2                         |
| `random_state`        | int+? | seed for reproducibility or `RandomState`(?)                   | None                      |
| `verbose`             | bool  | more printout in k-folding                                     | False                     |
| `original_weight`     | str   | name of weight branch in the sample to weight                  | None                      |
| `target_weight`       | str   | name of weight branch in the target sample                     | None                      |
| `output_branch_name`  | str   | name of output weight branch                                   | `gb_weight`               |
| `output_tree`         | str   | name of output tree                                            | weight_tree+`_weights`  |
| `truncate_weights`    | list  | truncate weights in limits given by this 2-element list        | None                      |
| `normalize_weights`   | bool  | normalize weight by sum of weights over sum of squared weights | False                     |
| `branches_for_filter` | list  | additional branches for filter action on sample to weight      | []                        |
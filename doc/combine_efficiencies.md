# Documentation for combine_efficiencies
Python script for calculating and combining efficiencies that are stored in form of weights in several trees in a single input file.
The combination uses integrated luminosities reported in the bookkeeping.

The script makes several assumptions:
- Trees for efficiency-numerator and -denominator are stored in the same input file.
- These trees all contain the same weight-branch
- Treenames contain a unique year-polarity substring of the form `yyp` (15U, 17D etc...)
- Treenames contain a substring that labels numerator (`reco_tree_name_pattern`) and denominator (`truth_tree_name_pattern`)
This means that an input file with trees `n15D, d15D, n15U, ...` containing weights `w` can be used like `python combine_efficiencies.py -i <input> -o <output> -r n -t d -w w`.

## Command line options
Run `python combine_efficiencies.py -h` to see:
```
usage: combine_efficiencies.py [-h] -i INPUT [INPUT ...] -o OUTPUT [-r RECO_TREE_NAME_PATTERN] [-t TRUTH_TREE_NAME_PATTERN] [-w WEIGHT_NAME [WEIGHT_NAME ...]] [--truncate_weights TRUNCATION_LIMITS TRUNCATION_LIMITS] [-c {pp,Pbp,pPb}]

Combine efficiencies from file containing numerator and denominator trees with weights split by year and polarity. ATTENTION: The script assumes that the ENTIRE DATAset for a given year and polarity has been processed (no failed
ganga/DIRAC jobs!)

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT [INPUT ...], --input INPUT [INPUT ...]
                        e.g. /path/to/input.root. Should contain trees with weights to combine.
  -o OUTPUT, --output OUTPUT
                        e.g. /path/to/output.json, will contain pairs of "efficiency" and "uncertainty".
  -r RECO_TREE_NAME_PATTERN, --reco_tree_name_pattern RECO_TREE_NAME_PATTERN
                        unique string to match numerator trees.
  -t TRUTH_TREE_NAME_PATTERN, --truth_tree_name_pattern TRUTH_TREE_NAME_PATTERN
                        unique string to match denominator trees.
  -w WEIGHT_NAME [WEIGHT_NAME ...], --weight_name WEIGHT_NAME [WEIGHT_NAME ...]
                        Name of the weight-branch.
  --truncate_weights TRUNCATION_LIMITS TRUNCATION_LIMITS
                        Truncate normalized weights before combination. Re-normalize after truncation.
  -c {pp,Pbp,pPb}, --collision_mode {pp,Pbp,pPb}
                        Mode of collision to read from lumi table.
```
Defaults are `-r reco -t truth -w gb_weight -c pp`.
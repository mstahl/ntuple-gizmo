# Documentation for truth_match.cpp
For generated (truth) and reconstructed (reco) input data, the script matches individual tracks according to their q-value.
The q-value is calculated as invariant mass of truth and reco tracks minus their own mass hypotheses. For parallel tracks, the q-value is close to 0.

## Input
The script constructs 4-vectors from `<candidate_name>_{PX,PY,PZ,PE}` and `<candidate_name>_TRUE{PX,PY,PZ,PE}` variables and expects
them to be present in both reco and truth trees respectively (`<candidate_name>` is configurable seperately for reco and truth tree). <br>
Since MC productions (to my knowledge) don't have unique event numbers, we use `EventInSequence` which is aligned in reco and truth trees that have been produced together.
***ATTENTION*** : try to avoid adding ntuples before running truth-matching on them. Otherwise the same `EventInSequence` values will occur multiple times.
The script can deal with that in principle, but to be on the safe side, it should be avioded, especially for larger productions.<br>
If optionally the mother key is used for matching in addition to `EventInSequence`, the scripts expects `<candidate_name>_MC_MOTHER_KEY` in the input trees.

## Command line options
- `-c` : location of the config file (automatic replacing and appending enabled)
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with @<filename> (similar to hadd functionality)). One or two input files are expected
- `-o` : output file
- `-t` : two input `TTree` names separated by semicolon
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions: optional list of track candidate names (if they are called the same in reco and truth tree) or colon separated pairs thereof

## Config file
All optional (but something has to be passed to `-c`). See [example_truth_match](config/example_truth_match.info) file for an example of all configurables.

## Output
The output of this script will be a friend tree of the reco tree. The tree will contain the following variables:
- `<candidate_name>_matched`   (bool, true if the track candidate was successfully matched)
- `<candidate_name>_truthID`   (int, ID of the truth cadidate with the same `EventInSequence`)
- `<candidate_name>_truthMID`  (int, mother ID of the truth cadidate with the same `EventInSequence`)
- `<candidate_name>_truthGMID` (int, ID of the truth cadidate with the same `EventInSequence`)
- `<candidate_name>_q_value`   (float, optional -- the truth matching criterion)

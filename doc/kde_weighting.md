# Documentation for kde_weighting.cpp
The script weights an "origin" tree to a "target" tree using Meerkat's kernel density estimators.
It can then apply the (unbinned) weighting scheme deduced from the "origin" and "target" KDEs to further samples on an event by event basis.
The output is a single file with one (or several) tree(s), each with a branch of weights that can be used as friend tree to the respective input tree.

## Details on obtaining KDEs
The strategy to arrive at adaptive KDEs for the full phase spaces is in a sense hardcoded and follows the logic of [PIDGen/PIDCorr](https://gitlab.cern.ch/lhcb/Urania/-/blob/abec39e38fe01cee90605e81a5132af0d9135fb2/PIDCalib/PIDPerfScripts/python/PIDGenExpert/Run2/PIDPdf.py).
This means that 1D binned KDEs are estimated in each dimension, then an adaptive KDE is build using the binned one as PDF for width scaling.
In more than 1 dimension, a factorised KDE is made from the adaptive KDE of the previous step and the 1D binned KDE.
This factorised KDE is then used as approximation when making higher dimensional binned and adaptive KDEs.

The final output are thus (normalised) adaptive KDEs for origin and target samples, whose densities at a certain point in phase space can be divided to get a weight.

Binning and initial widths are adapted to the dimensionality, reducing the exponent to the base of ten by one unit for each additional dimension.
This means that the 1D KDEs have the binning passed to `variables.<var>.nbins_kde`, and the number of bins and the width is scaled down for higher dimensions to avoid huge maps in Meerkat.

***ATTENTION*** All input samples to the weighting need to be free of NaNs and infinities!

## Command line options
- `-c` : location of the config file (automatic replacing and appending enabled)
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : origin file + tree name combination (single file appended with `:<treename>`). Only one input file!
- `-j` : search-replace expressions for the config file (<search_for0:replace_by0;search_for1:replace_by1;...>)
- `-o` : output file
- `-t` : file-tree combination `<file:tree>` for the target sample
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions: list of files and trees to apply the weighting scheme to; zipped like `"<file:tree>"`

## Config file
| name                        | type            | description                                                                                                                        | default                 |
-------------                 |------           |------------                                                                                                                        |----------
| `variables`                 | property tree   | variables for weighting                                                                                                          | - (mandatory)           |
| `variables.<var>.low`       | double          | lower phase space limit                                                                                                            | - (mandatory)           |
| `variables.<var>.high`      | double          | upper phase space limit                                                                                                            | - (mandatory)           |
| `variables.<var>.nbins_kde` | unsigned int    | number of bins (1D)                                                                                                                | 1000                    |
| `ntoys`                     | unsigned int    | number of toy events for MC convolution of the approximation PDF. Use binned convolution if toyEvents=0                            | 2000000                 |
| `origin_weight`             | optional string | name of a weight branch in the sample to weight                                                                                    | optional                |
| `target_weight`             | optional string | name of a weight branch in the sample to weight to                                                                                 | optional                |
| `output_branch_name`        | string          | name of output weight branch                                                                                                       | `kde_weight`            |
| `output_tree`               | string          | name of output tree                                                                                                                | weight_tree+`weights` |
| `outcompalg`                | int             | [compression algorithm](https://root.cern.ch/doc/master/namespaceROOT.html#a60945f6fe634f9f3be9872e57bf87f2e) for the output file  | inherited from input    |
| `outcomplvl`                | int             | [compression level](https://root.cern.ch/doc/master/namespaceROOT.html#a0b09a8c4888c9bb4388950ce7d5041b6) for the output file      | inherited from input    |

## Snakemake wrapper
The wrapper can take several inputs from the workflow itself or from outside.
These are "sample", "target_file", "origin_file", "additional_sample".
This is to have hooks for input files that are part of the workflow and those that aren't.

The wrapper needs:
- `input.origin_file` or `params.origin_file` (string): file with the origin tree to deduce the weighting scheme
- `input.target_file` or `params.target_file` (string): file with the target tree to deduce the weighting scheme
- `params.target_tree` (string): name of the target tree (to weight to for deducing the weighting scheme)
- `params.origin_tree` (string): name of the origin tree (the one that's weighted for deducing the weighting scheme)
- `params.config` (string): config file, see above

Optional arguments:
- `input.additional_sample` or `params.additional_sample` (list or str): files with data to apply the weighting to
- `params.work_dir` (str): working directory, default "$RF"
- `params.tee` (str): for logging, default ">"
- `params.verbosity` (castable to str)
- `params.additional_tree_name` (str) name of trees in "additional_sample" (they need to be the same in each file)

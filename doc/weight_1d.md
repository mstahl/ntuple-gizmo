# Documentation for weight_1d.cpp
The script weights two unbinned datasets ("origin" to "target") using an adaptive binning.
It then applies the weighting scheme to an unbinned sample in a tree (the sample selection needs to match with the "origin" tree)
and can take additional inputs.
The output is a single file with one or several tree(s), each with a branch of weights that can be used as friend tree to the respective input tree.

## Command line options
- `-c` : location of the config file (automatic replacing and appending enabled)
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : file-tree combination `<file:tree>` for the origin tree
- `-t` : file-tree combination `<file:tree>` for the target tree
- `-j` : search-replace expressions for the config file (<search_for0:replace_by0;search_for1:replace_by1;...>)
- `-n` : minimal number of events per bin to define adaptive binning for weighting (balanced between origin and target)
- `-o` : output file
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions: list of files and trees to apply the weighting scheme to; zipped like `"<file:tree>"`

## Config file
| name                 | type   | description                                                                                                                       | default                    |
-------------          |------  |------------                                                                                                                       |----------
| `calib_var`          | string | calibration variable used in the weighting (as defined by origin tree)                                                            | - (mandatory)              |
| `origin_weight`      | string | name of weight branch in the sample to weight                                                                                     | optional                   |
| `target_weight`      | string | name of weight branch in the sample to weight                                                                                     | optional                   |
| `output_branch_name` | string | name of output weight branch                                                                                                      | `weight_1d`                |
| `output_tree`        | string | name of output tree                                                                                                               | weight_tree+`weights`    |
| `truncation_factor`  | double | truncate weights when weighting                                                                                                   | 25                         |
| `normalize_weights`  | bool   | normalize applied weights to the sum of weights in the input tree (origin or additional)                                          | `false`          |
| `outcompalg`         | int    | [compression algorithm](https://root.cern.ch/doc/master/namespaceROOT.html#a60945f6fe634f9f3be9872e57bf87f2e) for the output file | inherited from input |
| `outcomplvl`         | int    | [compression level](https://root.cern.ch/doc/master/namespaceROOT.html#a0b09a8c4888c9bb4388950ce7d5041b6) for the output file     | inherited from input |

## Snakemake wrapper
The wrapper can take several inputs from the workflow itself or from outside.
These are "sample", "target_file", "origin_file", "additional_sample".
This is to have hooks for input files that are part of the workflow and those that aren't.

The wrapper needs:
- `input.origin_file` or `params.origin_file` (string): file with the origin tree to deduce the weighting scheme
- `input.target_file` or `params.target_file` (string): file with the target tree to deduce the weighting scheme
- `params.config` (string): config file, see above

Optional arguments:
- `input.additional_sample` or `params.additional_sample` (list or str): files with data to apply the weighting to
- `params.target_tree` (string): name of the target tree (to weight to for deducing the weighting scheme), default "t"
- `params.origin_tree` (string): name of the origin tree (the one that's weighted for deducing the weighting scheme), default "t"
- `params.work_dir` (str): working directory, default "$RF"
- `params.tee` (str): for logging, default ">"
- `params.verbosity` (castable to str)
- `params.min_events_per_bin` (int) to define the binning, default 42
- `params.additional_tree_name` (str) name of trees in "additional_sample" (they need to be the same in each file)

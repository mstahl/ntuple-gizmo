# chain2histRDF
`chain2histRDF` is a small script to save histograms from a `TTree` or `TChain` in an output root file.<br>
Prefer `chain2histRDF` over `stuff2hist`, because it's just much faster due to jitting and multithreading<br>
The script uses [`RDataFrame`](https://root.cern.ch/doc/master/classROOT_1_1RDataFrame.html) with features that are only available from ROOT 6.15 onwards. Make sure you have a corresponding version set up.

## Command line options
The options which can be given to the executable are
- `-c` : location of the config file
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with @<filename> (similar to hadd functionality))
- `-j` : search-replace expressions for the config file (<search_for0:replace_by0;search_for1:replace_by1;...)
- `-o` : output file
- `-t` : `TTree` name
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions are `friendfile(s):friendtree` combinations. Can take more than 1 file (see syntax from `-i` option)

# Write a config file
Check out the [template](config/template_chain2histRDF.info)!
To maintain some compatibility with `stuff2hist`, this script also allows for 3 types of draw commands:
- `Draw`, including cutting and weighting. The first argument of the the `TTree::Draw()` function is mimiced, `cut` and `weight` are additional fields in the node.
- `var`, `nbins`, `min`, `max`, `yvar`, `nbinsy`, `ymin`, `ymax`, `zvar`, `nbinsz`, `zmin`, `zmax`, `histname` and `weight` to have a easily understandable configuration. Here only var is mandatory
- simply a list of variables. Plot range and binning are chosen automatically. 2 and 3D plots supported by separating variables with a colon

The `Draw` and `var` commands are placed in a node that has to be called `hists`.

## Differences with respect to `stuff2hist`
A new `title` field has been introduced in both `Draw` and `var` nodes to set histogram and axis titles (`title "<hist_title>;<x-axis_title>;<y-axis_title>"`).

Using weights: note that there is a difference between this script and `stuff2hist`, which only allowed to define a single global weight for all histograms in the config file. However, a global weight can still be defined with a `global_weight` field in the `hists` node.

### Further options
- *threads* : (int, optional) number of threads for implicit multithreadind. **ATTENTION** by default all available cores will be used. your collegues might get angry if you overuse it
- *Define*  : (child, optional) calls [`RDataFrame`'s Define action](https://root.cern.ch/doc/master/classROOT_1_1RDF_1_1RInterface.html#aefd0d480704c0c39c599c28666c713d2).
              The script iterates key value pairs, where the key defines the name of the new variable and the value the expression/transformation to be evaluated.
- *Filter*  : (string, optional) calls [`RDataFrame`'s Filter action](https://root.cern.ch/doc/master/classROOT_1_1RDF_1_1RInterface.html#af415d0a369aaa449492563f47a13fd37).
              This allows to put a global cut for all histograms which are added to allow for faster processing
The script also allows to use IOjugglers [REPLACE](https://www.physi.uni-heidelberg.de/~mstahl/IOjuggler/html/namespaceIOjuggler.html#ae07c88817144b09be6692508d3daadaa) and [APPEND](https://www.physi.uni-heidelberg.de/~mstahl/IOjuggler/html/namespaceIOjuggler.html#a5843518801daf409eca1393bcc396079) nodes

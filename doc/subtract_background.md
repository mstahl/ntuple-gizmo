# Documentation for subtract_background.cpp
Given an unbinned sample in a tree and a 2D histogram (the sample selection needs to match with the histo),
the script performs a background subtraction using further histograms as input, which can be of different origin.
The output is a tree with a branch of weights that can be used as friend tree to the input tree.

## Details
The task at hand is to subtract 2D control variable distributions of various origin ("backgrounds") from a mixed ("signal+background")
input distribution to obtain the desired distribution ("signal"). The normalizations of all components need to be calculated beforehand,
for example by fitting a discriminating variable, and passed to this script via the command line.

First, the script makes an adaptive binning from the first input histogram (make sure to have a fine enough binning so that TH2A works).
All other input histograms adapt this binning so that the subtraction can happen.
After the subtraction, the bin-by-bin ratio of initial ("mixed") and final ("unfolded") histogram is calculated.
This ratio-histogram is used as a lookup-table to calculate the event-by-event weight that is stored in the output.
Note that weights are not normalized again.

## Command line options
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with @<filename> (similar to hadd functionality)). One or two input files are expected
- `-j` : control variables, e.g. `"B_PT;B_ETA"`
- `-n` : minimal number of events per bin to define adaptive binning from the first input histogram
- `-o` : output file
- `-t` : input `TTree` names
- `-w` : name of the weight branch in the output tree
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions: list of files histograms and their normalizations zipped like `"<file:hist;norm>"`

## Snakemake wrapper
The wrapper consumes inputs from the `input.sample` node or from `params.sample`.
This is to have hooks for input files that are part of the workflow and those that aren't.

The wrapper needs:
- `input.sample` or `params.sample` (`list` or `str`): an input file with unbinned data from which the main histogram for background subtraction was made
- `output` (`str`): the output friend-file
- `params.control_vars` (`list` or `str`): only 2 variables are supported, like `["B_PT","B_ETA"]` or `"B_PT;B_ETA"`
- `params.control_var_hist` (`str`): name of the histogram with the mixed distribution
- `params.file_control_var_hist` (`str`): file containing hist of control vars
- `params.norm_control_var_hist` (`str`): normalization of control var histogram (like the number of events from a fit to the discriminating variable)
- `params.subtract_hists` (`list` or `str`): name of the histogram(s) to subtract
- `params.files_subtract_hists` (`list` or `str`): file containing hists to subtract
- `params.norms_subtract_hists` (`list` or `str`): normalization of histograms to subtract
Optional arguments:
- `params.work_dir` (`str`): working directory, default `"$RF"`
- `params.tee` (`str`): for logging, default `">"`
- `params.tree_name` (`str`): tree name for input files and friends (see note above), default `"t"`
- `params.weight_name` (`str`): name of the weight branch in the output tree, default `"w"`
- `params.verbosity` (castable to `str`)

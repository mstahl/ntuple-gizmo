# Documentation for hadd_ntg.cpp

The script is basically a implementation of `hadd`, allowing to add single trees from the input file(s).

## Command line options
The options which can be given to the executable are
- `-d` : working directory (files will be prepended with this directory, optional)
- `-i` : input file(s) (single file, semicolon-separated list or ASCII file read in with @<filename> (similar to hadd functionality))
- `-o` : output file
- `-t` : input `TTree` name
- `-v` : verbosity (1-3, optional)
- `-h` : help
- nonoptions are `friendfile(s):friendtree` combinations. Can take more than 1 file (see syntax from `-i` option)

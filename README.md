# Please contribute
This is open analysis code. All LHCb members are invited to contribute. Please have a look at our [guidelines](CONTRIBUTING.md) for contributions.

# Documentation
The package contains several wrapper scripts that allow using ROOT by writing config files only
(based on the INFO parser of [boost::property_tree](http://www.boost.org/doc/libs/1_64_0/doc/html/property_tree/parsers.html#property_tree.parsers.info_parser)) <br>
Links to the documentation of the wrappers:
- [stuff2hist](doc/stuff2hist.md) for making ROOT histograms from TTrees, TChains or RooDataSets
- [draw_stuff](doc/draw_stuff.md) for plotting with ROOT
- [tree_trimmer](doc/tree_trimmer.md) for TTree manipulations
- [misID_beta](doc/misID_beta.md) for studying misidentification backgrounds
- [CutOptScan](doc/CutOptScan.md) for rectangular cut optimisation
- [trainMVA](doc/trainMVA.md) for classification or regression tasks with TMVA
- [applyMVA](doc/applyMVA.md) for applying TMVA weight-files to ntuples
- [tree_trimmerRDF](doc/tree_trimmerRDF.md) for trimming trees and chains
- [elwms](doc/elwms.md) for trimming trees and chains, applying MVAs, writing RooWorkspaces and making histograms all at the same time
- [multiple_candidates](doc/multiple_candidates.md) for finding multiple candidates and persisting them in an output tree
- [truth_match](doc/truth_match.md) for matching generated tracks to reconstructed tracks by kinematics

# Build standalone package
The initial step is to check out the repository, navigate there and update the IOjuggler submodule:
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/sneubert/ntuple-gizmo.git  (our your own fork)
cd ntuple-gizmo
git submodule update --init --recursive
```
To build the package, you need to setup an environment containing ROOT, boost and a c++17 compiler. The easiest solution is a setup from views provided by the SFT group on cvmfs, e.g.
```
source /cvmfs/sft.cern.ch/lcg/views/LCG_96bpython3/x86_64-centos7-gcc9-opt/setup.sh
```
Now, the package can be build with
```
mkdir build; cd build; cmake ..; make; cd -
```
You may also want to be able to enable [`TH2A`](https://gitlab.cern.ch/mstahl/2DAdaptiveBinning) support by doing `cmake -DWITH_TH2A=ON ..`.
Make sure cmake has not cached this option when switching this flag on and off.

# Integrate package into your project
You can integrate the package as a submodule into your package. Your parent `CMakeLists.txt` must only setup ROOT with RooFit TMVA and EG, boost and include the `src` directory of this package
as for example this [CMakeLists](CMakeLists.txt), or the [DfromB CMakeLists](https://gitlab.cern.ch/sneubert/DfromBBDTs/blob/master/CMakeLists.txt)

# Snakemake wrappers
The wrapper scripts in this package are wrapped for snakemake in [wrapper](wrapper/).
To use the wrapped wrappers, export `$NTGROOT` to the directory in which this README lives.
Snakemake will use this env variable to find the scripts.
Executables built by a parent CMake project in the parent `build` folder will get precedence over a possibly built executable from standalone ntuple-gizmo.

A few words on the purpose of the wrappers: their advantage is that often used idioms when assembling the snakemake rule can be hidden in the wrapper.
Also, snakemake input, output and params have a "natural" meaning and make the interface to the user much cleaner. Last but not least the wrappers allow
to fail earlier if some input arguments are not what they are intended to be (i.e. the wrappers can enforce a certain logic by assert statements).
The snakemake wrappers are documented with comment strings in the scripts.
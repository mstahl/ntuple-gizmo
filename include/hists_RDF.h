/**
  @date:   2020-04-18
  @author: mstahl
  @brief:  draw histograms with RDataFrame
*/
#include <ROOT/RDataFrame.hxx>
#include <IOjuggler.h>
#include <variant>
/**
@brief binning struct
*/
struct hdef {
  std::string               name;
  std::string               title;
  std::string               wght;
  std::array<std::string,3> vars;
  std::array<int,3>         bins;
  std::array<double,3>      mins;
  std::array<double,3>      maxs;
  //default ctor
  hdef(){
    name  = "htemp";
    title = "";
    wght  = "";
    vars  = {"","",""};
    bins  = {128,128,128};
    mins  = {0.,0.,0.};
    maxs  = {0.,0.,0.};
  }
};

/**
@TODO enter description of the struct here
*/
using hist_ptrs = std::variant<ROOT::RDF::RResultPtr<TH1D>,ROOT::RDF::RResultPtr<TH2D>,ROOT::RDF::RResultPtr<TH3D>>;
using result_ptrs = std::vector<hist_ptrs>;

namespace pt = boost::property_tree;
/**
  * @fn void hists_RDF(pt::ptree const& hist_config, result_ptrs& lazy_hists, ROOT::RDF::RNode& df,
                       const MessageService& msgsvc = MessageService("hists_RDF",MSG_LVL::INFO))
  * @param hist_config: property tree containing the histo drawing info
  * @param lazy_hists: struct containing vectors of explicitly typed histogram RResultPtrs, i.e. it contains all the pointers to hists that we can persist later
  * @param df: augmented data frame from the parent scope
  * @param msgsvc: MessageService object
  * @details: make a vector of RResultPtr to histos (they trigger the event loop when Get is called on the object pointer, but the RResultPtr is more than just a shared_ptr,
  *           which makes it un-castable to the TH1 base class. Therefore we have to use all forms explicitly in the result_ptrs struct)
  */
void hists_RDF(pt::ptree const& hist_config, result_ptrs& lazy_hists, ROOT::RDF::RNode& df,
               const MessageService& msgsvc = MessageService("hists_RDF",MSG_LVL::INFO)){
  auto nh = hist_config.size();
  msgsvc.infomsg("Requested to produce " + std::to_string(nh) + " histograms");
  auto const global_weight = hist_config.get<std::string>("global_weight","");
  // for checking if the variables are known to our RDataFrame instance
  auto const column_names = df.GetColumnNames();

  auto make_hist = [&lazy_hists,&msgsvc,&column_names] (const auto&& dim, hdef&& hp, ROOT::RDF::RNode& adf) -> void {
    //sanity check if variables are defined
    for(const auto& v : hp.vars){
      if(!v.empty() && std::find(column_names.begin(), column_names.end(), v) == column_names.end()){
        msgsvc.warningmsg("Cannot find "+v+" in current dataframe. Skipping this histogram."); return;
      }
    }
    if(hp.wght.empty()){//it's not pretty like this, but should perform better than using a default weight of 1.
      switch(dim){
        case 1: lazy_hists.push_back(
                adf.Histo1D(ROOT::RDF::TH1DModel{hp.name.data(),hp.title.data(),hp.bins[0],hp.mins[0],hp.maxs[0]},hp.vars[0]));
          break;
        case 2: lazy_hists.push_back(
                adf.Histo2D(ROOT::RDF::TH2DModel{hp.name.data(),hp.title.data(),hp.bins[0],hp.mins[0],hp.maxs[0],
                                                 hp.bins[1],hp.mins[1],hp.maxs[1]},hp.vars[0],hp.vars[1]));
          break;
        case 3: lazy_hists.push_back(
                adf.Histo3D(ROOT::RDF::TH3DModel{hp.name.data(),hp.title.data(),hp.bins[0],hp.mins[0],hp.maxs[0],
                                                 hp.bins[1],hp.mins[1],hp.maxs[1],hp.bins[2],hp.mins[2],hp.maxs[2]},
                                                 hp.vars[0],hp.vars[1],hp.vars[2]));
          break;
        default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one..."); return;
      }
    }
    else{
      switch(dim){
        case 1: lazy_hists.push_back(
                adf.Histo1D(ROOT::RDF::TH1DModel{hp.name.data(),hp.title.data(),hp.bins[0],hp.mins[0],hp.maxs[0]},hp.vars[0],hp.wght));
          break;
        case 2: lazy_hists.push_back(
                adf.Histo2D(ROOT::RDF::TH2DModel{hp.name.data(),hp.title.data(),hp.bins[0],hp.mins[0],hp.maxs[0],
                                                 hp.bins[1],hp.mins[1],hp.maxs[1]},hp.vars[0],hp.vars[1],hp.wght));
          break;
        case 3: lazy_hists.push_back(
                adf.Histo3D(ROOT::RDF::TH3DModel{hp.name.data(),hp.title.data(),hp.bins[0],hp.mins[0],hp.maxs[0],
                                                 hp.bins[1],hp.mins[1],hp.maxs[1],hp.bins[2],hp.mins[2],hp.maxs[2]},
                                                 hp.vars[0],hp.vars[1],hp.vars[2],hp.wght));
          break;
        default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one..."); return;
      }
    }
  };

  for (const auto& hconf : hist_config){
    ROOT::RDF::RNode adf = df;
    //apply cuts
    if(const auto cut = hconf.second.get_optional<std::string>("cut")){
      msgsvc.debugmsg("Applying Filter " + *cut);
      adf = adf.Filter(*cut);
    }
    //call histo lazy actions
    //decide whether to call Histo1D, Histo2D or Histo3D based on string parsing
    //we have to parse a Draw command here. do it like before, titles etc. can be passed later in draw_stuff
    if (const auto drawcmd = hconf.second.get_optional<std::string>("Draw")){
      //a Draw command can look like: zvar:yvar:xvar>>hist_name(<nbinsx>,<xlow>,<xhi>,<nbinsy>,<ylow>,<yhi>,<nbinsz>,<zlow>,<zhi>) (note the order!)
      //strip everything right of >> to get the variables we want to plot
      auto pos = (*drawcmd).find(">>");//14 in the example string
      auto varsstr = pos == std::string::npos ? *drawcmd : boost::algorithm::erase_tail_copy(*drawcmd,(*drawcmd).size()-pos);//zvar:yvar:xvar
      std::vector<std::string> vars;
      boost::algorithm::split(vars, varsstr , boost::algorithm::is_any_of(":"));
      msgsvc.debugmsg("First variable of this \"Draw\" command "+vars[0]);
      const auto dim = vars.size();
      msgsvc.debugmsg("Drawing " + std::to_string(dim) + "D histogram");
      //start to fill hdef struct with vars
      hdef draw_hd;
      switch(dim){
        case 1: draw_hd.vars[0] = vars[0]; break;
        case 2: draw_hd.vars = {vars[1],vars[0],""}; break;//adapt weird root convention
        case 3: draw_hd.vars = {vars[2],vars[1],vars[0]}; break;//adapt weird root convention
        default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one..."); continue;
      }

      //for setting the histo name, get the node name or parse hist_name(<nbinsx>,<xlow>,<xhi>,<nbinsy>,<ylow>,<yhi>,<nbinsz>,<zlow>,<zhi>)
      auto name_and_binning = pos == std::string::npos ? hconf.first : boost::algorithm::erase_head_copy(*drawcmd,pos+2);
      std::vector<std::string> namebinning;
      boost::algorithm::split(namebinning, name_and_binning , boost::algorithm::is_any_of("("));
      std::vector<std::string> binning;
      if(namebinning.size() > 1){//we have a binning
        boost::algorithm::trim_right_if(namebinning[1],boost::algorithm::is_any_of(")"));
        boost::algorithm::split(binning, namebinning[1],boost::algorithm::is_any_of(","));
        msgsvc.debugmsg("name binning "+namebinning[0]);
        for(const auto& b : binning)
          msgsvc.debugmsg("Binning: "+b);
      }
      //sanity check of the parsed binning
      bool use_std_binning = true;
      if(binning.size() > 0){
        if(3*dim != binning.size())
          msgsvc.warningmsg("Something seems to be wrong with the binning. Using standard binning instead...");
        else use_std_binning = false;
      }

      //fill the rest of hdef
      if(!use_std_binning){
        switch(dim){
          case 1:
            draw_hd.bins[0] = std::stoi(binning[0]);
            draw_hd.mins[0] = std::stod(binning[1]);
            draw_hd.maxs[0] = std::stod(binning[2]);
            break;
          case 2:
            draw_hd.bins = {std::stoi(binning[0]),std::stoi(binning[3]),128};
            draw_hd.mins = {std::stod(binning[1]),std::stod(binning[4]),0.};
            draw_hd.maxs = {std::stod(binning[2]),std::stod(binning[5]),0.};
            break;
          case 3:
            draw_hd.bins = {std::stoi(binning[0]),std::stoi(binning[3]),std::stoi(binning[6])};
            draw_hd.mins = {std::stod(binning[1]),std::stod(binning[4]),std::stod(binning[7])};
            draw_hd.maxs = {std::stod(binning[2]),std::stod(binning[5]),std::stod(binning[8])};
            break;
          default: msgsvc.warningmsg("Cannot draw a histogram with more than 3 dimensions or less than 1. Going to next one...");
            break;
        }
      }
      draw_hd.name  = namebinning[0];
      draw_hd.title = hconf.second.get<std::string>("title","");
      draw_hd.wght  = hconf.second.get<std::string>("weight",global_weight);

      //call function that adds the current histogram to the augmented dataframe
      make_hist(std::move(dim),std::move(draw_hd),adf);

    }//"Draw" command
    else if(const auto varx = hconf.second.get_optional<std::string>("var")){
      //try to get other variables
      const auto vary = hconf.second.get<std::string>("yvar","");
      const auto varz = hconf.second.get<std::string>("zvar","");
      auto dim = 1u;
      if(!vary.empty()){
        dim++;
        if(!varz.empty())
          dim++;
      }

      hdef draw_hd;
      draw_hd.name  = hconf.second.get<std::string>("histname",hconf.first);
      draw_hd.title = hconf.second.get<std::string>("title","");
      draw_hd.wght  = hconf.second.get<std::string>("weight",global_weight);
      draw_hd.vars  = {*varx,vary,varz};
      draw_hd.bins  = {hconf.second.get<int>("nbins",128),hconf.second.get<int>("nbinsy",128),hconf.second.get<int>("nbinsz",128)};
      draw_hd.mins  = {hconf.second.get<double>("min",0.),hconf.second.get<double>("ymin",0.),hconf.second.get<double>("zmin",0.)};
      draw_hd.maxs  = {hconf.second.get<double>("max",0.),hconf.second.get<double>("ymax",0.),hconf.second.get<double>("zmax",0.)};

      //call function that adds the current histogram to the augmented dataframe
      make_hist(std::move(dim),std::move(draw_hd),adf);

    }//end drawing with var commands
    else if(hconf.second.data().empty()){//draw all variables with some standard binning
      hdef draw_hd;
      //parse name of variable(s)
      std::vector<std::string> variables;
      boost::algorithm::split(variables, hconf.first, boost::algorithm::is_any_of(":"));
      if(variables.size()>3){
        msgsvc.warningmsg("Couldn't parse "+hconf.first+" . Going to next histogram.");
        continue;
      }
      draw_hd.name = boost::algorithm::join(variables,"_vs_");
      draw_hd.wght = global_weight;
      auto dim = variables.size();
      std::move(variables.begin(),variables.begin()+variables.size(),draw_hd.vars.begin());
      //call function that adds the current histogram to the augmented dataframe
      make_hist(std::move(dim),std::move(draw_hd),adf);
    }
    else if(hconf.first=="global_weight") msgsvc.infomsg("Making hists with global weight: "+hconf.second.data());
    else msgsvc.errormsg("\"Draw\" or \"var\" key missing. Going to next histogram.");
  }//end looping hist_config
}

void set_CustomPalette(const int& pal){

  //basically this: https://root.cern.ch/doc/master/TColor_8cxx_source.html#l02317
  if(pal < 113 || pal > 117)throw std::runtime_error("custom palette not found");

  auto setpal = [&] (const int&& nRGBs, Double_t* stops, Double_t* red, Double_t* green, Double_t* blue) {
    const int NCont = gStyle->GetNumberContours();
    for(int j = 0; j < nRGBs; j++ ){
      red  [j] /= 255;
      blue [j] /= 255;
      green[j] /= 255;
    }
    Int_t FI = TColor::CreateGradientColorTable(nRGBs, stops, red, green, blue, NCont);
    Int_t MyPalette[NCont];
    for (int i=0; i < NCont; i++)
      MyPalette[i] = FI+i;
    gStyle->SetPalette(NCont, MyPalette);
  };

  switch (pal) {
    case 113:
      {
        const int nRGBs = 9;
        Double_t stops[nRGBs] =  {0.0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1.0};
        Double_t red  [nRGBs]  = {255., 248.,  219., 152., 111.,  62.,  40.,   17.,  4.};
        Double_t green[nRGBs]  = {255., 248.,  236., 197., 162., 150., 131.,   91., 40.};
        Double_t blue [nRGBs]  = {150.,  70.,   43.,  27.,  23.,  21.,  18.,   14.,  9.};
        setpal(std::move(nRGBs),stops,red,green,blue);
      }
      break;
    case 114:
      {
        const int nRGBs = 21;
        Double_t stops[nRGBs] = {0.0 , 0.05, 0.10, 0.15, 0.20, 0.25, 0.30, 0.35, 0.40, 0.45, 0.50, 0.55, 0.60, 0.65, 0.70, 0.75, 0.80, 0.85, 0.90, 0.95, 1.0};
        Double_t red  [nRGBs] = {255., 230., 230., 220., 152., 111.,  62.,  40.,  17.,   4.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,   0.,    0.,  0., 0.,};
        Double_t green[nRGBs] = {255., 170., 200., 220., 197., 162., 150., 131.,  91.,  80.,  90., 100., 110.,  90.,  70.,  50.,  30.,  10.,    0.,  0., 0.,};
        Double_t blue [nRGBs] = {255.,   0.,  25.,  50.,  27.,  23.,  21.,  18.,  14.,  20.,  50.,  80., 110., 140., 170., 200., 230., 200.,  100., 50., 0.,};
        setpal(std::move(nRGBs),stops,red,green,blue);
      }
      break;
    case 115:
      {
        const int nRGBs = 4;
        Double_t stops[nRGBs] = {0.0 ,0.05, 0.50, 1.0};
        Double_t red  [nRGBs] = {255.,245.,   0., 0.,};
        Double_t green[nRGBs] = {255.,225., 128., 0.,};
        Double_t blue [nRGBs] = {255.,  0.,   0., 0.,};
        setpal(std::move(nRGBs),stops,red,green,blue);
      }
      break;
    case 117: // dark to light blue
      {
        const Int_t Number = 2;
        Double_t Red[Number]    = { 0.00, 0.90};
        Double_t Green[Number]  = { 0.00, 0.90};
        Double_t Blue[Number]   = { 0.40, 1.00};
        Double_t Length[Number] = { 0.00, 1.00};
        Int_t nb=50;
        TColor::CreateGradientColorTable(Number,Length,Red,Green,Blue,nb);
      }
      break;
  }
}
